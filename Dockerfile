## THIS Docker image is used to test Tplus

FROM docker:stable-dind as dind

USER root
RUN apk add bash sudo wget htop
ENV DOCKER_HOST unix:///var/run/docker.sock
RUN sudo apk add --no-cache libc6-compat

ADD dind/run.sh /run.sh
ADD dind/tplus.server /root/.tplus/server.yml

ADD linux /usr/local/bin/tplus-server

RUN sudo chmod +x /*.sh /usr/local/bin/tplus-server

ENTRYPOINT ["bash"]
CMD ["/run.sh"]


FROM dind as host
ADD dind/run_hostdocker.sh /run.sh
ENTRYPOINT ["bash"]
CMD ["/run.sh"]