# Tplus

Tplus-Server is the main binary of the Tplus development environment,
find more information here: http://tplus.dev/

## Build

to build this project, make sure [packr](https://github.com/gobuffalo/packr) is installed and run:

```
packr build main.go
```


### Documentation:

Current documentation for Tplus is available at [tplus.dev](https://tplus.dev)

### Join our Community

For feedback and questions, please find us @

* [Telegram](https://t.me/tuliptools)
* [Twitter](https://twitter.com/TulipToolsOU)
* [tezos-dev Slack](https://tezos-dev.slack.com/#/)

### Related Repositories:
* [Demo Projects](https://gitlab.com/tuliptools/tplusdemoprojects)
* [User Interface](https://gitlab.com/tuliptools/tplusgui)
* [CLI Tool](https://gitlab.com/tuliptools/TplusCLI)
* [Plugins](https://gitlab.com/tuliptools/TplusPlugins)

Tplus is developed by [TulipTools](https://tulip.tools/)

![Tulip Logo](https://tulip.tools/wp-content/uploads/2020/06/tulip_small-2.png)
