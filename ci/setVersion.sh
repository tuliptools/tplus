#!/bin/sh

export TEZOS_VESRION="tezos-v7.3"

export WebImage="registry.gitlab.com/tuliptools/tplusgui:$TPLUS_VERSION"
export TezosImage="registry.gitlab.com/tuliptools/tplus:$TEZOS_VESRION"
export NodeProxyImage="registry.gitlab.com/tuliptools/tezproxy:latest"
export ProxyImage="registry.gitlab.com/tuliptools/tplusvmproxy:latest"


docker pull $WebImage
docker pull $TezosImage
docker pull $NodeProxyImage
docker pull $ProxyImage

export WebImageDigest=`docker images --digests | grep tuliptools/tplusgui | grep $TPLUS_VERSION | awk '{{print $3}}'`
export TezosImageDigest=`docker images --digests | grep registry.gitlab.com/tuliptools/tplus | grep $TEZOS_VESRION  | awk '{{print $3}}'`
export NodeProxyImageDigest=`docker images --digests | grep registry.gitlab.com/tuliptools/tezproxy | grep latest | awk '{{print $3}}'`
export ProxyImageDigest=`docker images --digests | grep registry.gitlab.com/tuliptools/tplusvmproxy | grep latest | awk '{{print $3}}'`


echo "Version Summary:"

echo "WebImage: $WebImage - $WebImageDigest"
echo "TezosImage: $TezosImage - $TezosImageDigest"
echo "NodeProxyImage: $NodeProxyImage - $NodeProxyImageDigest"
echo "ProxyImage: $ProxyImage - $ProxyImageDigest"

curl -L https://raw.githubusercontent.com/johanhaleby/bash-templater/master/templater.sh -o /tmp/templater
chmod +x /tmp/templater
/tmp/templater ./internal/tplus-server/version/version.go.template > ./internal/tplus-server/version/version.go