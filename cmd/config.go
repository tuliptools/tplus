package cmd

import (
	"fmt"
	"github.com/AlecAivazis/survey/v2"
	"github.com/gobuffalo/packr"
	"github.com/spf13/cobra"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/config"
	"go.uber.org/dig"
	"gopkg.in/yaml.v2"
	"os"
	"runtime"
	"syscall"
)

func ConfigCommands(c *dig.Container, super *cobra.Command) {

	port := 0
	endpoints := ""
	endpointport := ""
	publicip := ""
	userauth := ""
	ros := runtime.GOOS

	var root = &cobra.Command{
		Use:   "config",
		Short: "Show and edit your Configuration",
	}

	var view = &cobra.Command{
		Use:   "view",
		Short: "Show Configuration",
		Run: func(cmd *cobra.Command, args []string) {
			c.Invoke(func(conf *config.Config) {
				d, _ := yaml.Marshal(conf)
				fmt.Println()
				fmt.Println(string(d))
				fmt.Println()
			})
		},
	}

	var init = &cobra.Command{
		Use:   "init",
		Short: "Create config file",
		Run: func(cmd *cobra.Command, args []string) {
			e := c.Invoke(func(box *packr.Box) {
				var mode string
				mode = "local"
				if ros == "darwin" {
					endpoints = "bind"
				}

				var hd string

				if os.Getenv("TPLUS_SERVER_CONFIG_PATH") == "" {
					hd, _ = os.UserHomeDir() // no trailing /
				} else {
					hd = os.Getenv("TPLUS_SERVER_CONFIG_PATH")
				}

				path := hd + "/.tplus"

				conf := config.Config{}
				conf.LogType = "short_and_pretty"
				conf.DataDir = path + "/server/data"

				syscall.Mkdir(conf.DataDir, 0777)

				conf.Mode = mode
				conf.Box = box
				conf.EndpointMode = endpoints
				conf.EndpointPort = endpointport
				conf.PublicIp = publicip

				qs := []*survey.Question{
					{
						Name:   "port",
						Prompt: &survey.Input{Message: "What port should the Web UI + API use?"},
					},
				}


				answers3 := struct {
					Port int
				}{}

				if port == 0 {
					err := survey.Ask(qs, &answers3)
					if err != nil {
						fmt.Println(err.Error())
						return
					}
					conf.WebPort = answers3.Port
				} else {
					conf.WebPort = port
				}


				qs2 := []*survey.Question{

					{
						Name: "multiuser",
						Prompt: &survey.Select{
							Message: "Will multiple Users have access to this Server?:",
							Options: []string{"yes", "no"},
							Default: "no",
						},
					},

				}


				answers4 := struct {
					Multiuser string
				}{}

				if userauth != "yes" && userauth != "no" {
					err := survey.Ask(qs2, &answers4)
					if err != nil {
						fmt.Println(err.Error())
						return
					}
					conf.EnableUserAuth = answers4.Multiuser == "yes"
				} else {
					conf.EnableUserAuth = userauth == "yes"
				}


				conf.Save()

				fmt.Println("Configuration saved! \n")

			})
			if e != nil {
				fmt.Println(e)
			}
		},
	}

	init.PersistentFlags().IntVar(&port, "port", 0, "Sets Web/API Port")
	init.PersistentFlags().StringVarP(&endpoints, "emode", "e", "redirect", "How to handle endpoints")
	init.PersistentFlags().StringVarP(&endpointport, "eport", "r", "8900", "TCP start port for endpoints")
	init.PersistentFlags().StringVarP(&publicip, "publicip", "p", "localhost", "Hostname for API/Web")
	init.PersistentFlags().StringVarP(&userauth, "userauth", "u","", "enable userauth")
	root.AddCommand(init)
	root.AddCommand(view)
	super.AddCommand(root)

}
