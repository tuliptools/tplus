package cmd

import (
	"fmt"
	"github.com/olekukonko/tablewriter"
	"github.com/spf13/cobra"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"go.uber.org/dig"
	"os"
)

func EnvCommands(c *dig.Container, super *cobra.Command) {

	var root = &cobra.Command{
		Use:   "env",
		Short: "Manage environments",
	}

	var ls = &cobra.Command{
		Use:   "ls",
		Short: "List environments",
		Run: func(cmd *cobra.Command, args []string) {
			e := c.Invoke(func(d *db.DB) {
				envs, err := d.GetEnvironments()
				if err != nil {
					fmt.Println(err)
				} else {

					var data [][]string

					table := tablewriter.NewWriter(os.Stdout)
					table.SetHeader([]string{"Id", "Branch", "Sandbox", "Name", "Tez Status", "Created", "Last used"})

					for _, e := range envs {
						sb := "no"
						if e.Tezos.Sandboxed == true {
							sb = "yes"
						}
						data = append(data, []string{
							e.Id, e.Tezos.Branch, e.Name, e.Tezos.Status, sb, e.Created.Format("January 02,  15:04:05"), e.LastRead.Format("January 02,  15:04:05"),
						})
					}
					for _, v := range data {
						table.Append(v)
					}
					table.SetBorder(false)
					fmt.Println()
					table.Render() // Send output
					fmt.Println()

				}
			})

			if e != nil {
				fmt.Println(e)
			}
		},
	}

	var rm = &cobra.Command{
		Use:   "rm <id>",
		Short: "Delete environment",
		Run: func(cmd *cobra.Command, args []string) {
			e := c.Invoke(func(d *db.DB) {
				var id string

				if len(args) == 1 {
					id = args[0]
				} else {
					fmt.Println("\nUsage: rm <envidId>\n")
					os.Exit(1)
				}

				u, e := d.GetInviteCodeById(id)
				if e == nil && u.Id == id {
					d.Delete(u)
				}

			})

			if e != nil {
				fmt.Println(e)
			}
		},
	}

	root.AddCommand(rm)
	root.AddCommand(ls)
	super.AddCommand(root)

}
