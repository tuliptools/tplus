package cmd

import (
	"fmt"
	"github.com/manifoldco/promptui"
	"github.com/spf13/cobra"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/manager"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/service"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"go.uber.org/dig"
	"os"
	"sync"
	"time"
)

func UpdateCommands(c *dig.Container, super *cobra.Command) {

	force := false

	checkforce := func(msg string) {
		if force == true {
			return
		}

		prompt := promptui.Select{
			Label: msg,
			Items: []string{"no", "yes"},
		}

		_, result, err := prompt.Run()

		if err == nil && result == "yes" {
			return
		} else {
			if err != nil {
				fmt.Println(err)
			}
			os.Exit(1)
		}
	}

	var root = &cobra.Command{
		Use:   "update",
		Short: "Update Tplus",
	}

	var nodes = &cobra.Command{
		Use:   "nodes",
		Short: "",
		Run: func(cmd *cobra.Command, args []string) {
			c.Invoke(func(db *db.DB,
				tm *task.Manager,
				d * service.DockerService,
				em *manager.HttpEndpointManager ) {

				checkforce("This command will stop and update all your Nodes, continue?")

				envs,e := db.GetEnvironments()
				if e != nil {
					fmt.Println("error getting envs, another instance running?")
					os.Exit(1)
				}
				if len(envs) == 0 {
					fmt.Println("No Nodes to update")
				}


				envManager := manager.NewEnvManagerNoBoot(tm,db,em,d)

				wg := &sync.WaitGroup{}
				for _,env2 := range envs {
					env := env2
					oldState := env.Tezos.Status
					fmt.Println("Stopping Environment", env.Name,"\n\n")
					envManager.Stop(&env)
					fmt.Println("Removing Node Container", env.Name,"\n\n")
					envManager.RemoveNodeContainer(&env)
					if oldState == "running" {
						fmt.Println("Starting new node and related containers ( if any )", "\n\n")
						envManager.StartWithWg(&env,wg)
					}
					time.Sleep(1*time.Second)
				}
				time.Sleep(1*time.Second)
				wg.Wait()
			})
		},
	}

	root.AddCommand(nodes)
	super.AddCommand(root)


}
