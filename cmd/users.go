package cmd

import (
	"fmt"
	"github.com/AlecAivazis/survey/v2"
	"github.com/olekukonko/tablewriter"
	"github.com/spf13/cobra"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/config"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"go.uber.org/dig"
	"os"
)

func NoAuthError(){
	fmt.Println("Multi User Support is not enabled")
	fmt.Println("If you want to change that,")
	fmt.Println("check your config and docs on: https://tplus.dev")
	os.Exit(1)
}
func UserCommands(c *dig.Container, super *cobra.Command) {

	username := ""
	password := ""

	var root = &cobra.Command{
		Use:   "user",
		Short: "Manage Server Users",
	}

	var ls = &cobra.Command{
		Use:   "ls",
		Short: "List Users",
		Run: func(cmd *cobra.Command, args []string) {
			e := c.Invoke(func(d *db.DB, c *config.Config) {
				if c.EnableUserAuth == false {
					NoAuthError()
				}
				users, err := d.GetUsers()
				if err != nil {
					fmt.Println(err)
				} else {

					var data [][]string

					table := tablewriter.NewWriter(os.Stdout)
					table.SetHeader([]string{"Id", "Username", "Admin", "Last Active"})

					for _, u := range users {
						ad := "no"
						if u.Admin {
							ad = "yes"
						}
						data = append(data, []string{
							u.Id, u.Username, ad, u.LastRead.Format("January 02,  15:04:05"),
						})
					}
					for _, v := range data {
						table.Append(v)
					}
					table.SetBorder(false)
					fmt.Println()
					table.Render() // Send output
					fmt.Println()

				}
			})

			if e != nil {
				fmt.Println(e)
			}
		},
	}

	var rm = &cobra.Command{
		Use:   "rm <id>",
		Short: "Delete User",
		Run: func(cmd *cobra.Command, args []string) {
			e := c.Invoke(func(d *db.DB, c *config.Config) {
				if c.EnableUserAuth == false {
					NoAuthError()
				}
				var id string

				if len(args) == 1 {
					id = args[0]
				} else {
					fmt.Println("\nUsage: rm <userId>\n")
					os.Exit(1)
				}

				u, e := d.GetUserById(id)
				if e == nil && u.Id == id {
					d.Delete(u)
				}

			})

			if e != nil {
				fmt.Println(e)
			}
		},
	}

	var create = &cobra.Command{
		Use:   "create",
		Short: "Create a new User",
		Run: func(cmd *cobra.Command, args []string) {
			e := c.Invoke(func(d *db.DB, c *config.Config) {
				if c.EnableUserAuth == false {
					NoAuthError()
				}

				var qs = []*survey.Question{
					{
						Name:     "Username",
						Prompt:   &survey.Input{Message: "Username"},
						Validate: survey.Required,
					},
					{
						Name:     "Password",
						Prompt:   &survey.Password{Message: "Password"},
						Validate: survey.Required,
					},
				}

				answers := struct {
					Password string
					Access   string
					Username string
				}{}

				user := db.User{}
				Id := db.GetRandomId("user")

				if username == "" && password == "" {
					err := survey.Ask(qs, &answers)
					if err != nil {
						fmt.Println(err.Error())
						return
					}

					user.Username = answers.Username
					user.SetPassword(answers.Password)
				} else {
					user.Username = username
					user.SetPassword(password)
				}

				user.Admin = true
				user.Id = Id
				e := d.Create(&user)

				if e != nil {
					fmt.Println(e)
				} else {
					fmt.Println("User created.")
				}
			})

			if e != nil {
				fmt.Println(e)
			}
		},
	}

	create.PersistentFlags().StringVarP(&username, "username", "u", "", "Username")
	create.PersistentFlags().StringVarP(&password, "password", "p", "", "Password")

	root.AddCommand(rm)
	root.AddCommand(ls)
	root.AddCommand(create)
	super.AddCommand(root)

}
