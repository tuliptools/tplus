package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/version"
	"go.uber.org/dig"
)

func VersionCommands(c *dig.Container, super *cobra.Command) {

	var version = &cobra.Command{
		Use:   "version",
		Short: "Show Version",
		Run: func(cmd *cobra.Command, args []string) {
			v := version.GetVersionInfo()
			fmt.Println("Version",v.Version)
			fmt.Println("WebImage",v.WebImage)
			fmt.Println("WebImageDigest:",v.WebImageDigest)
			fmt.Println("TezosImage:",v.TezosImage)
			fmt.Println("TezosImageDigest:",v.TezosImageDigest)
			fmt.Println("NodeProxyImage:",v.NodeProxyImage)
			fmt.Println("NodeProxyImageDigest:",v.NodeProxyImageDigest)
			fmt.Println("ProxyImage:",v.ProxyImage)
			fmt.Println("ProxyImageDigest:",v.ProxyImageDigest)
		},
	}
	super.AddCommand(version)
}
