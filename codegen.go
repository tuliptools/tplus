package main

import (
	"bytes"
	"github.com/tyler-sommer/stick"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"
)

func codegen() {
	var files []string

	root := "./"
	err := filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
		if strings.Contains(path, ".codegen") {
			files = append(files, path)
		}
		return nil
	})
	if err != nil {
		panic(err)
	}

	for _,f := range files {
		TwigCodeGen(f)
	}


}


func TwigCodeGen(path string) {
	newfile := strings.Replace(path,".codegen","",1)

	os.Remove(newfile)

	vars := map[string]stick.Value{
		"CI_COMMIT_SHA" : getenv("CI_COMMIT_SHA","na"),
		"CI_COMMIT_SHORT_SHA": getenv("CI_COMMIT_SHORT_SHA","na"),
		"CI_COMMIT_REF_NAME": getenv("CI_COMMIT_REF_NAME","na"),
		"CI_COMMIT_TAG": getenv("CI_COMMIT_TAG","na"),
	}


	data,_ := ioutil.ReadFile(path)
	env := stick.New(nil)
	buf := &bytes.Buffer{}
	if err := env.Execute(string(data), buf, vars); err != nil {
		log.Fatal(err)
	}

	ioutil.WriteFile(newfile,buf.Bytes(),0777)

}

func getenv(key, fallback string) string {
	value := os.Getenv(key)
	if len(value) == 0 {
		return fallback
	}
	return value
}
