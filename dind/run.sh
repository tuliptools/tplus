#/bin/bash

rm /var/run/docker.pid

export DOCKER_HOST=unix:///var/run/docker.sock

docker-entrypoint.sh dockerd &

sleep 10

/usr/local/bin/tplus-server run

