module gitlab.com/tuliptools/tplus

go 1.14

replace (
	github.com/Sirupsen/logrus v1.0.5 => github.com/sirupsen/logrus v1.0.5
	github.com/Sirupsen/logrus v1.3.0 => github.com/Sirupsen/logrus v1.0.6
	github.com/Sirupsen/logrus v1.4.0 => github.com/sirupsen/logrus v1.0.6
)

require (
	github.com/AlecAivazis/survey/v2 v2.0.7
	github.com/BurntSushi/locker v0.0.0-20171006230638-a6e239ea1c69
	github.com/DefinitelyNotAGoat/go-tezos v1.0.9
	github.com/Microsoft/go-winio v0.4.14 // indirect
	github.com/Sirupsen/logrus v1.4.0 // indirect
	github.com/c9s/goprocinfo v0.0.0-20200311234719-5750cbd54a3b
	github.com/containerd/containerd v1.3.6 // indirect
	github.com/coreos/etcd v3.3.13+incompatible
	github.com/cssivision/reverseproxy v0.0.1
	github.com/cstockton/go-conv v0.0.0-20170524002450-66a2b2ba36e1
	github.com/docker/distribution v2.7.1+incompatible // indirect
	github.com/docker/docker v17.12.0-ce-rc1.0.20200531234253-77e06fda0c94+incompatible
	github.com/docker/go-connections v0.4.0
	github.com/docker/go-metrics v0.0.1 // indirect
	github.com/docker/go-units v0.4.0 // indirect
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.6.2
	github.com/gobuffalo/envy v1.9.0 // indirect
	github.com/gobuffalo/packr v1.30.1
	github.com/gobuffalo/packr/v2 v2.8.0 // indirect
	github.com/golang/protobuf v1.4.0 // indirect
	github.com/google/uuid v1.1.1
	github.com/gorilla/websocket v1.4.2
	github.com/hashicorp/golang-lru v0.5.4
	github.com/hashicorp/hcl v1.0.0
	github.com/karlseguin/ccache v2.0.3+incompatible
	github.com/karlseguin/expect v1.0.7 // indirect
	github.com/karrick/godirwalk v1.15.6 // indirect
	github.com/mackerelio/go-osstat v0.1.0
	github.com/manifoldco/promptui v0.7.0
	github.com/markbates/pkger v0.17.1
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/olekukonko/tablewriter v0.0.4
	github.com/opencontainers/go-digest v1.0.0-rc1 // indirect
	github.com/opencontainers/image-spec v1.0.1 // indirect
	github.com/opencontainers/runc v0.1.1 // indirect
	github.com/opencontainers/runtime-spec v1.0.2 // indirect
	github.com/pkg/errors v0.9.1
	github.com/rogpeppe/go-internal v1.6.0 // indirect
	github.com/shirou/gopsutil v2.20.6+incompatible
	github.com/sirupsen/logrus v1.6.0
	github.com/spf13/cobra v1.0.0
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/spf13/viper v1.6.3
	github.com/tidwall/gjson v1.6.0
	github.com/tyler-sommer/stick v1.0.2
	github.com/vbatts/tar-split v0.11.1 // indirect
	go.etcd.io/bbolt v1.3.4
	go.uber.org/dig v1.9.0
	golang.org/x/crypto v0.0.0-20200602180216-279210d13fed
	golang.org/x/sync v0.0.0-20200317015054-43a5402ce75a // indirect
	golang.org/x/sys v0.0.0-20200602225109-6fdc65e7d980 // indirect
	golang.org/x/text v0.3.2
	golang.org/x/tools v0.0.0-20200308013534-11ec41452d41
	gopkg.in/src-d/go-git.v4 v4.13.1
	gopkg.in/yaml.v2 v2.2.8
)
