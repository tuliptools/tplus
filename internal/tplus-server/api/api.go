package api

import (
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httputil"
	"net/url"
	"os"
	"runtime"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/gobuffalo/packr"
	"github.com/google/uuid"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/config"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/events"
	http2 "gitlab.com/tuliptools/tplus/internal/tplus-server/http"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/manager"
	manager2 "gitlab.com/tuliptools/tplus/internal/tplus-server/metrics"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/service"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/tezcon"
)

type Api struct {
	port      int
	box       *packr.Box
	config    *config.Config
	user      *service.UserService
	docker    *service.DockerService
	envS      *manager.EnvManager
	wallet    *manager.WalletManager
	metrics   *manager2.Metrics
	db        *db.DB
	tezos     *tezcon.TezosConnector
	wsTokens  map[string]string
	uiUrl     string
	projects  *manager.ProjectManager
	endpoints *manager.HttpEndpointManager
	events    *events.Events
	bash      *manager.BashManager
	plugins   *manager.PluginsManager
	intClient *http2.InternalClient
	msgCache  map[string]string
	wsLock    *sync.Mutex
}

type Message struct {
	Success bool
	Error   string
	Message string
}

func NewApi(conf *config.Config,
	box *packr.Box,
	user *service.UserService,
	d *service.DockerService,
	env *manager.EnvManager,
	metrics *manager2.Metrics,
	db *db.DB,
	tezos *tezcon.TezosConnector,
	projects *manager.ProjectManager,
	endpoints *manager.HttpEndpointManager,
	bash *manager.BashManager,
	wallet *manager.WalletManager,
	plugins *manager.PluginsManager,
	client *http2.InternalClient,
	events *events.Events,
) *Api {
	a := Api{}
	a.port = conf.WebPort
	a.config = conf
	a.box = box
	a.user = user
	a.db = db
	a.docker = d
	a.envS = env
	a.metrics = metrics
	a.tezos = tezos
	a.wsTokens = map[string]string{}
	a.projects = projects
	a.endpoints = endpoints
	a.events = events.Source("api")
	a.wallet = wallet
	a.bash = bash
	a.plugins = plugins
	a.intClient = client
	a.msgCache = map[string]string{}
	a.wsLock = &sync.Mutex{}
	return &a
}

func (api *Api) genericResponse(e error, msg string, c *gin.Context) {
	if e != nil {
		api.error(e, c)
	} else {
		res := Message{}
		res.Success = true
		res.Message = msg
		res.Error = ""
		c.JSON(200, res)
	}
}

func (api *Api) error(e error, c *gin.Context) {
	res := Message{}
	res.Success = false
	res.Message = "Error..."
	res.Error = e.Error()
	c.JSON(400, res)
}

func (api *Api) Run() {
	gin.SetMode(gin.ReleaseMode)
	r := gin.New()
	r.Use(gin.RecoveryWithWriter(ioutil.Discard))
	r.Use(RequestIdMiddleware())

	if url, ok := os.LookupEnv("TPLUS_UI_URL"); ok {
		api.uiUrl = url
		fmt.Println("Setting UI URL to", api.uiUrl)
	} else {
		ip, err := api.docker.RunUI()
		if err != nil {
			panic(err)
		}
		api.uiUrl = "http://" + ip
	}

	if runtime.GOOS == "darwin" || os.Getenv("RUN_MAC_PROXY") == "true" {
		api.events.Log("Looks like we are on Mac, running docker proxy server...")
		e := api.docker.RunMacOsXProxy()
		if e != nil {
			api.events.Fatal(e.Error())
		}
	}

	config := cors.DefaultConfig()
	config.AllowAllOrigins = true
	config.AllowCredentials = true
	config.AddAllowHeaders("*")
	r.Use(cors.New(config))

	api.registerAutRoutes(r)
	api.registerdockerRoutes(r)
	api.registerTaskRoutes(r)
	api.registerMetricsRoutes(r)
	api.registerLogRoutes(r)
	api.registerPeerRoutes(r)
	api.registerSocketRoutes(r)
	api.projectRoutes(r)
	api.bashRoutes(r)
	api.envRoutes(r)
	api.walletRoutes(r)
	api.HostStatRoutes(r)
	api.pluginRoutes(r)
	api.studioRoutes(r)

	r.GET("/", api.root)
	r.Any("/ui/*webPath", api.webHandler)
	r.Any("/endpoints/*endpointid", api.epHandler)
	r.Any("/endpoints/*endpointid/*url", api.epHandler)
	r.Any("/file/:file", api.fileHandler)
	r.Any("/pluginsrepo/*url", api.pluginsProxy)
	r.Any("/projectrepo/*url", api.projectsProxy)

	al := sync.WaitGroup{}
	al.Add(1)

	go func() {
		var e error
		var addr string
		addr = ":" + strconv.Itoa(api.port)
		s := &http.Server{
			Addr:           addr,
			Handler:        r,
			ReadTimeout:    3 * time.Second,
			WriteTimeout:   3 * time.Second,
			MaxHeaderBytes: 1 << 20,
		}
		s.ListenAndServe()

		if e == nil {
			api.events.Info("API server exited")
		} else {
			api.events.Error("API exited with error:" + e.Error())
		}
		time.Sleep(1 * time.Second)
		al.Done()
	}()

	go func() {
		time.Sleep(1 * time.Second)
		api.events.Info("API starting")
		api.events.Info("API Endpoint: http://localhost:" + strconv.Itoa(api.port))
		api.events.Info("UI Endpoint:  http://localhost:" + strconv.Itoa(api.port) + "/ui")
	}()

	al.Wait()

}

func (api *Api) webHandler(c *gin.Context) {
	// forward to web container
	cl := api.intClient.GetProxiedClient()
	url := api.uiUrl + c.Param("webPath")

	req, err := http.NewRequest(c.Request.Method, url, c.Request.Body)
	resp, err := cl.Do(req)
	if err != nil {
		c.Abort()
		return
	}
	c.Header("Content-Type", resp.Header.Get("Content-Type"))
	io.Copy(c.Writer, resp.Body)
	defer resp.Body.Close()
	return
}

func (api *Api) pluginsProxy(c *gin.Context) {
	giturl := "https://gitlab.com/tuliptools/TplusPlugins/-/raw/master/"
	remote, err := url.Parse(giturl)
	if err != nil {
		panic(err)
	}
	if remote.Scheme == "https" {
		c.Request.URL.Host = remote.Host
		c.Request.URL.Scheme = remote.Scheme
		c.Request.Header.Set("X-Forwarded-Host", c.Request.Header.Get("Host"))
		c.Request.Host = remote.Host
	}
	c.Request.RequestURI = strings.Replace(c.Request.RequestURI, "/pluginsrepo", "", 1)
	c.Request.URL, _ = url.Parse(c.Request.RequestURI)
	proxy := httputil.NewSingleHostReverseProxy(remote)
	proxy.ServeHTTP(c.Writer, c.Request)
}

func (api *Api) projectsProxy(c *gin.Context) {
	giturl := "https://gitlab.com/tuliptools/tplusdemoprojects/-/raw/master/"
	remote, err := url.Parse(giturl)
	if err != nil {
		panic(err)
	}
	if remote.Scheme == "https" {
		c.Request.URL.Host = remote.Host
		c.Request.URL.Scheme = remote.Scheme
		c.Request.Header.Set("X-Forwarded-Host", c.Request.Header.Get("Host"))
		c.Request.Host = remote.Host
	}
	c.Request.RequestURI = strings.Replace(c.Request.RequestURI, "/projectrepo", "", 1)
	c.Request.URL, _ = url.Parse(c.Request.RequestURI)
	proxy := httputil.NewSingleHostReverseProxy(remote)
	proxy.ServeHTTP(c.Writer, c.Request)
}

func (api *Api) fileHandler(c *gin.Context) {
	file := c.Param("file")
	path := ""
	switch file {
	case "sandbox_json":
		path = "tezos/proto_params/sandbox.json"
	case "PsCARTHAGazKbHtnKfLzQg3kms52kSRpgnDY982a9oYsSXRLQEb":
		path = "tezos/proto_params/PsCARTHAGazKbHtnKfLzQg3kms52kSRpgnDY982a9oYsSXRLQEb.json"
	}

	b, err := api.box.Find(path)
	if err != nil {
		c.String(404, "file not found")
	} else {
		c.String(200, string(b))
	}
	return
}

func (api *Api) epHandler(c *gin.Context) {
	api.endpoints.HandleGin(c.Param("endpointid"), c)
}

func (api *Api) root(c *gin.Context) {
	c.Redirect(302, "/ui")
}

func (api *Api) authUser(next gin.HandlerFunc) gin.HandlerFunc {
	return func(c *gin.Context) {
		if api.config.EnableUserAuth == false {
			next(c)
			return
		}
		token := c.GetHeader("TOKEN")
		if len(token) <= 10 {
			c.String(401, "unauthorized")
			return
		}
		_, e := api.user.TokenToUser(token)
		if e == nil {
			next(c)
			return
		}
		c.String(401, "unauthorized")
		return
	}
}

func (api *Api) authAdmin(next gin.HandlerFunc) gin.HandlerFunc {
	return func(c *gin.Context) {
		if api.config.EnableUserAuth == false {
			next(c)
			return
		}
		token := c.GetHeader("TOKEN")
		if len(token) <= 10 {
			c.String(401, "unauthorized")
			return
		}
		u, e := api.user.TokenToUser(token)
		if e == nil && u.Admin {
			next(c)
			return
		}
		c.String(401, "unauthorized")
		return
	}
}

func (api *Api) User(c *gin.Context) *db.User {
	if api.config.EnableUserAuth == false {
		return api.user.GetDefaultUser()
	}
	token := c.GetHeader("TOKEN")
	u, _ := api.user.TokenToUser(token)
	return u
}

func RequestIdMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("X-Request-Id", uuid.New().String())
		c.Next()
	}
}
