package api

import (
	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
)

func (api *Api) registerAutRoutes(r *gin.Engine) {
	r.POST("/token", api.getToken)
	r.POST("/register", api.register)
	r.GET("/info", api.info)
}

func (api *Api) getToken(c *gin.Context) {
	if api.config.EnableUserAuth == false {
		defaultUser := api.user.GetDefaultUser()
		t, e := api.user.GetTokenForUser(defaultUser)
		if e == nil {
			c.JSON(200, Token{Token: t.Id})
			return
		}
		api.error(e, c)
		return
	}
	params := TokenRequest{}
	if e := c.Bind(&params); e != nil {
		api.error(errors.New("Invalid Username or Password"), c)
		return
	}

	t, e := api.user.GetToken(params.Username, params.Password)
	if e == nil {
		c.JSON(200, Token{Token: t.Id})
		return
	}
	api.error(e, c)
}

func (api *Api) register(c *gin.Context) {
	params := RegisterInput{}
	if e := c.Bind(&params); e != nil {
		api.error(e, c)
		return
	}
	e := api.user.Register(params.Username, params.Password)
	api.genericResponse(e, "User created", c)
}

func (api *Api) info(c *gin.Context) {
	res := Info{}
	res.ServerMode = api.config.Mode
	u := api.User(c)
	if u == nil {
		res.LoggedIn = false
		res.UserRole = "na"
		res.UserID = ""
	} else {
		res.LoggedIn = true
		res.UserRole = "user"
		if u.Admin {
			res.UserRole = "admin"
		}
		res.UserID = u.Id
	}
	c.JSON(200, res)
}
