package api

import (
	"encoding/base64"
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"net/http"
	"os"
	"path/filepath"
	"strings"
)

func (api *Api) bashRoutes(r *gin.Engine) {
	r.POST("/bash/:id/sync", api.authUser(api.GetFileList))
	r.POST("/bash/:id/up", api.authUser(api.FileUpload))
	r.GET("/bash/:id/down/:file", api.authUser(api.GetFile))
}

func (api *Api) GetFileList(c *gin.Context) {
	params := db.Syncfiles{}
	if e := c.Bind(&params); e != nil {
		api.error(e, c)
		return
	}

	user := api.User(c)
	env, err := api.envS.GetEnvironment(user, c.Param("id"))

	if err != nil {
		c.String(500, err.Error())
		return
	}

	bash := api.bash.GetFor(user, &env)
	list := bash.GetLocalFilelist(params)

	response := db.SyncResponse{}
	response.Existing = []db.SyncFile{}
	for fn, h := range list {
		response.Existing = append(response.Existing, db.SyncFile{
			Hash: h,
			Name: fn,
		})
	}

	c.JSON(200, response)
}

func (api *Api) GetFile(c *gin.Context) {
	file, _ := base64.StdEncoding.DecodeString(c.Param("file"))
	user := api.User(c)
	env, err := api.envS.GetEnvironment(user, c.Param("id"))
	if err != nil {
		c.String(500, err.Error())
		return
	}
	bash := api.bash.GetFor(user, &env)
	path := bash.GetDataDir()
	filepath := path + string(file)
	c.File(filepath)
}

func (api *Api) FileUpload(c *gin.Context) {
	file, err := c.FormFile("file")
	if err != nil {
		c.String(http.StatusBadRequest, fmt.Sprintf("get form err: %s", err.Error()))
		return
	}

	user := api.User(c)
	env, err := api.envS.GetEnvironment(user, c.Param("id"))
	bash := api.bash.GetFor(user, &env)

	filename := filepath.Base(file.Filename)
	path := strings.Replace(file.Filename, filename, "", 1)
	os.Remove(strings.Replace(bash.GetDataDir()+path+filename, "//", "/", -1))
	os.MkdirAll(strings.Replace(bash.GetDataDir()+path, "//", "/", -1), 0777)
	err = c.SaveUploadedFile(file, strings.Replace(bash.GetDataDir()+path+filename, "//", "/", -1))

	if err != nil {
		fmt.Println("error uploading file:", err)
	}

}
