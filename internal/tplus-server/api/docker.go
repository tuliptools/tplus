package api

import (
	"github.com/gin-gonic/gin"
	"strings"
)

func (api *Api) registerdockerRoutes(r *gin.Engine) {
	r.GET("/container", api.getContainers)
	r.GET("/container/:id", api.containerInspect)
}

func (api *Api) getContainers(c *gin.Context) {
	var result []ContainerLS
	containers, err := api.docker.FindContainerWithPrefix("mon")
	for _, c := range containers {
		inspect, e := api.docker.Inspect(c)
		if e == nil {
			result = append(result,
				ContainerLS{
					Name:    inspect.Name[1:],
					ID:      inspect.ID,
					Image:   inspect.Image,
					Command: strings.Join(inspect.Config.Entrypoint, " "),
					Created: inspect.Created,
					Status:  inspect.State.Status,
				})
		}
	}

	if err != nil {
		api.error(err, c)
		return
	}
	c.JSON(200, result)
	return
}

func (api *Api) containerInspect(c *gin.Context) {
	inspect, e := api.docker.Inspect(c.Param("id"))

	if e != nil {
		api.error(e, c)
		return
	}
	c.JSON(200, inspect)
	return
}
