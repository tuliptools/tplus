package api

import (
	"errors"
	"github.com/gin-gonic/gin"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/helpers"
)

func (api *Api) envRoutes(r *gin.Engine) {
	r.POST("/env/create/local", api.authUser(api.newLocalEnv))
	r.POST("/env/create/remote", api.authUser(api.newRemoteEnv))
	r.GET("/env", api.authUser(api.GetEnvs))
	r.GET("/env/:id", api.authUser(api.GetEnv))
	r.GET("/env/:id/stop", api.authUser(api.StopEnv))
	r.GET("/env/:id/start", api.authUser(api.StartEnv))
	r.GET("/env/:id/restart", api.authUser(api.RestartEnv))
	r.GET("/env/:id/clean", api.authUser(api.CleanEnv))
	r.GET("/env/:id/destroy", api.authUser(api.DestroyEnv))
}

func (api *Api) newLocalEnv(c *gin.Context) {
	params := AddEnvInput{}
	params.HistoryMode = "archive"
	if e := c.Bind(&params); e != nil {
		api.error(e, c)
		return
	}
	snapsource := ""
	if params.FromSnapshot {
		if params.SnapshotSource == "tulip" {
			snapsource, _ = helpers.GetTulipSnapshotUrl(params.Network, params.HistoryMode)
		} else {
			snapsource = params.SnapshotSource
		}
	}
	backup := ""
	if params.FromBackup {
		if params.BackupSource == "tulip" {
			backup = "tulip"
		} else {
			backup = params.BackupSource
		}
	}
	env := db.Environment{
		Protocol:        params.Protocol,
		SandboxParams:   params.SandboxParams,
		SandboxConfig:   params.SandboxConfig,
		Id:              db.GetRandomId("env"),
		Name:            params.Name,
		Shared:          params.Shared,
		CreatedByUserId: api.User(c).Id,
		SnapshotSource:  snapsource,
		BackupSource:    backup,
		Error:           "",
		Tezos: db.TezosNode{
			Remote:      false,
			RemoteNets:  []string{},
			Branch:      params.Network,
			Status:      "pending",
			HeadLevel:   0,
			HistoryMode: params.HistoryMode,
			Sandboxed:   params.Network == "sandbox",
			Container: db.Container{
				Resources: db.Resources{
					MemoryMax: params.RAM,
					CPUCount:  params.CPU,
				},
				Status: "pending",
			},
		},
	}
	var err error
	if env.Tezos.Sandboxed {
		err = api.envS.CreateSandboxNode(&env,nil)
	} else {
		err = api.envS.CreatePublicNetworkNode(&env,nil)
	}
	if err != nil {
		api.error(err, c)
		return
	}
	api.genericResponse(nil, "Environment created", c)
}

func (api *Api) newRemoteEnv(c *gin.Context) {
	params := AddRemoteInput{}

	if e := c.Bind(&params); e != nil {
		api.error(e, c)
		return
	}

	if len(params.RemoteNodes) == 0 {
		api.error(errors.New("no remote Endpoints configured"), c)
		return
	}

	env := db.Environment{
		Protocol:        "",
		SandboxParams:   "",
		SandboxConfig:   "",
		Id:              db.GetRandomId("env"),
		Name:            params.Name,
		Shared:          params.Shared,
		CreatedByUserId: api.User(c).Id,
		SnapshotSource:  "",
		BackupSource:    "",
		Error:           "",
		Tezos: db.TezosNode{
			Remote:      true,
			RemoteNets:  params.RemoteNodes,
			Branch:      "remotenet",
			Status:      "pending",
			HeadLevel:   0,
			HistoryMode: "unknown",
			Sandboxed:   false,
			Container: db.Container{
				Resources: db.Resources{
					MemoryMax: 500,
					CPUCount:  1,
				},
				Status: "pending",
			},
		},
	}
	var err error
	err = api.envS.CreateRemoteNode(&env,nil)
	if err != nil {
		api.error(err, c)
		return
	}
	api.genericResponse(nil, "Environment created", c)
}

func (api *Api) GetEnvs(c *gin.Context) {
	envs, err := api.envS.GetEnvironments(api.User(c))
	if err != nil {
		api.error(err, c)
		return
	}
	c.JSON(200, envs)
}

func (api *Api) GetEnv(c *gin.Context) {
	envs, err := api.envS.GetEnvironment(api.User(c), c.Param("id"))
	if err != nil {
		api.error(err, c)
		return
	}
	c.JSON(200, envs)
}

func (api *Api) StopEnv(c *gin.Context) {
	env, err := api.envS.GetEnvironment(api.User(c), c.Param("id"))
	if err != nil {
		api.error(err, c)
		return
	}
	go api.envS.Stop(&env)
	c.String(200, "Sheduled.")
}

func (api *Api) StartEnv(c *gin.Context) {
	env, err := api.envS.GetEnvironment(api.User(c), c.Param("id"))
	if err != nil {
		api.error(err, c)
		return
	}
	go api.envS.Start(&env)
	c.String(200, "Sheduled.")
}

func (api *Api) RestartEnv(c *gin.Context) {
	env, err := api.envS.GetEnvironment(api.User(c), c.Param("id"))
	if err != nil {
		api.error(err, c)
		return
	}
	go api.envS.Restart(&env)
	c.String(200, "Sheduled.")
}

func (api *Api) CleanEnv(c *gin.Context) {
	env, err := api.envS.GetEnvironment(api.User(c), c.Param("id"))
	if err != nil {
		api.error(err, c)
		return
	}
	go api.envS.Clean(&env)
	c.String(200, "Sheduled.")
}

func (api *Api) DestroyEnv(c *gin.Context) {
	env, err := api.envS.GetEnvironment(api.User(c), c.Param("id"))
	if err != nil {
		api.error(err, c)
		return
	}
	go api.envS.Destroy(&env)
	c.String(200, "Sheduled.")
}
