package api

import (
	"bufio"
	"github.com/gin-gonic/gin"
	"strconv"
)

func (api *Api) registerLogRoutes(r *gin.Engine) {
	r.GET("/env/:id/logs/tezos/:tail", api.authUser(api.getLogs))
	r.GET("/system/logs/:n", api.authUser(api.GetSystemLogs))
}

func (api *Api) getLogs(c *gin.Context) {
	e, _ := api.envS.GetEnvironment(api.User(c), c.Param("id"))
	reader, err := api.docker.GetLogs(e.Tezos.Container.Id, false, c.Param("tail"))
	var buffer []string
	scanner := bufio.NewScanner(reader)
	if err == nil {
		for scanner.Scan() {
			text := scanner.Text()
			if len(text) >= 8 {
				buffer = append(buffer, text[8:])
			}
		}
	}
	c.JSON(200, buffer)
}


func (api *Api) GetSystemLogs(c *gin.Context) {
	a,_ := strconv.Atoi(c.Param("n"))
	logs := api.events.GetLogs(a)
	c.JSON(200, logs)
}
