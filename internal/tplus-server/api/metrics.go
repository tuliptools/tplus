package api

import (
	"github.com/cstockton/go-conv"
	"github.com/gin-gonic/gin"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"sort"
	"time"
)

func (api *Api) registerMetricsRoutes(r *gin.Engine) {
	r.GET("/env/:id/metric/:source/:metric/:start/:end", api.getMetrics)
}

func (api *Api) getMetrics(c *gin.Context) {
	startInt, _ := conv.Int64(c.Param("start"))
	start := time.Unix(startInt, 0)

	endInt, _ := conv.Int64(c.Param("end"))
	end := time.Unix(endInt, 0)

	if (endInt - startInt) >= 60*60*24*31 {
		c.String(400, "Range too wide")
	}

	if (endInt - startInt) <= 0 {
		c.String(400, "Range too narrow or negative")
	}

	result := api.metrics.GetMetrics(c.Param("id"), c.Param("source"), c.Param("metric"), start, end)

	maxlen := 200
	if len(result) >= maxlen {
		l := len(result)
		skip := l / maxlen
		count := 0
		var new []db.MetricSimple
		if skip >= 2 {
			for {
				if count >= l {
					break
				}
				new = append(new, result[count])
				count = count + skip
			}
			sort.Slice(new, func(i, j int) bool {
				return new[i].Time.Before(new[j].Time)
			})
			c.JSON(200, new)
			return
		}
	}

	sort.Slice(result, func(i, j int) bool {
		return result[i].Time.Before(result[j].Time)
	})
	c.JSON(200, result)
	return
}
