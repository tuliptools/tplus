package api

type RegisterInput struct {
	Username   string `form:"username" json:"username" `
	Password   string `form:"password" json:"password" `
	InviteCode string `form:"invite" json:"invite" binding:"min=0,max=25"`
}

type TokenRequest struct {
	Username string `form:"username" json:"username" `
	Password string `form:"password" json:"password" `
}

type AddEnvInput struct {
	Name           string `form:"name" json:"name" `
	Network        string `form:"network" json:"network" `
	HistoryMode    string `form:"historyMode" json:"historyMode" `
	SnapshotUrl    string `form:"snapshotURL" json:"snapshotURL" `
	SnapshotSource string `form:"SnapshotSource" json:"SnapshotSource" `
	BackupSource   string `form:"BackupSource" json:"BackupSource" `
	BackupUrl      string `form:"backupURL" json:"backupURL" `
	Sandboxed      bool   `form:"sandbox" json:"sandbox" `
	FromSnapshot   bool   `form:"fromSnapshot" json:"fromSnapshot" `
	FromBackup     bool   `form:"fromBackup" json:"fromBackup" `
	RAM            uint   `form:"resourcesMemory" json:"resourcesMemory" `
	CPU            uint   `form:"resourcesCPU" json:"resourcesCPU" `
	Shared         bool   `form:"shared" json:"shared" `
	SandboxParams  string `form:"sandboxParams" json:"sandboxParams" `
	SandboxConfig  string `form:"sandboxConfig" json:"sandboxConfig" `
	Protocol       string `form:"protocol" json:"protocol" `
}

type AddRemoteInput struct {
	Name        string   `form:"name" json:"name" `
	Shared      bool     `form:"shared" json:"shared" `
	RemoteNodes []string `form:"remotes" json:"remotes" `
}

type Token struct {
	Token string
}

type ContainerLS struct {
	Name    string
	ID      string
	Image   string
	Command string
	Created string
	Status  string
}

type Info struct {
	LoggedIn   bool
	ServerMode string
	UserID     string
	UserRole   string
}
