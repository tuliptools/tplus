package api

import (
	linuxproc "github.com/c9s/goprocinfo/linux"
	"github.com/gin-gonic/gin"
	"github.com/mackerelio/go-osstat/memory"
	"github.com/shirou/gopsutil/cpu"
	"os"
	"time"
)

func (api *Api) HostStatRoutes(r *gin.Engine) {
	r.GET("/os/memory", api.authUser(api.GetHostMemoryStats))
	r.GET("/os/proc", api.authUser(api.GetHostProc))
	r.GET("/os/cpu", api.authUser(api.GetCPUStat))
}

func (api *Api) GetHostMemoryStats(c *gin.Context) {
	res := HostMemory{}

	m, err := memory.Get()
	if err != nil {
		res.Error = err.Error()
		res.Success = false
		c.JSON(200, res)
		return
	}
	res.Total = m.Total
	res.Used = m.Used
	res.Cached = m.Cached
	res.Free = m.Free
	c.JSON(200, res)
}

func (api *Api) GetHostProc(c *gin.Context) {
	res := HostProc{}

	stat, err := linuxproc.ReadStat("/proc/stat")
	if err != nil {
		res.Error = err.Error()
		res.Success = false
		c.JSON(200, res)
		return
	}
	res.CPUCount = len(stat.CPUStats)
	res.Processes = stat.Processes
	res.ProcessesBlocked = stat.ProcsBlocked
	res.ProcessesRunning = stat.ProcsRunning
	res.BootTime = stat.BootTime
	res.Hostname, _ = os.Hostname()
	c.JSON(200, res)
}

func (api *Api) GetCPUStat(c *gin.Context) {
	f, e := cpu.Percent(time.Second, false)
	if e != nil {
		c.String(500, e.Error())
		return
	}
	res := CpuStats{
		Total: f[0],
	}
	c.JSON(200, res)
}

type CpuStats struct {
	Error string
	Total float64
}

type HostProc struct {
	Error            string
	Success          bool
	CPUCount         int
	Processes        uint64
	ProcessesRunning uint64
	ProcessesBlocked uint64
	BootTime         time.Time
	Hostname         string
}

type HostMemory struct {
	Error   string
	Success bool
	Total   uint64
	Used    uint64
	Cached  uint64
	Free    uint64
}
