package api

import (
	"github.com/gin-gonic/gin"
)

func (api *Api) registerPeerRoutes(r *gin.Engine) {
	r.GET("/env/:id/peers", api.authUser(api.getPeers))
}

func (api *Api) getPeers(c *gin.Context) {
	envid := c.Param("id")

	peers, _ := api.tezos.GetPeers(envid)

	c.JSON(200, peers)

	return
}
