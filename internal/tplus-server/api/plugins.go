package api

import (
	"bufio"
	"encoding/json"
	"github.com/gin-gonic/gin"
)

func (api *Api) pluginRoutes(r *gin.Engine) {
	r.POST("/plugins/:id/register", api.authUser(api.RegisterPlugin))
	r.POST("/plugins/:id/stop", api.authUser(api.StopPlugin))
	r.POST("/plugins/:id/start", api.authUser(api.StartPlugin))
	r.POST("/plugins/:id/rm", api.authUser(api.DeletePlugin))
	r.GET("/plugins/:id", api.authUser(api.GetPlugin))
	r.GET("/plugins/:id/logs/:service", api.authUser(api.GetPluginServiceLogs))
}

func (api *Api) RegisterPlugin(c *gin.Context) {
	type B struct {
		ServiceDefinition string `form:"serviceDefinition" json:"serviceDefinition"`
	}
	fa, _ := c.GetRawData()
	body := B{}
	json.Unmarshal(fa, &body)
	env, err := api.envS.GetEnvironment(api.User(c), c.Param("id"))
	if err != nil {
		api.error(err, c)
		return
	}
	e := api.plugins.RegisterPlugin(&env, body.ServiceDefinition)

	if e != nil {
		c.String(500, e.Error())
	} else {
		c.String(200, "ok")
	}

}

func (api *Api) GetPlugin(c *gin.Context) {
	p := api.plugins.GetPluginsManager(c.Param("id"))
	if p == nil {
		c.String(400, "Plugin not found")
	} else {
		c.JSON(200, p.GetPlugin())
	}
}

func (api *Api) GetPluginServiceLogs(c *gin.Context) {
	p := api.plugins.GetPluginsManager(c.Param("id"))
	if p == nil {
		c.String(400, "Plugin not found")
	} else {
		for _, s := range p.GetPlugin().Services {
			if s.Name == c.Param("service") {
				reader, err := api.docker.GetLogs(s.ContainerId, false, c.Param("tail"))
				var buffer []string
				scanner := bufio.NewScanner(reader)
				if err == nil {
					for scanner.Scan() {
						text := scanner.Text()
						if len(text) >= 8 {
							buffer = append(buffer, text[8:])
						}
					}
				}
				if len(buffer) >= 1 {
					c.JSON( 200, buffer)
				} else {
					c.JSON(200,[]string{"No logs availalbe right now...."})
				}
			}
		}
	}
}

func (api *Api) StopPlugin(c *gin.Context) {
	p := api.plugins.GetPluginsManager(c.Param("id"))
	if p == nil {
		c.String(400, "Plugin not found")
	} else {
		go p.Stop()
		c.String(200, "ok")
	}
}

func (api *Api) StartPlugin(c *gin.Context) {
	p := api.plugins.GetPluginsManager(c.Param("id"))
	if p == nil {
		c.String(400, "Plugin not found")
	} else {
		go p.Start()
		c.String(200, "ok")
	}
}

func (api *Api) DeletePlugin(c *gin.Context) {
	p := api.plugins.GetPluginsManager(c.Param("id"))
	if p == nil {
		c.String(400, "Plugin not found")
	} else {
		go p.Delete()
		c.String(200, "ok")
	}
}
