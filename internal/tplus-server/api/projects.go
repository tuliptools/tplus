package api

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/models"
	"time"
)

func (api *Api) projectRoutes(r *gin.Engine) {
	r.GET("/env/:id/projects", api.authUser(api.GetProjects))
	r.GET("/env/:id/projects_importable", api.authUser(api.GetImportProjects))
	r.GET("/env/:id/projects/:pid", api.authUser(api.GetProject))
	r.POST("/projects/:id/new", api.authUser(api.CreateProject))
	r.POST("/projects/:id/new_git", api.authUser(api.CreateProjectGit))
	r.POST("/projects/:id/new_import", api.authUser(api.CreateProjectImport))
	r.GET("/projects/:id/:pid/rm", api.authUser(api.DeleteProject))
	r.GET("/project/:id/:pid/files", api.authUser(api.ListFiles))
	r.GET("/project/:id/:pid/tasks", api.authUser(api.GetTasks))
	r.GET("/project/:id/:pid/variables", api.authUser(api.GetVariables))
	r.POST("/project/:id/:pid/variables", api.authUser(api.SetVariables))
	r.GET("/project/:id/:pid/tasks/logs", api.authUser(api.GetTasksLogs))
	r.GET("/project/:id/:pid/run/:task", api.authUser(api.RunTask))
	r.GET("/project/:id/:pid/get/:taskid", api.authUser(api.GetTask))
	r.POST("/project/:id/:pid/read", api.authUser(api.ReadFile))
	r.GET("/project/:id/:pid/down", api.authUser(api.Down))
	r.POST("/project/:id/:pid/up", api.authUser(api.Up))
}

func (api *Api) GetProjects(c *gin.Context) {
	env, err := api.envS.GetEnvironment(api.User(c), c.Param("id"))
	if err != nil {
		api.error(err, c)
		return
	}
	projects := api.projects.GetProjectsForEnv(&env)
	var ps []struct {
		Name        string
		Id          string
		Description string
		Alias       string
		Source      string
	}
	for _, p := range projects {
		if p != nil {
			ps = append(ps, struct {
				Name        string
				Id          string
				Description string
				Alias       string
				Source      string
			}{
				Description: p.GetProject().Description,
				Alias:       p.GetProject().Alias,
				Name:        p.GetProject().Name,
				Id:          p.GetProject().Id,
			})
		}
	}
	c.JSON(200, ps)
}

func (api *Api) GetImportProjects(c *gin.Context) {
	env, err := api.envS.GetEnvironment(api.User(c), c.Param("id"))
	if err != nil {
		api.error(err, c)
		return
	}
	projects := api.projects.GetProjectsExceptEnv(&env)
	envs, _ := api.db.GetEnvironments()
	getName := func(id string) string {
		for _, a := range envs {
			if a.Id == id {
				return a.Name
			}
		}
		return "NA"
	}
	var ps []struct {
		Name        string
		Id          string
		Description string
		Alias       string
		Source      string
		EnvName     string
	}
	for _, p := range projects {
		envname := getName(p.GetProject().EnvId)
		if p != nil && envname != "NA" {
			ps = append(ps, struct {
				Name        string
				Id          string
				Description string
				Alias       string
				Source      string
				EnvName     string
			}{
				Description: p.GetProject().Description,
				Alias:       p.GetProject().Alias,
				Name:        p.GetProject().Name,
				Id:          p.GetProject().Id,
				EnvName:     envname,
			})
		}
	}
	c.JSON(200, ps)
}

func (api *Api) GetProject(c *gin.Context) {
	env, err := api.envS.GetEnvironment(api.User(c), c.Param("id"))
	if err != nil {
		api.error(err, c)
		return
	}
	project := api.projects.GetProjectByEnvId(&env, c.Param("pid"))
	c.JSON(200, project.GetProject())
}

func (api *Api) GetTask(c *gin.Context) {
	env, err := api.envS.GetEnvironment(api.User(c), c.Param("id"))
	if err != nil {
		api.error(err, c)
		return
	}
	project := api.projects.GetProjectByEnvId(&env, c.Param("pid"))
	task, e := project.GetTask(c.Param("taskid"))
	if e != nil {
		api.error(e, c)
		return
	}
	c.JSON(200, task)
}

func (api *Api) CreateProject(c *gin.Context) {
	params := struct {
		Name        string `json:"name"`
		Description string `json:"description"`
		Alias       string `json:"alias"`
		Source      string `json:"source"`
		Project     string `json:"project"`
	}{}
	if e := c.Bind(&params); e != nil {
		api.error(e, c)
		return
	}
	env, err := api.envS.GetEnvironment(api.User(c), c.Param("id"))
	if err != nil {
		api.error(err, c)
		return
	}
	// todo basic params valdiation
	epm, e := api.projects.CreateProject(&env, params.Name, params.Description, params.Alias, params.Source, params.Project)
	if e != nil {
		api.error(e, c)
		return
	}
	c.JSON(200, epm.GetProject().Id)
}

func (api *Api) CreateProjectGit(c *gin.Context) {
	params := struct {
		Name        string `json:"name"`
		Description string `json:"description"`
		Repourl     string `json:"repourl"`
	}{}
	if e := c.Bind(&params); e != nil {
		api.error(e, c)
		return
	}
	env, err := api.envS.GetEnvironment(api.User(c), c.Param("id"))
	if err != nil {
		api.error(err, c)
		return
	}
	// todo basic params valdiation
	_, e := api.projects.CreateProjectGit(&env, params.Name, params.Description, "", params.Repourl)
	if e != nil {
		api.error(e, c)
		return
	}
	c.JSON(200, "ok")
}

func (api *Api) CreateProjectImport(c *gin.Context) {
	params := struct {
		Name        string `json:"name"`
		Description string `json:"description"`
		ProjectID   string `json:"projectId"`
	}{}
	if e := c.Bind(&params); e != nil {
		api.error(e, c)
		return
	}
	env, err := api.envS.GetEnvironment(api.User(c), c.Param("id"))
	if err != nil {
		api.error(err, c)
		return
	}
	// todo basic params valdiation
	_, e := api.projects.CreateProjectImport(&env, params.Name, params.Description, "", params.ProjectID)
	if e != nil {
		api.error(e, c)
		return
	}
	c.JSON(200, "ok")
}

func (api *Api) DeleteProject(c *gin.Context) {
	env, err := api.envS.GetEnvironment(api.User(c), c.Param("id"))
	if err != nil {
		api.error(err, c)
		return
	}
	api.projects.DeleteProject(&env, c.Param("pid"))
	c.JSON(200, "ok")
}

func (api *Api) ListFiles(c *gin.Context) {
	env, err := api.envS.GetEnvironment(api.User(c), c.Param("id"))
	if err != nil {
		api.error(err, c)
		return
	}
	project := api.projects.GetProjectByEnvId(&env, c.Param("pid"))
	c.JSON(200, project.GetFiles())
}

func (api *Api) GetTasks(c *gin.Context) {
	env, err := api.envS.GetEnvironment(api.User(c), c.Param("id"))
	if err != nil {
		api.error(err, c)
		return
	}
	project := api.projects.GetProjectByEnvId(&env, c.Param("pid"))
	task, e := project.GetTasks()
	if e != nil {
		api.error(e, c)
		return
	}
	c.JSON(200, task)
}

func (api *Api) GetVariables(c *gin.Context) {
	env, err := api.envS.GetEnvironment(api.User(c), c.Param("id"))
	if err != nil {
		api.error(err, c)
		return
	}
	project := api.projects.GetProjectByEnvId(&env, c.Param("pid"))
	one, two, e := project.GetVariables()
	if e != nil {
		api.error(e, c)
		return
	}
	res := struct {
		Variables []models.TaskVariable
		Compiled  map[string]string
	}{
		one,
		two,
	}
	c.JSON(200, res)
}

func (api *Api) SetVariables(c *gin.Context) {
	params := struct {
		Variables map[string]string `json:"variables"`
	}{}
	c.BindJSON(&params)
	env, err := api.envS.GetEnvironment(api.User(c), c.Param("id"))
	if err != nil {
		api.error(err, c)
		return
	}
	project := api.projects.GetProjectByEnvId(&env, c.Param("pid"))
	project.SetVariables(params.Variables)
	c.String(200, "ok")
	time.Sleep(1 * time.Second)
}

func (api *Api) GetTasksLogs(c *gin.Context) {
	env, err := api.envS.GetEnvironment(api.User(c), c.Param("id"))
	if err != nil {
		api.error(err, c)
		return
	}
	project := api.projects.GetProjectByEnvId(&env, c.Param("pid"))
	task, e := project.GetTaskLogs()
	if e != nil {
		api.error(e, c)
		return
	}
	c.JSON(200, task)
}

func (api *Api) RunTask(c *gin.Context) {
	env, err := api.envS.GetEnvironment(api.User(c), c.Param("id"))
	if err != nil {
		api.error(err, c)
		return
	}
	project := api.projects.GetProjectByEnvId(&env, c.Param("pid"))
	task, e := project.RunTask(c.Param("task"))
	if e != nil {
		api.error(e, c)
		return
	}
	c.JSON(200, task)
}

func (api *Api) ReadFile(c *gin.Context) {
	params := struct {
		Name string `json:"name"`
	}{}
	c.BindJSON(&params)
	env, err := api.envS.GetEnvironment(api.User(c), c.Param("id"))
	if err != nil {
		api.error(err, c)
		return
	}
	project := api.projects.GetProjectByEnvId(&env, c.Param("pid"))
	f, e := project.GetFile(params.Name)
	if e != nil {
		api.error(e, c)
		return
	}
	c.String(200, f)
}

func (api *Api) Down(c *gin.Context) {
	params := struct {
		Name string `json:"name"`
	}{}
	c.BindJSON(&params)
	env, err := api.envS.GetEnvironment(api.User(c), c.Param("id"))
	if err != nil {
		api.error(err, c)
		return
	}
	project := api.projects.GetProjectByEnvId(&env, c.Param("pid"))
	r, e := project.Down()
	if e != nil {
		api.error(e, c)
		return
	}
	contentlen := int64(r.Len())
	contentType := "application/octet-stream"
	extraHeaders := map[string]string{
	}
	c.DataFromReader(200, contentlen, contentType, r, extraHeaders)

}

func (api *Api) Up(c *gin.Context) {
	env, err := api.envS.GetEnvironment(api.User(c), c.Param("id"))
	if err != nil {
		api.error(err, c)
		return
	}
	project := api.projects.GetProjectByEnvId(&env, c.Param("pid"))
	project.Up(c.Request.Body)
	c.String(200, "ok")
}
