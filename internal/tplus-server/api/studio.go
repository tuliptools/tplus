package api

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/models"
)

func (api *Api) studioRoutes(r *gin.Engine) {
	r.GET("/studio/:id/:pid", func(c *gin.Context) {
		api.Studiowshandler(c.Writer, c.Request, c.Param("id"), c.Param("pid"))
	})
}

type SocketMessage struct {
	Action      string     `json:"action"`
	File        string     `json:"file"`
	OldFile     string     `json:"oldFile"`
	Dir         string     `json:"dir"`
	Destination string     `json:"destination"`
	Content     string     `json:"content"`
	API         APIRequest `json:"api"`
	Task        string     `json:"task"`
}

type APIRequest struct {
	Url    string `json:"url"`
	Method string `json:"method"`
	Body   string `json:"body"`
	ID     string `json:"id"`
}

type StudioMessage struct {
	Mutation string
	Data     interface{}
}

var Studiowsupgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

func (api *Api) Studiowshandler(w http.ResponseWriter, r *http.Request, envId string, pId string) {

	fmt.Println("INIT")

	env, err := api.db.GetEnvironmentById(envId)
	if err != nil {
		fmt.Println(err)
		return
	}
	project := api.projects.GetProjectByEnvId(env, pId)

	if env == nil || project == nil || env.Tezos.Status != "running" {
		fmt.Println("error2")
		return
	}

	wsupgrader.CheckOrigin = func(r *http.Request) bool {
		return true
	}
	conn, err := wsupgrader.Upgrade(w, r, nil)
	if err != nil {
		fmt.Println("Failed to set websocket upgrade: %+v", err)
		return
	}
	go api.WsPing(conn)
	go api.Filecheck(conn, envId, pId)
	go api.Varscheck(conn, envId, pId)
	go api.WalletCheck(conn, envId)
	go api.ServiceCheck(conn, envId, pId)

	for {
		t, msg, err := conn.ReadMessage()
		if err != nil {
			break
		}
		if t == 1 {
			m := SocketMessage{}
			json.Unmarshal(msg, &m)
			if m.Action == "openFile" {
				f, e := project.GetFile(m.File)
				if e == nil {
					go api.StudioMessageNoCache(conn, "openFile", map[string]string{
						"name":    m.File,
						"content": f,
					})
				}

			}

			if m.Action == "editFile" {
				project.WriteFile(m.File, m.Content)
				if strings.Contains(m.File, "tasks/") {
					tasks, e := project.GetTasks()
					if e == nil {
						for i, _ := range tasks {
							tasks[i].Tasks = []models.RunnableTask{} // dont need this in the gui
						}
						go api.StudioMessage(conn, "setTasks", tasks)
					}
				}
			}

			if m.Action == "requestTasks" {
				tasks, e := project.GetTasks()
				if e == nil {
					go api.StudioMessage(conn, "setTasks", tasks)
				}
			}

			if m.Action == "makeAPIRequest" {
				go func(m SocketMessage, api *Api, conn *websocket.Conn) {
					type ApiReponse struct {
						Id   string
						Body string
						Time time.Time
					}
					res, e := project.MakeApiRequest(m.API.Url, m.API.Body, m.API.Method)
					msg := ApiReponse{}
					msg.Id = m.API.ID
					msg.Time = time.Now()
					if e == nil {
						msg.Body = res
					} else {
						msg.Body = "Error: " + e.Error()
					}
					b, _ := json.Marshal(msg)
					go api.StudioMessage(conn, "apiResponse", string(b))
				}(m, api, conn)
			}

			if m.Action == "requestTaskLogs" {
				tasks, e := project.GetTaskLogs()
				if e == nil {
					go api.StudioMessage(conn, "setTaskLogs", tasks)
				}
			}

			if m.Action == "requestServices" {
				s := project.GetServices()
				go api.StudioMessageNoCache(conn, "setServices", s)
			}

			if m.Action == "requestSingleTaskLogs" {
				tasks, e := project.GetTaskLogs()
				if e == nil {
					go api.StudioMessageNoCache(conn, "setTaskLogs", tasks)
				}
			}

			if m.Action == "requestFiles" {
				files := project.GetFiles()
				go api.StudioMessageNoCache(conn, "file_list", files)
			}

			if m.Action == "newFile" {
				project.WriteFile(m.File, " ")
				files := project.GetFiles()
				go api.StudioMessageNoCache(conn, "file_list", files)
			}

			if m.Action == "createDir" {
				project.Mkdir(m.File)
				files := project.GetFiles()
				go api.StudioMessageNoCache(conn, "file_list", files)
			}

			if m.Action == "delFile" {
				project.RemoveFile(m.File)
				files := project.GetFiles()
				go api.StudioMessageNoCache(conn, "file_list", files)
			}

			if m.Action == "moveDir" {
				project.RemoveFile(m.File)
				files := project.GetFiles()
				go api.StudioMessageNoCache(conn, "file_list", files)
			}

			if m.Action == "delDir" {
				project.RemoveDir(m.File)
				files := project.GetFiles()
				go api.StudioMessageNoCache(conn, "file_list", files)
			}

			if m.Action == "renameFile" {
				project.RenameFile(m.OldFile, m.File)
				files := project.GetFiles()
				go api.StudioMessageNoCache(conn, "file_list", files)
			}

			if m.Action == "runTask" {
				task, e := project.RunTask(m.File)
				if e == nil {
					api.StudioMessage(conn, "setRunningTask", task)
					go func() {
						for {
							api.StudioMessage(conn, "updateTask", task)
							if task.Finished {
								break
							}
							time.Sleep(600 * time.Millisecond)
						}
					}()
				}
			}

		}
	}

}

func (api *Api) StudioMessage(conn *websocket.Conn, mutation string, data interface{}) error {
	api.wsLock.Lock()
	defer api.wsLock.Unlock()
	msg := StudioMessage{
		Mutation: mutation,
		Data:     data,
	}
	m, _ := json.Marshal(&msg)
	// deduplicate
	key := conn.UnderlyingConn().RemoteAddr().String() + mutation
	if val, ok := api.msgCache[key]; !ok || val != string(m) || true {
		api.msgCache[key] = string(m)
		return conn.WriteMessage(websocket.TextMessage, m)
	}
	return nil
}

func (api *Api) StudioMessageNoCache(conn *websocket.Conn, mutation string, data interface{}) error {
	api.wsLock.Lock()
	defer api.wsLock.Unlock()
	msg := StudioMessage{
		Mutation: mutation,
		Data:     data,
	}
	m, _ := json.Marshal(&msg)
	return conn.WriteMessage(websocket.TextMessage, m)
	return nil
}

func (api *Api) WsPing(conn *websocket.Conn) {
	for {
		e := api.StudioMessage(conn, "ping", time.Now().String())
		if e != nil {
			break
		}
		time.Sleep(10 * time.Second)
	}
}

func (api *Api) Filecheck(conn *websocket.Conn, envid, pid string) {
	env, err := api.db.GetEnvironmentById(envid)
	if err != nil {
		fmt.Println(err)
		return
	}
	project := api.projects.GetProjectByEnvId(env, pid)
	for {
		files := project.GetFiles()
		if len(files) >= 1 {
			if api.StudioMessage(conn, "file_list", files) != nil {
				break
			}
		}
		time.Sleep(5 * time.Second)
	}
}
func (api *Api) Varscheck(conn *websocket.Conn, envid, pid string) {
	env, err := api.db.GetEnvironmentById(envid)
	if err != nil {
		fmt.Println(err)
		return
	}
	project := api.projects.GetProjectByEnvId(env, pid)
	for {
		taskvar, compiled, e := project.GetVariables()
		if e == nil {
			if api.StudioMessage(conn, "varsList", taskvar) != nil {
				break
			}
			if api.StudioMessage(conn, "compiledVars", compiled) != nil {
				break
			}
		}
		time.Sleep(3 * time.Second)
	}
}

func (api *Api) WalletCheck(conn *websocket.Conn, envid string) {
	env, err := api.db.GetEnvironmentById(envid)
	if err != nil {
		fmt.Println(err)
		return
	}
	api.wallet.GetAddresses(env)
	for {
		wallet := api.wallet.GetAddresses(env)
		if api.StudioMessage(conn, "wallet", wallet) != nil {
			break
		}
		time.Sleep(5 * time.Second)
	}
}

func (api *Api) ServiceCheck(conn *websocket.Conn, envid, pid string) {
	env, err := api.db.GetEnvironmentById(envid)
	if err != nil {
		fmt.Println(err)
		return
	}
	project := api.projects.GetProjectByEnvId(env, pid)
	for {
		s := project.GetServices()
		if api.StudioMessage(conn, "setServices", s) != nil {
			break
		}
		time.Sleep(5 * time.Second)
	}
}
