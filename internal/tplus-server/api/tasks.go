package api

import (
	"github.com/gin-gonic/gin"
)

func (api *Api) registerTaskRoutes(r *gin.Engine) {
	r.GET("/env/:id/tasks", api.authUser(api.getTasks))
	r.GET("/env/:id/tasks/:taskId", api.authUser(api.getTask))
}

func (api *Api) getTasks(c *gin.Context) {
	e, _ := api.envS.GetEnvironment(api.User(c), c.Param("id"))
	tasks, _ := api.db.GetTasksForEnvironment(e.Id)
	if tasks != nil {
		c.JSON(200, tasks)
	}
}

func (api *Api) getTask(c *gin.Context) {
	env, e := api.envS.GetEnvironment(api.User(c), c.Param("id"))
	if e == nil && env.Id != "" {
		tasks, _ := api.db.GetTaskById(c.Param("taskId"))
		c.JSON(200, tasks)
	} else {
		c.String(404, "task not found")
	}
}
