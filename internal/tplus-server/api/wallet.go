package api

import (
	"github.com/gin-gonic/gin"
)

func (api *Api) walletRoutes(r *gin.Engine) {
	r.POST("/wallet/:id/new/:name", api.authUser(api.NewAddress))
	r.POST("/wallet/:id/alias/:name/:key", api.authUser(api.NewAlias))
	r.POST("/wallet/:id/faucet/:name", api.authUser(api.NewFaucet))
	r.GET("/wallet/:id/remove/:name", api.authUser(api.DeleteKey))
	r.POST("/wallet/:id/delegate/:from/:to", api.authUser(api.Delegate))
	r.POST("/wallet/:id/transfer/:from/:to/:amount", api.authUser(api.Transfer))
	r.GET("/wallet/:id/list", api.authUser(api.GetAddresses))
	r.GET("/wallet/:id/pending", api.authUser(api.GetPendingOps))
	r.GET("/wallet/:id/bake", api.authUser(api.Bake))
}

func (api *Api) GetAddresses(c *gin.Context) {
	envs, err := api.envS.GetEnvironment(api.User(c), c.Param("id"))
	if err != nil {
		api.error(err, c)
		return
	}

	res := api.wallet.GetAddresses(&envs)
	c.JSON(200, res)
}

func (api *Api) Delegate(c *gin.Context) {
	envs, err := api.envS.GetEnvironment(api.User(c), c.Param("id"))
	if err != nil {
		api.error(err, c)
		return
	}

	res := api.wallet.Delegate(&envs, c.Param("from"), c.Param("to"))
	c.JSON(200, res)
}

func (api *Api) Bake(c *gin.Context) {
	envs, err := api.envS.GetEnvironment(api.User(c), c.Param("id"))
	if err != nil {
		api.error(err, c)
		return
	}
	res := api.wallet.Bake(&envs)
	c.JSON(200, res)
}

func (api *Api) Transfer(c *gin.Context) {
	envs, err := api.envS.GetEnvironment(api.User(c), c.Param("id"))
	if err != nil {
		api.error(err, c)
		return
	}

	res := api.wallet.Transfer(&envs, c.Param("from"), c.Param("to"), c.Param("amount"))
	c.JSON(200, res)
}

func (api *Api) GetPendingOps(c *gin.Context) {
	envs, err := api.envS.GetEnvironment(api.User(c), c.Param("id"))
	if err != nil {
		api.error(err, c)
		return
	}

	res := api.wallet.GetPendingOps(&envs)
	c.JSON(200, res)
}

func (api *Api) NewAddress(c *gin.Context) {
	envs, err := api.envS.GetEnvironment(api.User(c), c.Param("id"))
	if err != nil {
		api.error(err, c)
		return
	}

	res := api.wallet.NewAddress(&envs, c.Param("name"))
	c.JSON(200, res)
}

func (api *Api) NewAlias(c *gin.Context) {
	envs, err := api.envS.GetEnvironment(api.User(c), c.Param("id"))
	if err != nil {
		api.error(err, c)
		return
	}

	res := api.wallet.NewAlias(&envs, c.Param("name"), c.Param("key"))
	c.JSON(200, res)
}

func (api *Api) DeleteKey(c *gin.Context) {
	envs, err := api.envS.GetEnvironment(api.User(c), c.Param("id"))
	if err != nil {
		api.error(err, c)
		return
	}

	res := api.wallet.DeleteKey(&envs, c.Param("name"))
	c.JSON(200, res)
}

func (api *Api) NewFaucet(c *gin.Context) {
	type B struct {
		File string `json:"json",form:"json"`
	}
	body := B{}
	c.BindJSON(&body)
	envs, err := api.envS.GetEnvironment(api.User(c), c.Param("id"))
	if err != nil {
		api.error(err, c)
		return
	}

	res := api.wallet.NewFaucet(&envs, c.Param("name"), []byte(body.File))
	c.JSON(200, res)
}

type Address struct {
	Address string
	Balance uint64
	Alias   string
}
