package config

import (
	"bytes"
	"fmt"
	"github.com/gobuffalo/packr"
	"github.com/spf13/viper"
	"github.com/tyler-sommer/stick"
	"log"
	"os"
)

func ReadConfig(box *packr.Box) *Config {

	viper.SetConfigName("server")

	if os.Getenv("TPLUS_SERVER_CONFIG_PATH") == "" {
		viper.AddConfigPath("$HOME/.tplus")
	} else {

		viper.AddConfigPath(os.Getenv("TPLUS_SERVER_CONFIG_PATH"))
	}

	err := viper.ReadInConfig()
	if err != nil {
		fmt.Println("No config file found!!")
		fmt.Println("Run `tplus-server config init`")
		os.Exit(1)

	}

	c := Config{}
	err = viper.Unmarshal(&c)
	if err != nil {
		fmt.Printf("unable to read config File, %v", err)
	}

	if c.DataDir == "" ||
		c.Mode == "" ||
		c.PublicIp == "" ||
		c.EndpointMode == "" {
		fmt.Println("ERROR: invalid config file")
		fmt.Println("Try: tplus-server config init")
		os.Exit(1)
	}
	c.Box = box

	os.MkdirAll(c.DataDir, 0777)

	return &c
}

func (config *Config) Save() {

	var hd string

	if os.Getenv("TPLUS_SERVER_CONFIG_PATH") == "" {
		hd, _ = os.UserHomeDir() // no trailing /
	} else {
		hd = os.Getenv("TPLUS_SERVER_CONFIG_PATH")
	}

	path := hd + "/.tplus"

	err := os.MkdirAll(path, 0700)
	if err != nil {
		fmt.Println("Could not create config directory " + path)
		fmt.Println("Please make sure config directory exists and retry")
		os.Exit(1)
	}

	os.Remove(path + "/server.yml")
	data, e := config.Box.Find("config.yml.twig")
	f, e := os.OpenFile(path+"/server.yml", os.O_CREATE|os.O_RDWR, 0700)
	if e != nil {
		fmt.Println("Could not create config file in " + path)
		os.Exit(1)
	}

	ua := "false"
	if config.EnableUserAuth {
		ua = "true"
	}
	varmap := map[string]stick.Value{}
	varmap["datadir"] = config.DataDir
	varmap["mode"] = config.Mode
	varmap["webport"] = config.WebPort
	varmap["endpointmode"] = config.EndpointMode
	varmap["endpointport"] = config.EndpointPort
	varmap["publicip"] = config.PublicIp
	varmap["enableuser"] = ua
	varmap["logtype"] = config.LogType

	buf := &bytes.Buffer{}
	env := stick.New(nil)
	if err := env.Execute(string(data), buf, varmap); err != nil {
		log.Fatal(err)
	}

	f.Write(buf.Bytes())
	f.Close()

}
