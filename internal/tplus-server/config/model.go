package config

import "github.com/gobuffalo/packr"

type Config struct {
	Mode           string
	DataDir        string
	ApiPort        int
	WebPort        int
	EndpointPort   string
	EndpointMode   string
	PublicIp       string
	EnableUserAuth bool
	LogType        string
	Box            *packr.Box
}

type RegisterSettings struct {
	Enabled           bool
	ReCaptchaEnabled  bool
	ReCaptchaCode     string
	AllowPublicNodes  bool
	MaxEnvs           int
	RequireInviteCode bool
}
