package db

import (
	"errors"
	"fmt"
	"github.com/BurntSushi/locker"
	lru "github.com/hashicorp/golang-lru"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/config"
	bolt "go.etcd.io/bbolt"
	"os"
	"sort"
	"strings"
	"time"
)

func NewBoltDb(config *config.Config) *bolt.DB {
	file := config.DataDir + "/db/bolt.db"
	os.MkdirAll(config.DataDir+"/db", 0777)
	boltdb, err := bolt.Open(file, 0777, &bolt.Options{Timeout: 1 * time.Second})
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	return boltdb
}

type DB struct {
	StorageDir string
	bolt       *bolt.DB
	locks      *locker.Locker
	lru        *lru.Cache
}

func NewDB(boltdb *bolt.DB) *DB {
	d := DB{}
	d.bolt = boltdb
	d.locks = locker.NewLocker()
	d.lru,_ = lru.New(100)

	_ = d.bolt.Update(func(tx *bolt.Tx) error {
		_, _ = tx.CreateBucket([]byte("Task"))
		_, _ = tx.CreateBucket([]byte("InviteCode"))
		_, _ = tx.CreateBucket([]byte("Environment"))
		_, _ = tx.CreateBucket([]byte("User"))
		_, _ = tx.CreateBucket([]byte("Token"))
		_, _ = tx.CreateBucket([]byte("DockerImage"))
		_, _ = tx.CreateBucket([]byte("Project"))
		_, _ = tx.CreateBucket([]byte("ProjectTask"))

		return nil
	})

	return &d
}

func (db *DB) Create(obj Model) error {
	db.lru.Add(obj.GetId(),obj.Marshal())
	obj.SetCreatedNow()
	obj.setLastSaveNow()
	obj.setLastReadNow()
	return db.bolt.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket(obj.GetBucket())
		err := b.Put([]byte(obj.GetId()), obj.Marshal())
		return err
	})
}


func (db *DB) Save(obj Model) error {
	db.lru.Add(obj.GetId(),obj.Marshal())
	obj.setLastSaveNow()
	return db.bolt.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket(obj.GetBucket())
		err := b.Put([]byte(obj.GetId()), obj.Marshal())
		return err
	})
}


func (db *DB) GetTasksForEnvironment(envid string) ([]Task,error){
	res := []Task{}
	var e error
	tasks,e := db.GetTasks()
	for _, t := range tasks {
		if strings.ToLower(t.EnvironmentId) == strings.ToLower(envid) && t.State != "registered" {
			res = append(res, t)
		}
	}

	if e != nil {
		return res,e
	}

	sort.Slice(res, func(i, j int) bool {
		return res[j].Created.Before(res[i].Created)
	})

	return res,e
}

func (db *DB) GetUserByUsername(name string) (*User,error){
	res := User{}
	var e error
	users,e := db.GetUsers()
	for _,u := range users {
		if strings.ToLower(u.Username) == strings.ToLower(name) {
			res = u
			break
		}
	}

	if e == nil && res.Username == "" {
		e = errors.New("Username not found")
	}

	return &res,e
}

func (db *DB) Delete(obj Model) error {
	return db.bolt.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket(obj.GetBucket())
		return b.Delete([]byte(obj.GetId()))
	})
}

func (db *DB) GetDockerImage(id string) (*DockerImage,error) {
	res := DockerImage{}
	e := db.bolt.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("DockerImage"))
		v := b.Get([]byte(id))
		res.UnMarshal(v)
		return nil
	})
	if res.Id == "" {
		res.Id = id
		db.Save(&res)
	}
	return &res,e
}

func (db *DB) DeleteEnvironment(envid string) error {
	e := db.bolt.Update(func(tx *bolt.Tx) error {
		tx.Bucket([]byte("Environment")).Delete([]byte(envid))
		return nil
	})
	return e
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Generated methods


// generated for Environment



func (db *DB) UpdateEnvironment(val *Environment, f func(update *Environment) *Environment) {
	old, e := db.GetEnvironmentById(val.GetId())
	if e == nil {
		db.locks.Lock(val.GetId())
    	defer db.locks.Unlock(val.GetId())
		n := f(old)
		db.Save(n)
	}
}


// generated for Environment

func (db *DB) GetEnvironments() ([]Environment,error) {
	res := []Environment{}

	e := db.bolt.View(func(tx *bolt.Tx) error {
		// Assume bucket exists and has keys
		b := tx.Bucket([]byte("Environment"))

		c := b.Cursor()

		for k, v := c.First(); k != nil; k, v = c.Next() {
			tmp := Environment{}
			tmp.UnMarshal(v)
			res = append(res, tmp)
		}

		return nil
	})

	return res,e
}

func (db *DB) GetEnvironmentById(id string) (*Environment,error) {
	res := Environment{}

	if val,ok := db.lru.Get(id); ok {
    	res.UnMarshal(val.([]byte))
    	return &res,nil
    }

	e := db.bolt.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("Environment"))
		v := b.Get([]byte(id))
		res.UnMarshal(v)
		return nil
	})
	if res.Id == "" {
		db.Save(&res)
	}
	return &res,e
}



// generated for Environment



func (db *DB) UpdateInviteCode(val *InviteCode, f func(update *InviteCode) *InviteCode) {
	old, e := db.GetInviteCodeById(val.GetId())
	if e == nil {
		db.locks.Lock(val.GetId())
    	defer db.locks.Unlock(val.GetId())
		n := f(old)
		db.Save(n)
	}
}


// generated for InviteCode

func (db *DB) GetInviteCodes() ([]InviteCode,error) {
	res := []InviteCode{}

	e := db.bolt.View(func(tx *bolt.Tx) error {
		// Assume bucket exists and has keys
		b := tx.Bucket([]byte("InviteCode"))

		c := b.Cursor()

		for k, v := c.First(); k != nil; k, v = c.Next() {
			tmp := InviteCode{}
			tmp.UnMarshal(v)
			res = append(res, tmp)
		}

		return nil
	})

	return res,e
}

func (db *DB) GetInviteCodeById(id string) (*InviteCode,error) {
	res := InviteCode{}

	if val,ok := db.lru.Get(id); ok {
    	res.UnMarshal(val.([]byte))
    	return &res,nil
    }

	e := db.bolt.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("InviteCode"))
		v := b.Get([]byte(id))
		res.UnMarshal(v)
		return nil
	})
	if res.Id == "" {
		db.Save(&res)
	}
	return &res,e
}



// generated for Environment



func (db *DB) UpdateUser(val *User, f func(update *User) *User) {
	old, e := db.GetUserById(val.GetId())
	if e == nil {
		db.locks.Lock(val.GetId())
    	defer db.locks.Unlock(val.GetId())
		n := f(old)
		db.Save(n)
	}
}


// generated for User

func (db *DB) GetUsers() ([]User,error) {
	res := []User{}

	e := db.bolt.View(func(tx *bolt.Tx) error {
		// Assume bucket exists and has keys
		b := tx.Bucket([]byte("User"))

		c := b.Cursor()

		for k, v := c.First(); k != nil; k, v = c.Next() {
			tmp := User{}
			tmp.UnMarshal(v)
			res = append(res, tmp)
		}

		return nil
	})

	return res,e
}

func (db *DB) GetUserById(id string) (*User,error) {
	res := User{}

	if val,ok := db.lru.Get(id); ok {
    	res.UnMarshal(val.([]byte))
    	return &res,nil
    }

	e := db.bolt.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("User"))
		v := b.Get([]byte(id))
		res.UnMarshal(v)
		return nil
	})
	if res.Id == "" {
		db.Save(&res)
	}
	return &res,e
}



// generated for Environment



func (db *DB) UpdateToken(val *Token, f func(update *Token) *Token) {
	old, e := db.GetTokenById(val.GetId())
	if e == nil {
		db.locks.Lock(val.GetId())
    	defer db.locks.Unlock(val.GetId())
		n := f(old)
		db.Save(n)
	}
}


// generated for Token

func (db *DB) GetTokens() ([]Token,error) {
	res := []Token{}

	e := db.bolt.View(func(tx *bolt.Tx) error {
		// Assume bucket exists and has keys
		b := tx.Bucket([]byte("Token"))

		c := b.Cursor()

		for k, v := c.First(); k != nil; k, v = c.Next() {
			tmp := Token{}
			tmp.UnMarshal(v)
			res = append(res, tmp)
		}

		return nil
	})

	return res,e
}

func (db *DB) GetTokenById(id string) (*Token,error) {
	res := Token{}

	if val,ok := db.lru.Get(id); ok {
    	res.UnMarshal(val.([]byte))
    	return &res,nil
    }

	e := db.bolt.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("Token"))
		v := b.Get([]byte(id))
		res.UnMarshal(v)
		return nil
	})
	if res.Id == "" {
		db.Save(&res)
	}
	return &res,e
}



// generated for Environment



func (db *DB) UpdateTask(val *Task, f func(update *Task) *Task) {
	old, e := db.GetTaskById(val.GetId())
	if e == nil {
		db.locks.Lock(val.GetId())
    	defer db.locks.Unlock(val.GetId())
		n := f(old)
		db.Save(n)
	}
}


// generated for Task

func (db *DB) GetTasks() ([]Task,error) {
	res := []Task{}

	e := db.bolt.View(func(tx *bolt.Tx) error {
		// Assume bucket exists and has keys
		b := tx.Bucket([]byte("Task"))

		c := b.Cursor()

		for k, v := c.First(); k != nil; k, v = c.Next() {
			tmp := Task{}
			tmp.UnMarshal(v)
			res = append(res, tmp)
		}

		return nil
	})

	return res,e
}

func (db *DB) GetTaskById(id string) (*Task,error) {
	res := Task{}

	if val,ok := db.lru.Get(id); ok {
    	res.UnMarshal(val.([]byte))
    	return &res,nil
    }

	e := db.bolt.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("Task"))
		v := b.Get([]byte(id))
		res.UnMarshal(v)
		return nil
	})
	if res.Id == "" {
		db.Save(&res)
	}
	return &res,e
}



// generated for Environment



func (db *DB) UpdateProject(val *Project, f func(update *Project) *Project) {
	old, e := db.GetProjectById(val.GetId())
	if e == nil {
		db.locks.Lock(val.GetId())
    	defer db.locks.Unlock(val.GetId())
		n := f(old)
		db.Save(n)
	}
}


// generated for Project

func (db *DB) GetProjects() ([]Project,error) {
	res := []Project{}

	e := db.bolt.View(func(tx *bolt.Tx) error {
		// Assume bucket exists and has keys
		b := tx.Bucket([]byte("Project"))

		c := b.Cursor()

		for k, v := c.First(); k != nil; k, v = c.Next() {
			tmp := Project{}
			tmp.UnMarshal(v)
			res = append(res, tmp)
		}

		return nil
	})

	return res,e
}

func (db *DB) GetProjectById(id string) (*Project,error) {
	res := Project{}

	if val,ok := db.lru.Get(id); ok {
    	res.UnMarshal(val.([]byte))
    	return &res,nil
    }

	e := db.bolt.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("Project"))
		v := b.Get([]byte(id))
		res.UnMarshal(v)
		return nil
	})
	if res.Id == "" {
		db.Save(&res)
	}
	return &res,e
}



// generated for Environment



func (db *DB) UpdateProjectTask(val *ProjectTask, f func(update *ProjectTask) *ProjectTask) {
	old, e := db.GetProjectTaskById(val.GetId())
	if e == nil {
		db.locks.Lock(val.GetId())
    	defer db.locks.Unlock(val.GetId())
		n := f(old)
		db.Save(n)
	}
}


// generated for ProjectTask

func (db *DB) GetProjectTasks() ([]ProjectTask,error) {
	res := []ProjectTask{}

	e := db.bolt.View(func(tx *bolt.Tx) error {
		// Assume bucket exists and has keys
		b := tx.Bucket([]byte("ProjectTask"))

		c := b.Cursor()

		for k, v := c.First(); k != nil; k, v = c.Next() {
			tmp := ProjectTask{}
			tmp.UnMarshal(v)
			res = append(res, tmp)
		}

		return nil
	})

	return res,e
}

func (db *DB) GetProjectTaskById(id string) (*ProjectTask,error) {
	res := ProjectTask{}

	if val,ok := db.lru.Get(id); ok {
    	res.UnMarshal(val.([]byte))
    	return &res,nil
    }

	e := db.bolt.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("ProjectTask"))
		v := b.Get([]byte(id))
		res.UnMarshal(v)
		return nil
	})
	if res.Id == "" {
		db.Save(&res)
	}
	return &res,e
}

