package db

import (
	"math/rand"
	"sync"
	"time"
)

var randChars = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890")

var randLock *sync.Mutex

func GetRandomId(prefix string) string {
	if randLock == nil {
		randLock = &sync.Mutex{}
		rand.Seed(time.Now().UTC().UnixNano())
	}
	randLock.Lock()
	b := make([]rune, 16)
	for i := range b {
		b[i] = randChars[rand.Intn(len(randChars))]
	}
	randLock.Unlock()
	return prefix + "-" + string(b)
}

func GetTokenString() string {
	rand.Seed(time.Now().UTC().UnixNano())
	b := make([]rune, 32)
	for i := range b {
		b[i] = randChars[rand.Intn(len(randChars))]
	}
	return "id" + "-" + string(b)
}

func GetRandomString(count int) string {
	if randLock == nil {
		randLock = &sync.Mutex{}
		rand.Seed(time.Now().UTC().UnixNano())
	}
	randLock.Lock()
	b := make([]rune, count)
	for i := range b {
		b[i] = randChars[rand.Intn(len(randChars))]
	}
	randLock.Unlock()
	return string(b)
}
