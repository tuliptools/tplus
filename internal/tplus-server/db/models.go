package db

/*
 Codegened file, do not edit directly
*/

import (
	"encoding/json"
	"golang.org/x/crypto/bcrypt"
	"time"
)


type SyncFile struct {
	Name string
	Hash string
}

type Syncfiles struct {
	EnvId string
	Files []SyncFile
	Initial bool
}

func (obj *Syncfiles) Exists(file string) bool{
	for _,a := range obj.Files {
		if a.Name == file {
			return true
		}
	}
	return false
}

func (obj *Syncfiles) HashMatch(file, hash string) bool{
	for _,a := range obj.Files {
		if a.Hash == hash && a.Name == file {
			return true
		}
	}
	return false
}

type ProjectTask struct {
    ModelBase
	ProjectId string
	Name      string `yaml:"Name"`
	Id        string
	Created   time.Time
	Logs      []string
	Result    bool
	Finished  bool
	Cmd       []string
	LastSaved time.Time
    Tasks       []RunnableTask `yaml:"Tasks"`
    Filename string
    Error string
    CustomDockers map[string]string
    Image       string `yaml:"Image"`
    TplusInfo       TplusInfo
    Variables   map[string]string
	InitVariables   []struct {
		Name    string `yaml:"Name"`
		Description    string `yaml:"Description"`
		Type    string `yaml:"Type"`
		Default string `yaml:"Default"`
	} `yaml:"Variables"`
}

type RunnableTask struct {
	Include      string `yaml:"Include"`
    Name       string   `yaml:"Name,omitempty"`
	Image      string   `yaml:"Image,omitempty"`
	Command    []string   `yaml:"Command,omitempty"`
	Bash       []string   `yaml:"Bash,omitempty"`
	Timeout    string   `yaml:"Timeout,omitempty"`
	Log        []string `yaml:"Log,omitempty"`
	WaitBlocks int      `yaml:"WaitBlocks,omitempty"`
	Blocking   string      `yaml:"Blocking,omitempty"`
	Verbose   string      `yaml:"Verbose,omitempty"`
	Register   string      `yaml:"Register,omitempty"`
	IsSet   string      `yaml:"IsSet,omitempty"`
	IsNotSet   string      `yaml:"IsNotSet,omitempty"`
	HideLog   string      `yaml:"HideLog,omitempty"`
	DebugVars   string      `yaml:"DebugVars,omitempty"`
	ParseJson   []string      `yaml:"ParseJson,omitempty"`
}

type TplusInfo struct {
	ProjectName string `json:"ProjectName"`
	Description string `json:"Description"`
	Tasks       struct {
		DefaultImage string `json:"DefaultImage"`
	} `json:"Tasks"`
}

type SyncResponse struct {
	Existing []SyncFile
}

type ModelBase struct {
	Created time.Time
	LastRead time.Time
	LastSave time.Time
}

type Model interface {
	SetCreatedNow()
	setLastReadNow()
	setLastSaveNow()
	Marshal() []byte
	GetId() string
	GetBucket() []byte
}


type PluginBase struct {
	Id string
	EnvId string
	Installed bool
	Enabled bool
	HTTPEndpoint string
	HTTPEndpointActive bool
	Containers []Container
}

type Project struct {
	ModelBase
	Id            string
	EnvId         string
	Name          string
	Description   string
	Alias         string
	SetupDone     bool
	Source        string
	Template      string
	GitUrl        string
	ClonedFrom    string
	Services      []Service
	Variables     map[string]string
}


type Service struct {
	Status     string
	Error      string
	Name       string      `yaml:"Name"`
	Identifier string      `yaml:"Identifier"`
	Image      string      `yaml:"Image"`
	Mounts     []Mounts    `yaml:"Mounts"`
	Endpoints  []Endpoints `yaml:"Endpoints"`
}

type Mounts struct {
	Source      string `yaml:"Source"`
	Destination string `yaml:"Destination"`
}

type Endpoints struct {
	Type string `yaml:"Type"`
	Port string `yaml:"Port"`
	Name string `yaml:"Name"`
}


type Environment struct {
	Id string
	ModelBase
	Name            string
	Tezos           TezosNode
	ClientContainer Container
	CreatedByUserId string
	Shared          bool
	Consoles        map[string]Container
	SnapshotSource string
	BackupSource   string
	Error          string
	SandboxParams  string
	SandboxConfig  string
	SandboxSetupDone bool
	Protocol string
	Plugins []Plugin
}

type DockerImage struct {
	Id string
	ModelBase
	LastPull time.Time
}

type TezosNode  struct {
	Remote        bool
	RemoteNets    []string
	Sandboxed     bool
	Branch        string
	Status        string
	HeadLevel     uint64
	HeadHash      string
	Container     Container
	DataDir       string
	ConfigDir     string
	ClientDataDir string
	SharedDir     string
	SnapshotDir   string
	BackupDir     string
	HistoryMode   string
	Endpoint      string
}

type Container struct {
	Id string
	Name string
	Ips []string
	Network string
	NetworkID string
	UpdateAvailable bool
	Status string
	Resources Resources
}


type Metric struct {
	Time          time.Time
	EnvironmentId string
	Source        string
	Metric        string
	Value         float64
}

type MetricSimple struct {
	Time  time.Time
	Value float64
}

func (obj *Metric) ToBytes() []byte {
	b, _ := json.Marshal(obj)
	return b
}


type Task struct {
	ModelBase
	Id string
	Name string
	Description string
	Progress string
	Done bool
	EnvironmentId string
	Task string
    Logs  map[string]string
    State string
    OnSuccess string
    OnFailure string
    ObjectsUpdated [][]string
    Type string
}


type InviteCode struct {
	ModelBase
	Id string
	Description string
	Used int
	MaxUsed int
}

type Resources struct {
	CPUCount  uint // in milliCPU
	MemoryMax uint // in byte
}

type User struct {
	ModelBase
	Id string
	Admin bool
	Username string
	Password []byte
	Environments Environment
}

func (obj *User) SetPassword(plain string) {
	hash, _ := bcrypt.GenerateFromPassword([]byte(plain), 12)
	obj.Password = hash
}

func (obj *User) PasswordIsCorrect(plain string) bool {
	err := bcrypt.CompareHashAndPassword(obj.Password, []byte(plain))
	if err != nil {
		return false
	}
	return true
}


type Token struct {
	ModelBase
	Id string
	UserId string
}




// Generated interface functions


// generated for Environment

func (obj *Environment) Size() int64  {
	return int64(len(obj.Marshal()))
}

func (obj *Environment) GetBucket() []byte {
     return []byte("Environment")
}

func (obj *Environment) GetId() string {
     return obj.Id
}
func (obj *Environment) Marshal() []byte {
     b,_ := json.Marshal(obj)
     return b
}

func (obj *Environment) UnMarshal(b []byte) error {
    return json.Unmarshal(b,obj)
}


func (obj *Environment) SetCreatedNow() {
    obj.Created = time.Now()
}

func (obj *Environment) setLastReadNow() {
    obj.LastRead = time.Now()
}

func (obj *Environment) setLastSaveNow() {
    obj.LastSave = time.Now()
}


// generated for InviteCode

func (obj *InviteCode) Size() int64  {
	return int64(len(obj.Marshal()))
}

func (obj *InviteCode) GetBucket() []byte {
     return []byte("InviteCode")
}

func (obj *InviteCode) GetId() string {
     return obj.Id
}
func (obj *InviteCode) Marshal() []byte {
     b,_ := json.Marshal(obj)
     return b
}

func (obj *InviteCode) UnMarshal(b []byte) error {
    return json.Unmarshal(b,obj)
}


func (obj *InviteCode) SetCreatedNow() {
    obj.Created = time.Now()
}

func (obj *InviteCode) setLastReadNow() {
    obj.LastRead = time.Now()
}

func (obj *InviteCode) setLastSaveNow() {
    obj.LastSave = time.Now()
}


// generated for User

func (obj *User) Size() int64  {
	return int64(len(obj.Marshal()))
}

func (obj *User) GetBucket() []byte {
     return []byte("User")
}

func (obj *User) GetId() string {
     return obj.Id
}
func (obj *User) Marshal() []byte {
     b,_ := json.Marshal(obj)
     return b
}

func (obj *User) UnMarshal(b []byte) error {
    return json.Unmarshal(b,obj)
}


func (obj *User) SetCreatedNow() {
    obj.Created = time.Now()
}

func (obj *User) setLastReadNow() {
    obj.LastRead = time.Now()
}

func (obj *User) setLastSaveNow() {
    obj.LastSave = time.Now()
}


// generated for Token

func (obj *Token) Size() int64  {
	return int64(len(obj.Marshal()))
}

func (obj *Token) GetBucket() []byte {
     return []byte("Token")
}

func (obj *Token) GetId() string {
     return obj.Id
}
func (obj *Token) Marshal() []byte {
     b,_ := json.Marshal(obj)
     return b
}

func (obj *Token) UnMarshal(b []byte) error {
    return json.Unmarshal(b,obj)
}


func (obj *Token) SetCreatedNow() {
    obj.Created = time.Now()
}

func (obj *Token) setLastReadNow() {
    obj.LastRead = time.Now()
}

func (obj *Token) setLastSaveNow() {
    obj.LastSave = time.Now()
}


// generated for Task

func (obj *Task) Size() int64  {
	return int64(len(obj.Marshal()))
}

func (obj *Task) GetBucket() []byte {
     return []byte("Task")
}

func (obj *Task) GetId() string {
     return obj.Id
}
func (obj *Task) Marshal() []byte {
     b,_ := json.Marshal(obj)
     return b
}

func (obj *Task) UnMarshal(b []byte) error {
    return json.Unmarshal(b,obj)
}


func (obj *Task) SetCreatedNow() {
    obj.Created = time.Now()
}

func (obj *Task) setLastReadNow() {
    obj.LastRead = time.Now()
}

func (obj *Task) setLastSaveNow() {
    obj.LastSave = time.Now()
}


// generated for DockerImage

func (obj *DockerImage) Size() int64  {
	return int64(len(obj.Marshal()))
}

func (obj *DockerImage) GetBucket() []byte {
     return []byte("DockerImage")
}

func (obj *DockerImage) GetId() string {
     return obj.Id
}
func (obj *DockerImage) Marshal() []byte {
     b,_ := json.Marshal(obj)
     return b
}

func (obj *DockerImage) UnMarshal(b []byte) error {
    return json.Unmarshal(b,obj)
}


func (obj *DockerImage) SetCreatedNow() {
    obj.Created = time.Now()
}

func (obj *DockerImage) setLastReadNow() {
    obj.LastRead = time.Now()
}

func (obj *DockerImage) setLastSaveNow() {
    obj.LastSave = time.Now()
}


// generated for Project

func (obj *Project) Size() int64  {
	return int64(len(obj.Marshal()))
}

func (obj *Project) GetBucket() []byte {
     return []byte("Project")
}

func (obj *Project) GetId() string {
     return obj.Id
}
func (obj *Project) Marshal() []byte {
     b,_ := json.Marshal(obj)
     return b
}

func (obj *Project) UnMarshal(b []byte) error {
    return json.Unmarshal(b,obj)
}


func (obj *Project) SetCreatedNow() {
    obj.Created = time.Now()
}

func (obj *Project) setLastReadNow() {
    obj.LastRead = time.Now()
}

func (obj *Project) setLastSaveNow() {
    obj.LastSave = time.Now()
}


// generated for ProjectTask

func (obj *ProjectTask) Size() int64  {
	return int64(len(obj.Marshal()))
}

func (obj *ProjectTask) GetBucket() []byte {
     return []byte("ProjectTask")
}

func (obj *ProjectTask) GetId() string {
     return obj.Id
}
func (obj *ProjectTask) Marshal() []byte {
     b,_ := json.Marshal(obj)
     return b
}

func (obj *ProjectTask) UnMarshal(b []byte) error {
    return json.Unmarshal(b,obj)
}


func (obj *ProjectTask) SetCreatedNow() {
    obj.Created = time.Now()
}

func (obj *ProjectTask) setLastReadNow() {
    obj.LastRead = time.Now()
}

func (obj *ProjectTask) setLastSaveNow() {
    obj.LastSave = time.Now()
}
