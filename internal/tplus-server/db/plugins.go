package db

import "github.com/tyler-sommer/stick"

type Plugin struct {
	Id          string
	Name        string
	Description string
	Status      string
	Services    []*PluginService
	Vars        map[string]stick.Value
}

type PluginService struct {
	Status      string
	Name        string
	ServiceName string
	Description string
	Image       string
	Entrypoint  []string
	Command     []string
	Env         []string
	ContainerId string
	Templates   []PluginTemplate
	Endpoints   []PluginEndpoints
	Volumes     []PluginVolume
}

type PluginEndpoints struct {
	Id         string
	Port       int
	Proto      string
	Name       string
	Register   string
	Type       string
	URL        string
	HomePath   string
}

type PluginVolume struct {
	Name        string
	Description string
	Mount       string
	HostDir     string
}

type PluginTemplate struct {
	Name     string
	Source   string
	Dest     string
	HostFile string
}
