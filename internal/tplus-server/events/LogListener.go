package events

import (
	"encoding/json"
	"fmt"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/config"
	"os"
	"sort"
	"strings"
)

type LogListener struct {
	maxCache int
	store    *EventStore
	LogType string
}

func NewLogListener(events *Events, config *config.Config) *LogListener{
	l := LogListener{}
	l.store = NewEventStore(5000)
	l.LogType = config.LogType
	go l.run(events)
	events.setLogs(&l)
	return &l
}

type LogListenerFactory EventSubscriberFactory

func NewLogListenerFactory(config *config.Config) LogListenerFactory {
	return func(events *Events) {
		NewLogListener(events,config)
	}
}

func (ll *LogListener) Last(n int) []*Event {
	return ll.store.Get(n)
}

func (ll *LogListener) run(events *Events){
	eventchan := events.Listen(LogSelector)
	_,silent := os.LookupEnv("TPLUS_SILENT")
	levels := map[string]int{
		"DEBUG": 1,
		"INFO": 2,
		"WARN": 3,
		"ERROR": 4,
		"FATAL": 10,
	}
	minlevelEnv,_ := os.LookupEnv("TPLUS_LOG")
	minlevel := levels["INFO"]
	if  val,ok := levels[minlevelEnv]; ok {
		minlevel = val
	}
	for {
		e := <- eventchan
		if !silent && levels[e.Level] >= minlevel {
			switch ll.LogType {
			case "pretty":
				ll.stdoutNice(e)
			case "short_and_pretty":
				ll.stdoutNiceSimple(e)
			case "json":
				ll.json(e)
			default:
				ll.stdoutNiceSimple(e)
			}
		}
		ll.append(e)
	}
}

func (ll *LogListener) json(e *Event){
	b,_ := json.Marshal(e)
	fmt.Println(string(b))
}

func (ll *LogListener) stdoutNice(e *Event){
	fmt.Println(e.Timestamp.Format("02-Jan-2006 15:04:05"), "  |", e.Message)
	var keys []string
	for k := range e.Context {
		keys = append(keys, k)
	}
	sort.Strings(keys)
	for _,k := range keys {
		v := e.Context[k]
		if v != "" && k != "" {
			fmt.Println("                       | " + k + ": " + v)
		}
	}
	fmt.Println("")
}

func (ll *LogListener) stdoutNiceSimple(e *Event){
	fmt.Println(e.Timestamp.Format("02-Jan-2006 15:04:05"), "  |", e.Message)
	var keys []string
	for k := range e.Context {
		keys = append(keys, k)
	}
	sort.Strings(keys)
	for _,k := range keys {
		v := e.Context[k]
		if  ( strings.Contains(strings.ToLower(k),"name") ||
			strings.Contains(strings.ToLower(k),"plugin") ||
			strings.Contains(strings.ToLower(k),"project") ||
			strings.Contains(strings.ToLower(k),"block") ) &&
			k != "taskName"  {
			k = strings.Replace(k,"name","",-1)
			k = strings.Replace(k,"Name","",-1)
			k = strings.Title(k)
			if v != "" && k != "" {
				fmt.Println("                       | " + k + ": " + v)
			}
		}
	}
	fmt.Println("")
}




func (ll *LogListener) append(e * Event) {
	ll.store.Add(e)
}