package events

import (
	"encoding/json"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/config"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	http2 "gitlab.com/tuliptools/tplus/internal/tplus-server/http"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/tezcon"
	"io/ioutil"
	"strconv"
	"time"
)

type TezosWatcher struct {
	active map[string]string
	db *db.DB
	events *Events
	config *config.Config
	client *http2.InternalClient
}


func NewTezosWatcher(events *Events, db *db.DB,c *config.Config, client *http2.InternalClient) *TezosWatcher{
	t := TezosWatcher{}
	t.db = db
	t.events = events.Source("TezosWatcher")
	t.config = c
	t.client = client
	t.active = map[string]string{}
	go t.checkEnvs()
	return &t
}


func (tw *TezosWatcher) checkEnvs(){
	for {
		envs,e := tw.db.GetEnvironments()
		if e == nil {
			for _,env := range envs {
				if _,ok := tw.active[env.Id]; !ok {
					ep := env
					tw.active[ep.Id] = "true"
					go tw.monitor(&ep)
				}
			}
		}
		time.Sleep(3*time.Second)
	}
}

func (tw *TezosWatcher) monitor(env *db.Environment){
	c := tw.client.GetDefaultClient()
	wait := time.Duration(5*time.Second)
	if env.Tezos.Branch == "sandbox" {
		wait = 2000 * time.Millisecond
	}
	last := int64(0)
	log := tw.events.WithEnv(env)
	for {
		ne,e := tw.db.GetEnvironmentById(env.Id)
		if e == nil  && ne.Tezos.Status == "running" {
			env = ne
			baseUrl := "http://localhost:" + strconv.Itoa(tw.config.WebPort)
			ep := baseUrl + "/endpoints/" + env.Tezos.Endpoint + "/chains/main/blocks/head"
			res,e := c.Get(ep)
			if e == nil {
				body,_ := ioutil.ReadAll(res.Body)
				res.Body.Close()
				block := tezcon.Block{}
				e2 := json.Unmarshal(body, &block)
					if e2 == nil && block.Header.Level != last {
						last = block.Header.Level
						tw.db.UpdateEnvironment(env, func(update *db.Environment) *db.Environment {
							update.Tezos.HeadLevel = uint64(block.Header.Level)
							update.Tezos.HeadHash = block.Hash
							return update
						})
						l := log.WithMany(map[string]string{
							"Blockhash" : block.Hash,
							"Blocklevel" : strconv.Itoa(int(block.Header.Level)),
							"Baker" : block.Metadata.Baker,
							"Blocktime": block.Header.Timestamp.String(),
						})
						l.Info("Last Block updated")
						event := l.baseEvent()
						event.Type = "block_update"
						event.Message = "Last Block Update"
						event.Payload = string(body)
						log.register(event)
					}
			}
		}
		time.Sleep(wait)
	}

}

type TezosWatcherFactory EventSubscriberFactory

func NewTezosWatcherFactory(db *db.DB, config *config.Config, client *http2.InternalClient) TezosWatcherFactory {
	return func(events *Events) {
		NewTezosWatcher(events,db,config, client)
	}
}