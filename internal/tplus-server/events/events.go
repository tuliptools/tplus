package events

import (
	"encoding/json"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"time"
)

type Events struct {
	parent *Events
	vars map[string]string
	Sources []string
	chans []chan *Event
	Logs *LogListener
}

type EventSubscriberFactory func(e *Events)

func NewEvents(ll LogListenerFactory, tez TezosWatcherFactory) *Events {
	res := Events{}
	res.vars = map[string]string{}
	res.Sources = []string{}
	res.vars["tplus_start"] = time.Now().String()
	res.chans = []chan *Event{}
	ll(&res)
	tez(&res)
	return &res
}



func (ev *Events) Silent() *Events{
	c := ev.copy()
	c.parent = nil
	return c
}

func (ev *Events) setLogs(l *LogListener){
	ev.Logs = l
}
type EventSelector func(e *Event) bool

func (ev *Events) Listen(selector EventSelector) chan *Event {
	out := make(chan *Event)
	in := make(chan *Event)
	ev.chans = append(ev.chans, in)

	go func(in, out chan *Event, selector EventSelector) {
		for {
			a := <- in
			if selector(a) {
				out <- a
			}
		}
	}(in,out, selector)

	return out
}

func (ev *Events) GetLogs(n int) []*Event {
	if ev.parent != nil {
		return ev.parent.GetLogs(n)
	}
	if ev.Logs == nil {
		return []*Event{}
	} else {
		return ev.Logs.Last(n)
	}
}


func (ev *Events) With(key, value string) *Events {
	res := ev.copy()
	res.vars[key] = value
	return res
}

func (ev *Events) WithMany(m map[string]string) *Events {
	res := ev.copy()
	for k,v := range m {
		res.vars[k] = v
	}
	return res
}

func (ev *Events) WithEnv(e *db.Environment) *Events {
	res := ev.copy()
	res.vars["envId"] = e.GetId()
	res.vars["envName"] = e.Name
	res.vars["tezosNet"] = e.Tezos.Branch
	res.vars["tezosHM"] = e.Tezos.HistoryMode
	return res
}

func (ev *Events) Source(key string) *Events{
	res := ev.copy()
	res.Sources = append(res.Sources, key)
	return res
}


func (ev *Events) copy() *Events {
	res := Events{}
	if ev.parent == nil {
		res.parent = ev
	} else {
		res.parent = ev.parent
	}
	res.vars = map[string]string{}
	for k,v := range ev.vars {
		res.vars[k] = v
	}
	res.Sources = []string{}
	for _,a := range ev.Sources {
		res.Sources = append(res.Sources, a)
	}
	res.Sources = ev.Sources
	return &res
}

func (ev *Events) baseEvent() *Event{
	e := Event{}
	e.Id = db.GetRandomId("event")
	e.Timestamp = time.Now()
	e.Context = ev.vars
	e.Source = ev.Sources
	return &e
}

func (ev *Events) Obj(o struct{}) {
	base := ev.baseEvent()
	base.Level = "DEBUG"
	base.Type = "internal"
	a,_ := json.Marshal(o)
	base.Payload = string(a)
	ev.register(base)

}
func (ev *Events) Log(s string) {
	base := ev.baseEvent()
	base.Type = "log"
	base.Level = "INFO"
	base.Message = s
	ev.register(base)

}
func (ev *Events) Debug(s string) {
	base := ev.baseEvent()
	base.Type = "log"
	base.Level = "DEBUG"
	base.Message = s
	ev.register(base)

}
func (ev *Events) Info(s string) {
	base := ev.baseEvent()
	base.Type = "log"
	base.Level = "INFO"
	base.Message = s
	ev.register(base)

}
func (ev *Events) Warn(s string) {
	base := ev.baseEvent()
	base.Type = "log"
	base.Level = "WARN"
	base.Message = s
	ev.register(base)

}
func (ev *Events) Error(s string) {
	base := ev.baseEvent()
	base.Type = "log"
	base.Level = "ERROR"
	base.Message = s
	ev.register(base)

}
func (ev *Events) Fatal(s string) {
	base := ev.baseEvent()
	base.Type = "log"
	base.Level = "FATAL"
	base.Message = s
	ev.register(base)
}

func (ev *Events) register(e *Event){
	if ev.parent != nil {
		ev.parent.register(e)
	} else {
		for _,c := range ev.chans {
			c <- e
		}
	}
}