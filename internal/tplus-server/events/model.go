package events

import "time"

type Event struct {
	Id string
	Type string // log or internal
	Level string // set when log
	Message string
	Context map[string]string
	Timestamp time.Time
	Source []string
	Payload string
}