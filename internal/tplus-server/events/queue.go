package events

import (
	"sync"
)

/*
 * simple Queue implementation,
 * not really used as a queue but just to keep
 * max n entries
 */
type EventStore struct {
	lock *sync.Mutex
	OrderFTB  map[*Event]*Event
	OrderBTF  map[*Event]*Event
	Front  *Event
	Rear   *Event
	len    int
	maxlen int
	mutex  *sync.Mutex
}

func NewEventStore(n int) *EventStore{
	e := EventStore{}
	e.OrderBTF = map[*Event]*Event{}
	e.OrderFTB = map[*Event]*Event{}
	e.maxlen = n
	e.len = 0
	e.lock = &sync.Mutex{}
	e.mutex = &sync.Mutex{}
	return &e
}

func (q *EventStore) Get(n int) []*Event {
	q.lock.Lock()
	defer q.lock.Unlock()
	res := []*Event{}
	if q.Front == nil {
		return res
	}
	a := q.Front
	c := 1
	for {
		res = append(res, a)
		if c >= n {
			break
		}
		if val,ok := q.OrderFTB[a]; ok {
			a = val
		} else {
			break
		}
		c++
	}
	return res
}

func (q *EventStore) Add(e *Event) {
	q.lock.Lock()
	defer q.lock.Unlock()
	q.mutex.Lock()
	q.mutex.Unlock()
	if (q.Front == nil ) {
		q.Front = e
		q.Rear = e
		q.len = 1
	} else {
		q.len = q.len + 1
		tmp := q.Front
		q.Front = e
		q.OrderFTB[e] = tmp
		q.OrderBTF[tmp] = e
		if q.len > q.maxlen {
			del := q.Rear
			q.Rear = q.OrderBTF[del]
			delete(q.OrderBTF,del)
			delete(q.OrderFTB,del)
		}
	}
}