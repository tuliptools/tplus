package helpers

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"
)

func GetTulipSnapshotUrl(network, mode string) (url string, err error) {
	network = strings.ToLower(network)
	mode = strings.ToLower(mode)
	key := network + "_" + mode

	shots := TulipSnapshotInfo{}
	api := "https://snaps.tulip.tools/snapshots.json"

	client := http.Client{
		Timeout: 1 * time.Second,
	}
	res, err := client.Get(api)
	defer res.Body.Close()

	fmt.Println(shots)
	fmt.Println(key)

	if err == nil {
		b, _ := ioutil.ReadAll(res.Body)
		json.Unmarshal(b, &shots)

		if val, ok := shots.Snapshots[key]; ok {
			return "https://snaps.tulip.tools/" + val.Filename, nil
		}
	}

	err = errors.New("could not get Snapshot URL")
	return url, err
}

type Snapshot struct {
	Filename  string    `json:"Filename"`
	Network   string    `json:"Network"`
	Block     int       `json:"Block"`
	Blockhash string    `json:"Blockhash"`
	Blocktime time.Time `json:"Blocktime"`
}

type TulipSnapshotInfo struct {
	Snapshots  map[string]Snapshot `json:"Snapshots"`
	LastChange time.Time           `json:"LastChange"`
}
