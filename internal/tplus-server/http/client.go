package http

import (
	"net/http"
	"net/url"
	"os"
	"runtime"
	"time"
)

type InternalClient struct {
}

func GetClient() *InternalClient {
	return &InternalClient{}
}

func (hc *InternalClient) GetProxiedClient() *http.Client {
	h := http.Client{}
	h.Timeout = 3 * time.Second
	if runtime.GOOS == "darwin" || os.Getenv("RUN_MAC_PROXY") == "true" {
		proxyUrl, _ := url.Parse("http://localhost:60501")
		h.Transport = &http.Transport{
			MaxIdleConns: 10,
			Proxy: http.ProxyURL(proxyUrl),
			IdleConnTimeout: 3*time.Second,
		}
	}
	go func() {
		for {
			time.Sleep(3*time.Second)
			h.CloseIdleConnections()
		}
	}()
	return &h
}

func (hc *InternalClient) GetDefaultClient() *http.Client {
	h := http.Client{}
	h.Timeout = 3 * time.Second
	go func() {
		for {
			time.Sleep(3*time.Second)
			h.CloseIdleConnections()
		}
	}()
	return &h
}
