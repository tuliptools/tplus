package manager

import (
	"github.com/cssivision/reverseproxy"
	"github.com/gin-gonic/gin"
	config2 "gitlab.com/tuliptools/tplus/internal/tplus-server/config"
	events2 "gitlab.com/tuliptools/tplus/internal/tplus-server/events"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/service"
	"io"
	"net"
	"net/http"
	"net/url"
	"os"
	"runtime"
	"strconv"
	"strings"
	"sync"
	"time"
)

type HttpEndpointManager struct {
	docker    *service.DockerService
	endpoints map[string]string
	redirects map[string]string
	proxies   map[string]int
	lock      *sync.Mutex
	config    *config2.Config
	events   *events2.Events
}

func NewEndpointManager(d *service.DockerService, config *config2.Config, events *events2.Events) *HttpEndpointManager {
	em := HttpEndpointManager{}
	em.docker = d
	em.events = events.Source("Endpoint manager")
	em.config = config
	em.endpoints = map[string]string{}
	em.redirects = map[string]string{}
	em.proxies = map[string]int{}
	em.lock = &sync.Mutex{}
	return &em
}

func (endpoints *HttpEndpointManager) HandleGin(id string, c *gin.Context) {
	endpoints.lock.Lock()

	if id[len(id)-1:] != "/" {
		id = id + "/"
	}
	if len(id) <= 25 {
		c.String(500, "Endpoint not ready")
		endpoints.lock.Unlock()
		return
	}
	id = id[1:26]
	if redirect, ok := endpoints.redirects[id]; ok {
		if val, ok := endpoints.proxies[redirect]; ok {
			endpoints.lock.Unlock()
			host := c.Request.Host
			hosts := strings.Split(host, ":")
			host = hosts[0]
			c.Request.RequestURI = strings.Replace(c.Request.RequestURI, "/endpoints/"+id, "", 1)
			c.Redirect(302, "http://"+host+":"+strconv.Itoa(val)+c.Request.RequestURI)
			return
		}
		endpoints.lock.Unlock()
		c.Request.RequestURI = strings.Replace(c.Request.RequestURI, "/endpoints/"+id, "", 1)
		c.Redirect(302, redirect+c.Request.RequestURI)
		return
	}

	var target string
	var ok bool
	if target, ok = endpoints.endpoints[id]; !ok {
		endpoints.lock.Unlock()
		c.String(404, "Endpoint not found!!")
	} else {
		endpoints.lock.Unlock()
		remote, err := url.Parse(target)
		if err != nil {
			panic(err)
		}
		if remote.Scheme == "https" {
			c.Request.URL.Host = remote.Host
			c.Request.URL.Scheme = remote.Scheme
			c.Request.Header.Set("X-Forwarded-Host", c.Request.Header.Get("Host"))
			c.Request.Host = remote.Host
		}
		c.Request.RequestURI = strings.Replace(c.Request.RequestURI, "/endpoints/"+id, "", 1)
		cl := http.Client{}
		if runtime.GOOS == "darwin" || os.Getenv("RUN_MAC_PROXY") == "true" {
			proxyUrl, _ := url.Parse("http://localhost:60501")
			cl.Transport = &http.Transport{
				Proxy: http.ProxyURL(proxyUrl),
			}
		}
		req, err := http.NewRequest(c.Request.Method, remote.String()+c.Request.RequestURI, c.Request.Body)
		resp, err := cl.Do(req)
		if err != nil {
			c.Abort()
			return
		}
		if resp.StatusCode >= 300 || resp.StatusCode <= 199 {
			endpoints.events.WithMany(map[string]string{
				"HTTP Status" : resp.Status,
				"ContentLength" : strconv.Itoa(int(resp.ContentLength)),
				"URI" : c.Request.RequestURI,
			}).Warn("Request to " + id + " failed with: " + resp.Status)
		}


		c.Header("Content-Type", resp.Header.Get("Content-Type"))
		io.Copy(c.Writer, resp.Body)
		resp.Body.Close()
	}
}

func (endpoints *HttpEndpointManager) RegisterEndpoint(id, target string) {
	endpoints.lock.Lock()
	defer endpoints.lock.Unlock()
	endpoints.endpoints[id] = target
	endpoints.events.Debug("Endpoint " + id + " --> " + target)
}

/*
 * Used for things like Websites, that cant handle redirects via custom urls
 */
func (endpoints *HttpEndpointManager) RegisterRedirect(id, target string) {
	endpoints.lock.Lock()
	defer endpoints.lock.Unlock()
	endpoints.redirects[id] = target
	if endpoints.config.EndpointMode == "bind" { // bind port on host and redirect
		// used on tplus-web and installs in a VM
		port := endpoints.GetFreePort()
		endpoints.StartPortProxy(port, target)
		endpoints.proxies[target] = port
	}
	endpoints.events.Debug("Redirect " + id + " --> " + target)
}

func (endpoints *HttpEndpointManager) GetFreePort() int {
	minport, _ := strconv.Atoi(endpoints.config.EndpointPort)
	if minport <= 1000 {
		minport = 8900
	}

	portToUse := minport
	for {
		used := endpoints.isUsed(portToUse)
		if used == true {
			portToUse++
		} else {
			break
		}
	}
	return portToUse
}

func (endpoints *HttpEndpointManager) StartPortProxy(port int, target string) {
	go func() {

		handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			path, err := url.Parse(target)
			if err != nil {
				panic(err)
				return
			}
			proxy := reverseproxy.NewReverseProxy(path)
			if runtime.GOOS == "darwin" || os.Getenv("RUN_MAC_PROXY") == "true" {
				proxyUrl, _ := url.Parse("http://localhost:60501")
				proxy.Transport = &http.Transport{
					Proxy: http.ProxyURL(proxyUrl),
				}
			}
			proxy.ServeHTTP(w, r)
		})
		s := &http.Server{
			Addr:           ":" + strconv.Itoa(port),
			Handler:        handler,
			ReadTimeout:    2 * time.Second,
			WriteTimeout:   2 * time.Second,
			IdleTimeout:    5 * time.Second,
			MaxHeaderBytes: 1 << 20,
		}
		s.ListenAndServe()

	}()
}

func (endpoints *HttpEndpointManager) isUsed(port int) bool {
	timeout := 5 * time.Millisecond
	conn, err := net.DialTimeout("tcp", net.JoinHostPort("127.0.0.1", strconv.Itoa(port)), timeout)
	if conn != nil {
		defer conn.Close()
	}
	if err != nil {
		return false
	} else {
		return true
	}
}

func (endpoints *HttpEndpointManager) DeRegisterEndpoint(id string) {
	endpoints.lock.Lock()
	defer endpoints.lock.Unlock()
	delete(endpoints.endpoints, id)
}
