package manager

import (
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	events2 "gitlab.com/tuliptools/tplus/internal/tplus-server/events"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/service"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task/node"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task/remote"
	"sync"
)

type EnvManager struct {
	tm     *task.Manager
	db     *db.DB
	em     *HttpEndpointManager
	docker *service.DockerService
	events *events2.Events
}

func NewEnvManager(tm *task.Manager, db *db.DB, em *HttpEndpointManager, d *service.DockerService, ev *events2.Events) *EnvManager {
	e := EnvManager{}
	e.tm = tm
	e.db = db
	e.em = em
	e.events = ev
	e.docker = d
	go e.boot()
	return &e
}


func NewEnvManagerNoBoot(tm *task.Manager, db *db.DB, em *HttpEndpointManager, d *service.DockerService) *EnvManager {
	e := EnvManager{}
	e.tm = tm
	e.db = db
	e.em = em
	e.docker = d
	return &e
}


func (envs *EnvManager) boot() {
	list, _ := envs.db.GetEnvironments()

	wg := &sync.WaitGroup{}
	for i := range list {
		env := list[i]
		state := env.Tezos.Status
		if state == "running" || state == "pending" || state == "" {
			if env.Tezos.Sandboxed == false && env.Tezos.Remote == false {
				_ = envs.CreatePublicNetworkNode(&env,wg)
			}
			if env.Tezos.Sandboxed == true && env.Tezos.Remote == false {
				_ = envs.CreateSandboxNode(&env,wg)
			}
			if env.Tezos.Remote == true {
				_ = envs.CreateRemoteNode(&env,wg)
			}
		}
	}

	go func() {
		wg.Wait()
		envs.events.With("event_key","envs_init_done").Info("All nodes started")
	}()
}

func (envs *EnvManager) CreatePublicNetworkNode(environment *db.Environment,wg *sync.WaitGroup) error {
	log := envs.events.WithEnv(environment)
	log.Info("Starting public environment...")
	createTask := envs.GetLocalEnvironmentCreateTask(environment)
	go func() {
		if wg != nil {
			wg.Add(1)
		}
		envs.tm.RunAndBlock(createTask)
		log.Info("Start task finished")
		envs.registerEndpoint(environment)
		if wg != nil {
			wg.Done()
		}
	}()
	return nil
}

func (envs *EnvManager) CreateSandboxNode(environment *db.Environment, wg *sync.WaitGroup) error {
	log := envs.events.WithEnv(environment)
	log.Info("Starting sandbox...")
	createTask := envs.GetLocalSandboxCreateTask(environment)
	go func() {
		if wg != nil {
			wg.Add(1)
		}
		envs.tm.RunAndBlock(createTask)
		envs.registerEndpoint(environment)
		log.Info("Start task finished")
		if wg != nil {
			wg.Done()
		}
	}()
	return nil
}

func (envs *EnvManager) CreateRemoteNode(environment *db.Environment,wg *sync.WaitGroup) error {
	log := envs.events.WithEnv(environment)
	log.Info("Starting remote environment...")
	createTask := envs.GetRemoteEnvCreateTask(environment)
	go func() {
		if wg != nil {
			wg.Add(1)
		}
		envs.tm.RunAndBlock(createTask)
		envs.registerEndpoint(environment)
		log := envs.events.WithEnv(environment)
		log.Info("Start task finished")
		if wg != nil {
			wg.Done()
		}
	}()
	return nil
}

func (envs *EnvManager) Start(environment *db.Environment) error {
	return envs.StartWithWg(environment,nil)
}

func (envs *EnvManager) StartWithWg(environment *db.Environment,wg *sync.WaitGroup) error {
	envs.db.UpdateEnvironment(environment, func(update *db.Environment) *db.Environment {
		update.Tezos.Container.Status = "running"
		update.Tezos.Status = "running"
		return update
	})
	if environment.Tezos.Sandboxed == false && environment.Tezos.Remote == false {
		_ = envs.CreatePublicNetworkNode(environment,wg)
	}
	if environment.Tezos.Sandboxed == true && environment.Tezos.Remote == false {
		_ = envs.CreateSandboxNode(environment,wg)
	}
	if environment.Tezos.Remote == true {
		_ = envs.CreateRemoteNode(environment,wg)
	}
	return nil
}

func (envs *EnvManager) registerEndpoint(e *db.Environment) {
	e, _ = envs.db.GetEnvironmentById(e.Id)
	if e.Tezos.Container.Status == "running" {
		name := e.Tezos.Container.Name
		ip, err := envs.docker.GetContainerIP(name)
		if err == nil && e.Tezos.Endpoint == "" {
			e.Tezos.Endpoint = db.GetRandomId("endpoint")
			envs.db.Save(e)
		}

		envs.em.RegisterEndpoint(e.Tezos.Endpoint, "http://"+ip+":8732")
	}
}

func (envs *EnvManager) Stop(environment *db.Environment) error {
	log := envs.events.WithEnv(environment)
	log.Info("Stopping environment...")
	envs.db.UpdateEnvironment(environment, func(update *db.Environment) *db.Environment {
		update.Tezos.Container.Status = "stopping..."
		update.Tezos.Status = "stopping..."
		return update
	})
	stopTask := envs.tm.RegisterInternal(node.NewStopEnvTask(environment))
	envs.tm.RunAndBlock(stopTask)
	log.Info("Stopped")
	envs.db.UpdateEnvironment(environment, func(update *db.Environment) *db.Environment {
		update.Tezos.Container.Status = "stopped"
		update.Tezos.Status = "stopped"
		return update
	})
	return nil
}

func (envs *EnvManager) Restart(environment *db.Environment) error {
	log := envs.events.WithEnv(environment)
	log.Info("Restarting environment...")
	stop := envs.tm.RegisterInternal(node.NewStopEnvTask(environment))
	envs.tm.RunAndBlock(stop)
	log.Info("Restarted.")
	return envs.Start(environment)
}

func (envs *EnvManager) Clean(environment *db.Environment) error {
	log := envs.events.WithEnv(environment)
	log.Info("Clean environment...")
	stop := envs.tm.RegisterInternal(node.NewStopEnvTask(environment))
	clean := envs.tm.RegisterInternal(node.NewRemoveEnvDataTask(environment))
	stop.SetOnSuccess(clean)
	envs.tm.RunAndBlock(stop)
	log.Info("Env cleaned")
	return envs.Start(environment)
}

func (envs *EnvManager) Destroy(environment *db.Environment) error {
	log := envs.events.WithEnv(environment)
	log.Info("Destroying environment...")
	stop := envs.tm.RegisterInternal(node.NewStopEnvTask(environment))
	clean := envs.tm.RegisterInternal(node.NewRemoveEnvDataTask(environment))
	remove := envs.tm.RegisterInternal(node.NewRemoveEnvContainerTask(environment))
	stop.SetOnSuccess(clean)
	clean.SetOnSuccess(remove)
	envs.tm.RunAndBlock(stop)
	envs.db.DeleteEnvironment(environment.Id)
	log.Info("Destroyed")
	return nil
}

func (envs *EnvManager) RemoveNodeContainer(environment *db.Environment) error {
	remove := envs.tm.RegisterInternal(node.NewRemoveNodeContainerTask(environment))
	envs.tm.RunAndBlock(remove)
	return nil
}

func (envs *EnvManager) GetEnvironments(user *db.User) ([]db.Environment, error) {
	var res []db.Environment
	list, error := envs.db.GetEnvironments()
	if error != nil {
		return res, error
	}
	for _, env := range list {
		if env.Shared || env.CreatedByUserId == user.Id {
			res = append(res, env)
		}
	}
	return res, nil
}

func (envs *EnvManager) GetPeers(user *db.User) ([]db.Environment, error) {
	var res []db.Environment
	list, error := envs.db.GetEnvironments()
	if error != nil {
		return res, error
	}
	for _, env := range list {
		if env.Shared || env.CreatedByUserId == user.Id {
			res = append(res, env)
		}
	}
	return res, nil
}

func (envs *EnvManager) GetEnvironment(user *db.User, id string) (db.Environment, error) {
	res := db.Environment{}
	list, error := envs.db.GetEnvironments()
	if error != nil {
		return res, error
	}
	for _, env := range list {
		if (env.Shared || env.CreatedByUserId == user.Id) && env.Id == id {
			res = env
		}
	}
	return res, nil
}

func (envs *EnvManager) GetLocalEnvironmentCreateTask(env *db.Environment) task.Task {
	dirs := envs.tm.RegisterInternal(node.NewNodeStoragePrepareTask(env))

	ImagePull := envs.tm.RegisterInternal(node.NewNodeImagePullTask(env))

	configCheck := envs.tm.RegisterInternal(node.NewNodeConfigCheck(env))
	configCreate := envs.tm.RegisterInternal(node.NewNodeConfigCreate(env))

	configCheck.SetOnFailure(configCreate)

	clientconfigCheck := envs.tm.RegisterInternal(node.NewClientConfigCheck(env))
	clientconfigCreate := envs.tm.RegisterInternal(node.NewClientConfigCreate(env))

	clientconfigCheck.SetOnFailure(clientconfigCreate)

	NetworkCheck := envs.tm.RegisterInternal(node.NewNetworkCheckTask(env))
	NetworkCreate := envs.tm.RegisterInternal(node.NewNetworkCreateTask(env))

	NetworkCheck.SetOnFailure(NetworkCreate)

	VersionCheck := envs.tm.RegisterInternal(node.NewVersionFileCheck(env))
	VersionCreate := envs.tm.RegisterInternal(node.NewNodeVersionFileCreate(env))

	VersionCheck.SetOnFailure(VersionCreate)

	IdCheck := envs.tm.RegisterInternal(node.NewNodeIdentityCheckTask(env))
	IdGen := envs.tm.RegisterInternal(node.NewNodeIdentityCreateTask(env))

	IdCheck.SetOnFailure(IdGen)

	SnapCheck := envs.tm.RegisterInternal(node.NewSnapshotCheckTask(env))
	SnapDownload := envs.tm.RegisterInternal(node.NewSnapshotDownloadTask(env))

	SnapImport := envs.tm.RegisterInternal(node.NewSnapshotImportTask(env))

	SnapCheck.SetOnFailure(SnapDownload)

	ContainerCheck := envs.tm.RegisterInternal(node.NewContainerCheckTask(env))
	ContainerCreate := envs.tm.RegisterInternal(node.NewContainerCreateTask(env))

	ContainerCheck.SetOnFailure(ContainerCreate)

	ContainerStart := envs.tm.RegisterInternal(node.NewContainerStartTask(env))

	ClientContainerCheck := envs.tm.RegisterInternal(node.NewClientContainerCheckTask(env))
	ClientContainerCreate := envs.tm.RegisterInternal(node.NewClientContainerCreateTask(env))
	ClientContainerCheck.SetOnFailure(ClientContainerCreate)
	ClientContainerStart := envs.tm.RegisterInternal(node.NewClientContainerStartTask(env))
	ClientContainerCheck.SetOnSuccess(ClientContainerStart)

	dirs.SetOnSuccess(ImagePull)
	ImagePull.SetOnSuccess(configCheck)

	SnapCheck.SetOnSuccess(SnapImport)
	SnapImport.SetOnSuccess(NetworkCheck)

	clientconfigCheck.SetOnSuccess(SnapCheck)
	configCheck.SetOnSuccess(clientconfigCheck)
	NetworkCheck.SetOnSuccess(VersionCheck)
	VersionCheck.SetOnSuccess(IdCheck)
	IdCheck.SetOnSuccess(ContainerCheck)

	ContainerCheck.SetOnSuccess(ContainerStart)

	ContainerStart.SetOnSuccess(ClientContainerCheck)

	if env.Tezos.Branch == "sandbox" {
		Sandbox := envs.tm.RegisterInternal(node.NewSandboxSetupTask(env))
		ClientContainerStart.SetOnSuccess(Sandbox)
	}

	return dirs
}

func (envs *EnvManager) GetLocalSandboxCreateTask(env *db.Environment) task.Task {
	dirs := envs.tm.RegisterInternal(node.NewNodeStoragePrepareTask(env))

	ImagePull := envs.tm.RegisterInternal(node.NewNodeImagePullTask(env))

	configCheck := envs.tm.RegisterInternal(node.NewNodeConfigCheck(env))
	configCreate := envs.tm.RegisterInternal(node.NewNodeConfigCreate(env))

	configCheck.SetOnFailure(configCreate)

	clientconfigCheck := envs.tm.RegisterInternal(node.NewClientConfigCheck(env))
	clientconfigCreate := envs.tm.RegisterInternal(node.NewClientConfigCreate(env))

	clientconfigCheck.SetOnFailure(clientconfigCreate)

	NetworkCheck := envs.tm.RegisterInternal(node.NewNetworkCheckTask(env))
	NetworkCreate := envs.tm.RegisterInternal(node.NewNetworkCreateTask(env))

	NetworkCheck.SetOnFailure(NetworkCreate)

	VersionCheck := envs.tm.RegisterInternal(node.NewVersionFileCheck(env))
	VersionCreate := envs.tm.RegisterInternal(node.NewNodeVersionFileCreate(env))

	VersionCheck.SetOnFailure(VersionCreate)

	IdCheck := envs.tm.RegisterInternal(node.NewNodeIdentityCheckTask(env))
	IdGen := envs.tm.RegisterInternal(node.NewNodeIdentityCreateTask(env))

	IdCheck.SetOnFailure(IdGen)

	ContainerCheck := envs.tm.RegisterInternal(node.NewContainerCheckTask(env))
	ContainerCreate := envs.tm.RegisterInternal(node.NewContainerCreateTask(env))

	ContainerCheck.SetOnFailure(ContainerCreate)

	ContainerStart := envs.tm.RegisterInternal(node.NewContainerStartTask(env))

	ClientContainerCheck := envs.tm.RegisterInternal(node.NewClientContainerCheckTask(env))
	ClientContainerCreate := envs.tm.RegisterInternal(node.NewClientContainerCreateTask(env))
	ClientContainerCheck.SetOnFailure(ClientContainerCreate)
	ClientContainerStart := envs.tm.RegisterInternal(node.NewClientContainerStartTask(env))
	ClientContainerCheck.SetOnSuccess(ClientContainerStart)

	dirs.SetOnSuccess(ImagePull)
	ImagePull.SetOnSuccess(configCheck)

	clientconfigCheck.SetOnSuccess(NetworkCheck)
	configCheck.SetOnSuccess(clientconfigCheck)
	NetworkCheck.SetOnSuccess(VersionCheck)
	VersionCheck.SetOnSuccess(IdCheck)
	IdCheck.SetOnSuccess(ContainerCheck)

	ContainerCheck.SetOnSuccess(ContainerStart)

	ContainerStart.SetOnSuccess(ClientContainerCheck)

	Sandbox := envs.tm.RegisterInternal(node.NewSandboxSetupTask(env))
	ClientContainerStart.SetOnSuccess(Sandbox)

	return dirs
}

func (envs *EnvManager) GetRemoteEnvCreateTask(env *db.Environment) task.Task {
	dirs := envs.tm.RegisterInternal(node.NewNodeStoragePrepareTask(env))

	ImagePull := envs.tm.RegisterInternal(remote.NewRemoteImagePullTask(env))

	configCheck := envs.tm.RegisterInternal(remote.NewRemoteConfigCheck(env))
	configCreate := envs.tm.RegisterInternal(remote.NewRemoteConfigCreate(env))

	configCheck.SetOnFailure(configCreate)

	clientconfigCheck := envs.tm.RegisterInternal(node.NewClientConfigCheck(env))
	clientconfigCreate := envs.tm.RegisterInternal(node.NewClientConfigCreate(env))

	clientconfigCheck.SetOnFailure(clientconfigCreate)

	NetworkCheck := envs.tm.RegisterInternal(node.NewNetworkCheckTask(env))
	NetworkCreate := envs.tm.RegisterInternal(node.NewNetworkCreateTask(env))

	NetworkCheck.SetOnFailure(NetworkCreate)

	ContainerCheck := envs.tm.RegisterInternal(remote.NewContainerCheckTask(env))
	ContainerCreate := envs.tm.RegisterInternal(remote.NewContainerCreateTask(env))

	ContainerCheck.SetOnFailure(ContainerCreate)

	ContainerStart := envs.tm.RegisterInternal(remote.NewContainerStartTask(env))

	ClientContainerCheck := envs.tm.RegisterInternal(node.NewClientContainerCheckTask(env))
	ClientContainerCreate := envs.tm.RegisterInternal(node.NewClientContainerCreateTask(env))
	ClientContainerCheck.SetOnFailure(ClientContainerCreate)
	ClientContainerStart := envs.tm.RegisterInternal(node.NewClientContainerStartTask(env))
	ClientContainerCheck.SetOnSuccess(ClientContainerStart)

	dirs.SetOnSuccess(ImagePull)
	ImagePull.SetOnSuccess(configCheck)

	clientconfigCheck.SetOnSuccess(NetworkCheck)
	configCheck.SetOnSuccess(clientconfigCheck)

	NetworkCheck.SetOnSuccess(ContainerCheck)

	ContainerCheck.SetOnSuccess(ContainerStart)

	ContainerStart.SetOnSuccess(ClientContainerCheck)

	return dirs
}
