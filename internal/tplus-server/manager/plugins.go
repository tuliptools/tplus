package manager

import (
	"bytes"
	"errors"
	"github.com/gobuffalo/packr"
	"github.com/tyler-sommer/stick"
	config2 "gitlab.com/tuliptools/tplus/internal/tplus-server/config"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/events"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/models"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/service"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task/plugin"
	"gopkg.in/yaml.v2"
	"strconv"
	"strings"
	"sync"
	"time"
)

type PluginsManager struct {
	db          *db.DB
	docker      *service.DockerService
	tm          *task.Manager
	pluginsById map[string]*PluginManager
	pLock       *sync.Mutex
	endpoints   *HttpEndpointManager
	Box         *packr.Box
	config      *config2.Config
	events      *events.Events
}

func NewPluginsManager(tm *task.Manager, events *events.Events, db *db.DB, docker *service.DockerService, end *HttpEndpointManager, box *packr.Box, config *config2.Config) *PluginsManager {

	p := PluginsManager{}
	p.db = db
	p.tm = tm
	p.docker = docker
	p.pluginsById = map[string]*PluginManager{}
	p.pLock = &sync.Mutex{}
	p.endpoints = end
	p.Box = box
	p.events = events.Source("Plugins")
	p.config = config
	go p.init()
	return &p
}

func (pm *PluginsManager) RegisterPlugin(env *db.Environment, pluginYML string) error {

	pluginYML = strings.Replace(pluginYML, "\\\"", "", -1)


	vars := map[string]stick.Value{
		"tezos_privatemode": "false",
		"tezos_historymode": env.Tezos.HistoryMode,
		"docker_network":    env.Tezos.Branch,
		"node":              "http://node:8732",
		"node_port":         "8732",
		"node_hostname":     "node",
		"node_schema":       "http",
		"env_name":          env.Name,
		"rpc_endpoint":      "http://:" + pm.config.PublicIp + strconv.Itoa(pm.config.WebPort) + "/endpoints/" + env.Tezos.Endpoint,
	}

	// register Endpoint stuff before parsing the main yml
	endpointIds := map[string]string{}
	yml := models.PluginYMLRoot{}
	yaml.Unmarshal([]byte(pluginYML), &yml)
	p := db.Plugin{}
	p.Name = yml.Name
	p.Description = yml.Description
	p.Id = db.GetRandomId("plugin")
	for i, s := range yml.Services {
		for j, e := range s.Endpoints {
			mapname := i + "_" + strconv.Itoa(j)
			id := db.GetRandomId("endpoint")
			endpointIds[mapname] = id
			if e.Register != "" {
				en := "http://" + pm.config.PublicIp + ":" + strconv.Itoa(pm.config.WebPort) + "/endpoints/" + id
				vars[e.Register] = strings.Replace(en, ":80", "", 1)
				vars[e.Register+"_ID"] = id
				vars[e.Register+"_PATH"] = "/endpoints/" + id + "/"
			}
		}
	}

	for key, val := range vars {
		vars[strings.ToUpper(key)] = val
	}
	stickEnv := stick.New(nil)
	filebuffer := bytes.Buffer{}
	if err := stickEnv.Execute(pluginYML, &filebuffer, vars); err == nil {

		yml := models.PluginYMLRoot{}
		yaml.Unmarshal(filebuffer.Bytes(), &yml)
		p := db.Plugin{}
		p.Name = yml.Name
		p.Description = yml.Description
		p.Id = db.GetRandomId("plugin")
		p.Vars = vars

		for _, e := range env.Plugins {
			if e.Name == p.Name {
				return errors.New("plugin already exists")
			}
		}

		for name, s := range yml.Services {
			service := &db.PluginService{}
			service.Name = name
			service.Image = s.Image
			service.Env = s.Env
			service.ServiceName = s.ServiceName
			service.Description = s.Description
			service.Endpoints = []db.PluginEndpoints{}
			service.Templates = []db.PluginTemplate{}
			service.Volumes = []db.PluginVolume{}
			service.Entrypoint = s.Entrypoint
			service.Command = s.Command
			service.Volumes = []db.PluginVolume{}
			for i, e := range s.Endpoints {
				newE := db.PluginEndpoints{}
				newE.Name = e.Name
				newE.Id = endpointIds[name+"_"+strconv.Itoa(i)]
				newE.Proto = e.Proto
				newE.Port = e.Port
				newE.Register = e.Register
				newE.Type = e.Type
				newE.HomePath = e.HomePath
				service.Endpoints = append(service.Endpoints, newE)
			}
			for _, v := range s.Volumes {
				vol := db.PluginVolume{}
				vol.Description = v.Description
				vol.Name = v.Name
				vol.Mount = v.Mount
				vol.HostDir = pm.config.DataDir + "/envs/" + env.Id + "/plugins/" + service.Name + "/" + vol.Name
				service.Volumes = append(service.Volumes, vol)
			}
			for _, t := range s.Templates {
				nt := db.PluginTemplate{}
				nt.Name = t.Name
				nt.Dest = t.Dest
				nt.Source = t.Source
				nt.HostFile = pm.config.DataDir + "/envs/" + env.Id + "/plugins/" + service.Name + "/configs/" + nt.Name
				service.Templates = append(service.Templates, nt)
			}
			p.Services = append(p.Services, service)
		}

		if env.Plugins == nil {
			env.Plugins = []db.Plugin{}
		}

		env.Plugins = append(env.Plugins, p)
		pm.db.UpdateEnvironment(env, func(update *db.Environment) *db.Environment {
			update.Plugins = env.Plugins
			return update
		})

	} else {
		return errors.New("Cant parse Services YML")
	}

	return nil
}

func (pm *PluginsManager) getPluginManager(env *db.Environment, x *db.Plugin) *PluginManager {
	p := x
	id := p.Id

	pm.pLock.Lock()

	if val, ok := pm.pluginsById[id]; ok {
		pm.pLock.Unlock()
		return val
	}

	new := PluginManager{}
	new.stickVars = x.Vars
	new.db = pm.db
	new.docker = pm.docker
	new.tm = pm.tm
	new.plugin = p
	new.env = env
	new.endpoints = pm.endpoints
	new.canceled = false
	new.config = pm.config
	new.events = pm.events.With("pluginId",x.Id).With("pluginName",x.Name).WithEnv(env)
	pm.pluginsById[id] = &new
	pm.pLock.Unlock()

	go new.init()

	return &new
}

func (pm *PluginsManager) GetPluginsManager(id string) *PluginManager {
	if val, ok := pm.pluginsById[id]; ok {
		return val
	}
	return nil
}

func (pm *PluginsManager) init() {
	setupEnvs := map[string]bool{}
	for {
		time.Sleep(5*time.Second)
		envs, err := pm.db.GetEnvironments()
		if err == nil {
			for _, e := range envs {
				if e.Tezos.Status == "running" {
					for _, p := range e.Plugins {
						if _,ok := setupEnvs[e.Id + p.Id]; !ok {
							en := e
							ep := p
							pm.getPluginManager(&en, &ep)
							setupEnvs[e.Id +p.Id] = true
						}
					}
				}
			}
		}
	}
}

type PluginManager struct {
	PluginStatus string
	db           *db.DB
	docker       *service.DockerService
	tm           *task.Manager
	env          *db.Environment
	plugin       *db.Plugin
	endpoints    *HttpEndpointManager
	config       *config2.Config
	canceled     bool
	stickVars    map[string]stick.Value
	events       *events.Events
}

func (pm *PluginManager) initStick() {
}

func (pm *PluginManager) GetPlugin() *db.Plugin {
	return pm.plugin
}

func (pm *PluginManager) init() {
	go func() {
		for {
			if pm.canceled {
				break
			}
			time.Sleep(5 * time.Second)
			pm.updateEnv()
			pm.CheckRunning()
		}
	}()
	pm.updateEnv()
	pm.updatePlugin()
	if pm.plugin.Status != "stopped" {
		pm.plugin.Status = "pending"
		pm.Start()
	}

	return
}

func (pm *PluginManager) CheckRunning() {
	if pm.PluginStatus == "running" && pm.plugin.Status == "running" {
		wg := &sync.WaitGroup{}
		for _, s := range pm.plugin.Services {
			if s.Status == "running" {
				sn := s
				go func(sn *db.PluginService) {
					wg.Add(1)
					check := pm.GetServiceCheckTask(sn)
					pm.tm.RunAndBlockWithEvents(check,pm.events.Silent())
					wg.Done()
				}(sn)
				time.Sleep(100 * time.Millisecond)
			}
		}
		wg.Wait()
	}
}


func (pm *PluginManager) Stop() {
	if strings.Contains(pm.PluginStatus,"...") || pm.PluginStatus == "stopped" {
		return
	}
	pm.PluginStatus = "stopping..."
	pm.plugin.Status = "stopping..."
	pm.savePlugin()
	wg := &sync.WaitGroup{}
	for _, s := range pm.plugin.Services {
		go func(s db.PluginService) {
			sn := s
			wg.Add(1)
			pm.events.Info("Stopping PluginService: " + s.Name +" (" + s.Description + ") ...")
			stop := pm.GetServiceStopTask(&sn)
			pm.tm.RunAndBlockWithEvents(stop,pm.events)
			wg.Done()
			pm.events.Info("Stopped PluginService: " + s.Name)
		}(*s)
		time.Sleep(100 * time.Millisecond)
	}
	wg.Wait()
	pm.plugin.Status = "stopped"
	pm.PluginStatus = "stopped"
	pm.savePlugin()
}

func (pm *PluginManager) Start() {
	if strings.Contains(pm.PluginStatus,"...") || pm.PluginStatus == "running"{
		return
	}
	pm.PluginStatus = "starting..."
	pm.plugin.Status = "starting...."
	pm.savePlugin()

	// start pull all images
	for _,s := range pm.plugin.Services {
		pm.docker.PullImageBlocking(s.Image)
	}

	pm.savePlugin()
	wg := sync.WaitGroup{}
	for _, s := range pm.plugin.Services {
		sn := s
		go func(s *db.PluginService) {
			wg.Add(1)
			pm.events.Info("Starting PluginService: " + s.Name +" (" + s.Description + ") ... ")
			start := pm.GetServiceStartTask(s)
			pm.tm.RunAndBlockWithEvents(start,pm.events)
			pm.registerEndpoint(s)
			wg.Done()
			pm.events.Info("Started PluginService: " + s.Name +" (" + s.Description + ")")
		}(sn)
		time.Sleep(500 * time.Millisecond)
	}
	wg.Wait()
	pm.plugin.Status = "running"
	pm.PluginStatus = "running"
	pm.savePlugin()
}

func (pm *PluginManager) Delete() {
	if strings.Contains(pm.PluginStatus,"..."){
		return
	}
	if pm.PluginStatus == "deleted" {
		return
	}
	pm.PluginStatus = "removing..."
	pm.plugin.Status = "removing..."
	pm.savePlugin()
	for _, s := range pm.plugin.Services {
		pm.events.Info("Removing PluginService: " + s.Name +" (" + s.Description + ") ...")
		rm := pm.GetServiceRemoveTask(s)
		pm.tm.RunAndBlockWithEvents(rm,pm.events)
		time.Sleep(500 * time.Millisecond)
		pm.events.Info("Removed PluginService: " + s.Name +" (" + s.Description + ")")
	}
	pm.deletePlugin()
	pm.canceled = true
	pm.plugin.Status = "deleted"
	pm.savePlugin()
}

func (pm *PluginManager) registerEndpoint(s *db.PluginService) {
	if len(s.Endpoints) == 0 {
		return
	}
	envID := strings.Replace(pm.env.Id, "env-", "", -1)
	envID = strings.ToLower(envID)

	sid := strings.ToLower(s.Name)
	sid = strings.Replace(sid, " ", "", -1)
	containerName := "tplus_plugin_" + envID + "_" + sid
	id, _ := pm.docker.GetContainerId(containerName)
	info, err := pm.docker.GetContainerInfo(id)
	ip := ""
	for _, n := range info.NetworkSettings.Networks {
		ip = n.IPAddress
		break
	}
	if err == nil {
		for _, e := range s.Endpoints {
			port := strconv.Itoa(e.Port)
			if e.Type != "redirect" {
				pm.endpoints.RegisterEndpoint(e.Id, "http://"+ip+":"+port)
			} else {
				pm.endpoints.RegisterRedirect(e.Id, "http://"+ip+":"+port)
			}
		}
	}
}

func (pm *PluginManager) GetServiceStartTask(service *db.PluginService) task.Task {
	storage := pm.tm.RegisterInternal(plugin.NewPluginStoragePrepareTask(pm.env, service))
	config := pm.tm.RegisterInternal(plugin.NewPluginConfigPrepareTask(pm.env, service, pm.stickVars))
	container := pm.tm.RegisterInternal(plugin.NewPluginContainerPrepareTask(pm.env, service))
	start := pm.tm.RegisterInternal(plugin.NewPluginContainerStartTask(pm.env, service))
	storage.SetOnSuccess(config)
	config.SetOnSuccess(container)
	container.SetOnSuccess(start)
	return storage
}

func (pm *PluginManager) GetServiceOnlyStartTask(service *db.PluginService) task.Task {
	start := pm.tm.RegisterInternal(plugin.NewPluginContainerStartTask(pm.env, service))
	return start
}

func (pm *PluginManager) GetServiceRemoveTask(service *db.PluginService) task.Task {
	stop := pm.tm.RegisterInternal(plugin.NewPluginContainerRemoveTask(pm.env, service))
	storage := pm.tm.RegisterInternal(plugin.NewPluginStorageDeleteTask(pm.env, service))
	stop.SetOnSuccess(storage)
	return stop
}

func (pm *PluginManager) GetServiceStopTask(service *db.PluginService) task.Task {
	stop := pm.tm.RegisterInternal(plugin.NewPluginContainerStopTask(pm.env, service))
	return stop
}

func (pm *PluginManager) GetServiceCheckTask(service *db.PluginService) task.Task {
	check := pm.tm.RegisterInternal(plugin.NewContainerCheckTask(pm.env, service))
	return check
}

func (pm *PluginManager) updateEnv() {
	e, err := pm.db.GetEnvironmentById(pm.env.Id)
	if err == nil {
		pm.env = e
	}
}

func (pm *PluginManager) updatePlugin() {
	e, err := pm.db.GetEnvironmentById(pm.env.Id)
	if err == nil {
		for _, p := range e.Plugins {
			if p.Id == pm.plugin.Id {
				x := p
				pm.plugin = &x
			}
		}
	}
}

func (pm *PluginManager) savePlugin() {
	pm.events.Debug("Save plugin")
	pm.db.UpdateEnvironment(pm.env, func(update *db.Environment) *db.Environment {
		for i, op := range update.Plugins {
			if op.Name == pm.plugin.Name {
				update.Plugins[i] = *pm.plugin
			}
		}
		return update
	})
}
func (pm *PluginManager) deletePlugin() {
	pm.events.Info("Delete plugin")
	pm.db.UpdateEnvironment(pm.env, func(update *db.Environment) *db.Environment {
		var np []db.Plugin
		for _, op := range update.Plugins {
			if op.Name != pm.plugin.Name {
				np = append(np, op)
			}
		}
		update.Plugins = np
		return update
	})
}
