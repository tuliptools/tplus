package manager

import (
	"archive/tar"
	"bytes"
	"compress/gzip"
	"crypto/md5"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/coreos/etcd/pkg/stringutil"
	"github.com/gobuffalo/packr"
	lru "github.com/hashicorp/golang-lru"
	"github.com/tidwall/gjson"
	config2 "gitlab.com/tuliptools/tplus/internal/tplus-server/config"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/events"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/helpers"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/models"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/service"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/service/docker"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"gopkg.in/src-d/go-git.v4"
	"gopkg.in/yaml.v2"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"
)

type ProjectManager struct {
	db            *db.DB
	docker        *service.DockerService
	tm            *task.Manager
	config        *config2.Config
	box           *packr.Box
	projects      map[string]*EnvProjectManager
	projectsByEnv map[string][]*EnvProjectManager
	pLock         *sync.Mutex
	wallet        *WalletManager
	endpoints     *HttpEndpointManager
	events        *events.Events
	serviceManager *ProjectServiceManager
}

func NewProjectManager(serviceManager *ProjectServiceManager,ev *events.Events, tm *task.Manager, w *WalletManager, db *db.DB, docker *service.DockerService, box *packr.Box, end *HttpEndpointManager, config *config2.Config) *ProjectManager {

	p := ProjectManager{}
	p.db = db
	p.events = ev
	p.tm = tm
	p.docker = docker
	p.projects = map[string]*EnvProjectManager{}
	p.projectsByEnv = map[string][]*EnvProjectManager{}
	p.pLock = &sync.Mutex{}
	p.endpoints = end
	p.config = config
	p.box = box
	p.wallet = w
	p.serviceManager = serviceManager
	go p.init()
	return &p
}

func (pm *ProjectManager) CreateProject(env *db.Environment, name, desc, alias, source, project string) (*EnvProjectManager, error) {
	newp := db.Project{}
	newp.Name = name
	newp.Template = project
	newp.Source = source
	newp.Alias = alias
	newp.Description = desc
	newp.Id = db.GetRandomId("project")
	newp.Created = time.Now()
	newp.LastSave = time.Now()
	newp.LastRead = time.Now()
	newp.EnvId = env.Id
	pm.db.Save(&newp)
	return pm.getProject(env, &newp), nil
}

func (pm *ProjectManager) CreateProjectGit(env *db.Environment, name, desc, alias, giturl string) (*EnvProjectManager, error) {
	newp := db.Project{}
	newp.Name = name
	newp.GitUrl = giturl
	newp.Source = ""
	newp.Alias = alias
	newp.Description = desc
	newp.Id = db.GetRandomId("project")
	newp.Created = time.Now()
	newp.LastSave = time.Now()
	newp.LastRead = time.Now()
	newp.EnvId = env.Id
	pm.db.Save(&newp)
	return pm.getProject(env, &newp), nil
}

func (pm *ProjectManager) CreateProjectImport(env *db.Environment, name, desc, alias, projectId string) (*EnvProjectManager, error) {
	newp := db.Project{}
	newp.Name = name
	newp.GitUrl = ""
	newp.Source = ""
	newp.ClonedFrom = projectId
	newp.Alias = alias
	newp.Description = desc
	newp.Id = db.GetRandomId("project")
	newp.Created = time.Now()
	newp.LastSave = time.Now()
	newp.LastRead = time.Now()
	newp.SetupDone = false
	newp.EnvId = env.Id
	pm.db.Save(&newp)
	return pm.getProject(env, &newp), nil
}

func (pm *ProjectManager) DeleteProject(env *db.Environment, id string) {
	project := pm.GetProjectByEnvId(env, id)
	go project.Cleanup()
	delete(pm.projects, id)
	pm.pLock.Lock()
	defer pm.pLock.Unlock()
	var n []*EnvProjectManager
	for _, nn := range pm.projectsByEnv[env.Id] {
		if nn.project.Id != id {
			n = append(n, nn)
		}
	}
	pm.projectsByEnv[env.Id] = n
	return
}

func (pm *ProjectManager) valdiateAlias(env *db.Environment, alias string) bool {
	p, _ := pm.db.GetProjects()
	for _, pp := range p {
		if pp.EnvId == env.Id && pp.Alias == alias {
			return false
		}
	}
	return true
}

func (pm *ProjectManager) GetProjectByEnvId(env *db.Environment, id string) *EnvProjectManager {
	pm.pLock.Lock()
	if val, ok := pm.projectsByEnv[env.Id]; ok {
		for _, p := range val {
			if p.GetProject().Id == id {
				pm.pLock.Unlock()
				return p
			}
		}
	}
	pm.pLock.Unlock()
	return nil
}

func (pm *ProjectManager) GetProjectById(id string) *EnvProjectManager {
	pm.pLock.Lock()
	for k := range pm.projectsByEnv {
		if val, ok := pm.projectsByEnv[k]; ok {
			for _, p := range val {
				if p.GetProject().Id == id {
					pm.pLock.Unlock()
					return p
				}
			}
		}
	}
	pm.pLock.Unlock()
	return nil
}

func (pm *ProjectManager) GetProjectsForEnv(env *db.Environment) []*EnvProjectManager {
	pm.pLock.Lock()
	defer pm.pLock.Unlock()
	if val, ok := pm.projectsByEnv[env.Id]; ok {
		return val
	}
	return []*EnvProjectManager{}
}

func (pm *ProjectManager) GetProjectsExceptEnv(env *db.Environment) []*EnvProjectManager {
	pm.pLock.Lock()
	defer pm.pLock.Unlock()
	var res []*EnvProjectManager
	for k, v := range pm.projectsByEnv {
		if k != env.Id {
			res = append(res, v...)
		}
	}
	return res
}

func (pm *ProjectManager) getProject(env *db.Environment, p *db.Project) *EnvProjectManager {
	id := env.Id + "::" + p.Id

	pm.pLock.Lock()
	defer pm.pLock.Unlock()

	if val, ok := pm.projects[id]; ok {
		return val
	}

	new := EnvProjectManager{}
	new.pm = pm
	new.db = pm.db
	new.box = pm.box
	new.docker = pm.docker
	new.tm = pm.tm
	new.project = p
	new.config = pm.config
	new.env = env
	new.serviceManager = pm.serviceManager.ForEnv(env).ForProject(p)
	new.wallet = pm.wallet
	new.endpoints = pm.endpoints
	new.fileLock = &sync.Mutex{}
	new.serviceLock = &sync.Mutex{}
	new.services = map[string]*ProjectServiceManager{}
	new.events = pm.events.
		With("projectId", p .Id).
		With("projectName", p.Name)
	new.imageCache, _ = lru.New(100)
	new.projectPath = pm.config.DataDir + "/envs/" + env.Id + "/projects/" + p.Id

	go new.init()

	pm.projects[id] = &new
	if _, ok := pm.projectsByEnv[env.Id]; ok {
		pm.projectsByEnv[env.Id] = append(pm.projectsByEnv[env.Id], &new)
	} else {
		pm.projectsByEnv[env.Id] = []*EnvProjectManager{&new}
	}
	return &new
}

func (pm *ProjectManager) init() {
	projects, err := pm.db.GetProjects()
	if err == nil {
		for _, pi := range projects {
			p := pi
			env, _ := pm.db.GetEnvironmentById(p.EnvId)
			pm.getProject(env, &p)
		}
	} else {
		pm.events.Error(err.Error())
	}
}

/* Project Specific Manager */

type EnvProjectManager struct {
	box         *packr.Box
	db          *db.DB
	docker      *service.DockerService
	tm          *task.Manager
	project     *db.Project
	env         *db.Environment
	endpoints   *HttpEndpointManager
	wallet      *WalletManager
	config      *config2.Config
	projectPath string
	fileLock    *sync.Mutex
	imageCache  *lru.Cache
	pm          *ProjectManager
	events      *events.Events
	services    map[string]*ProjectServiceManager
	serviceLock *sync.Mutex
	serviceManager *ProjectServiceManager
}

type ServiceUpdate struct {
	Running *db.Service
	ShouldBe *db.Service
}

func (envpm *EnvProjectManager) GetProject() *db.Project {
	return envpm.project
}

func (envpm *EnvProjectManager) manageServicesLoop() {
	for {
		envpm.manageServices()
		time.Sleep(5*time.Second)
	}
}

func (envpm *EnvProjectManager) manageServices(){
	envpm.serviceLock.Lock()
	defer envpm.serviceLock.Unlock()

	 existing := []string{}

	 fs := envpm.GetFiles()
	 for _,f := range fs {
		if strings.HasPrefix(f.Path, "services/") {
			s := envpm.readService(f.Path)
			if s != nil {
				existing = append(existing, f.Path)
				if val,ok := envpm.services[f.Path]; ok {
					val.UpdateService(s)
				} else {
					envpm.services[f.Path] = envpm.serviceManager.ForService(s)
				}
			}
		}
	 }

	 for k,_ := range envpm.services {
	 	if !stringInSlice(k,existing){
	 		envpm.services[k].Destroy()
	 		delete(envpm.services,k)
		}
	 }
}

func (envpm *EnvProjectManager) GetServices() []models.ProjectService {
	r := []models.ProjectService{}
	for _,a := range envpm.services {
		r = append(r, a.GetService())
	}
	return r
}

func stringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}


func (envpm *EnvProjectManager) init() {
	go envpm.manageServicesLoop()
	if envpm.project.SetupDone == false {
		os.MkdirAll(envpm.projectPath, 0777)
		if envpm.project.Template != "" && envpm.project.Source == "" {
			url := "https://gitlab.com/tuliptools/tplusdemoprojects/-/raw/master/" + envpm.project.Template
			res, _ := http.Get(url)
			defer res.Body.Close()
			Untar(envpm.projectPath, res.Body)
		}
		if envpm.project.GitUrl != "" {
			git.PlainClone(envpm.projectPath, false, &git.CloneOptions{
				URL:      envpm.project.GitUrl,
				Progress: os.Stdout,
			})
		}

		if envpm.project.ClonedFrom != "" {
			other := envpm.pm.GetProjectById(envpm.project.ClonedFrom)
			ignore := []string{".git", ".idea", ".tplus-project.json"}
			helpers.CopyDirectory(other.projectPath, envpm.projectPath, ignore)
		}

		envpm.project.SetupDone = true
		envpm.save()
	}
}

func (envpm *EnvProjectManager) save() {
	envpm.db.Save(envpm.project)
}

func (envpm *EnvProjectManager) GetFiles() []models.File {
	envpm.fileLock.Lock()
	defer envpm.fileLock.Unlock()
	var res []models.File

	path := envpm.projectPath
	filepath.Walk(path, func(path string, info os.FileInfo, err error) error {
		if info != nil && info.Name() != envpm.project.Id {
			n := models.File{}
			n.Path = strings.Replace(path, envpm.projectPath, "", 1)
			n.Path = n.Path[1:]
			n.Mode = info.Mode().String()
			n.IsDir = info.IsDir()
			if !strings.Contains(n.Path, ".git") && !strings.Contains(n.Path, ".idea") && !strings.Contains(n.Path,".tplus-project.") {
				res = append(res, n)
			}
		}
		return nil
	})

	return res
}

func (envpm *EnvProjectManager) Cleanup() {
	ids, _ := envpm.docker.FindContainersForProject(envpm.project.Id)
	for _, id := range ids {
		envpm.docker.DeleteContainer(id)
	}
	os.RemoveAll(envpm.projectPath)
	envpm.db.Delete(envpm.project)
}


func (envpm *EnvProjectManager) readService(file string) *db.Service{
	path := envpm.projectPath + "/" + file
	service := db.Service{}
	byteRes, e := ioutil.ReadFile(path)
	if e == nil {
		e = yaml.Unmarshal(byteRes, &service)
	}
	if e != nil || service.Identifier == "" {
		//  TODO: report error back to studio
		fmt.Println(e)
		return nil
	} else {
		return &service
	}

}

// reutrns container id
func (envpm *EnvProjectManager) makeServicedocker(service *db.Service) string {
	cname := "tplus_service_" + service.Identifier + "_" + envpm.project.Id
	cname = strings.Replace(strings.Replace(cname,"-","_",-1),"project","",-1)

	id,e := envpm.docker.GetContainerId(cname)

	if e != nil || id == "" {
		templ := docker.GetDefaultTemplate(envpm.project.EnvId)
		templ.ContainerName = cname
		templ.Image = service.Image
		templ.Mounts = map[string]string{}
		for _,m := range service.Mounts {
			templ.Mounts[envpm.projectPath + "/" + m.Source] = m.Destination
		}
	}

	return ""
}


func (envpm *EnvProjectManager) addService(s db.Service){
	for _,ps := range envpm.project.Services {
		if s.Identifier == ps.Identifier {
			return
		}
	}
	s.Status = "running"
	envpm.project.Services = append(envpm.project.Services, s)
}


func (envpm *EnvProjectManager) RunTask(filename string) (*db.ProjectTask, error) {
	task := db.ProjectTask{}
	path := envpm.projectPath + "/tasks/" + filename
	tplusinfopath := envpm.projectPath + "/.tplus.json"

	byteRes, e := ioutil.ReadFile(path)
	if e != nil {
		return &task, errors.New("could not read Task definition")
	}
	yaml.Unmarshal(byteRes, &task)

	info := db.TplusInfo{}
	byteRes2, e := ioutil.ReadFile(tplusinfopath)
	if e != nil {
		return &task, errors.New("could not read .tplus.json")
	}
	json.Unmarshal(byteRes2, &info)

	task.ProjectId = envpm.project.Id
	task.Id = db.GetRandomId("task")
	task.Created = time.Now()
	task.LastSaved = time.Now()
	task.LastSave = time.Now()
	task.Finished = false
	task.Logs = []string{}
	task.Filename = filename
	if task.Cmd == nil {
		task.Cmd = []string{}
	}
	if task.Variables == nil {
		task.Variables = map[string]string{}
	}
	if task.Tasks == nil {
		task.Tasks = []db.RunnableTask{}
	}
	task.TplusInfo = info

	envpm.db.Save(&task)

	ev := envpm.events.With("taskFilename", filename).
		With("projectTaskID",task.GetId()).
		With("projectTaskName",task.Name)
	go envpm.runTaskBG(&task,ev)
	return &task, e
}

func (envpm *EnvProjectManager) runTaskBG(task *db.ProjectTask,ev *events.Events) {
	go envpm.saveTaskBG(task)
	task.Logs = append(task.Logs, "===================================================================")
	task.Logs = append(task.Logs, "#   "+task.Name)
	task.Logs = append(task.Logs, "#   Starting Task at "+time.Now().Format("Mon Jan _2 15:04:05 2006"))
	task.Logs = append(task.Logs, "#   For Project "+envpm.project.Id+" ( "+envpm.project.Name+" )")
	task.Logs = append(task.Logs, "#   In Environment "+envpm.project.EnvId+" ( "+envpm.env.Name+" - "+envpm.env.Tezos.Branch+" ) ")
	task.Logs = append(task.Logs, "===================================================================")
	task.Logs = append(task.Logs, "")

	_, vars, e := envpm.GetVariables()
	if e != nil {
		ev.Error("Could not fetch Task Variables")
		task.Logs = append(task.Logs, "Could not fetch Task Variables")
		task.Finished = true
		task.Result = false
		return
	}
	ev.Info("Pulling Images...")
	e = envpm.pullImages(task)
	if e != nil {
		ev.Error("Stopping Task execution due to error: " +  e.Error())
		task.Logs = append(task.Logs, "Stopping Task execution due to error: ", e.Error())
		task.Finished = true
		task.Result = false
		return
	} else {
		ev.Info("Pull Images Done")
	}

	for i := range task.Tasks {
		e := envpm.runSubTask(task, i, vars, ev)
		if e != nil {
			ev.Error("Stopping Task execution due to error: " +  e.Error())
			task.Logs = append(task.Logs, "Stopping Task execution due to error: ", e.Error())
			task.Finished = true
			task.Result = false
			return
		}
	}

	if len(task.Variables) >= 1 {
		task.Logs = append(task.Logs, "Summary of Variables registered during execution:")
		for a, b := range task.Variables {
			task.Logs = append(task.Logs, "\t"+a+": "+b)
		}
	}

	task.Finished = true
	ev.Info("ProjectTask finished")
	envpm.docker.FixPermissions(envpm.projectPath)
}

func (envpm *EnvProjectManager) runContainerTask(task *db.ProjectTask, i int, vars map[string]string,ev *events.Events) error {

	subtask := task.Tasks[i]

	image := subtask.Image
	if image == "" {
		image = task.Image
	}
	if image == "" {
		image = task.TplusInfo.Tasks.DefaultImage
	}

	if val, ok := task.CustomDockers[image]; ok {
		image = val
	}

	if image == "" {
		ev.Info("using default iamge: registry.gitlab.com/tuliptools/tplus:cli")
		image = "registry.gitlab.com/tuliptools/tplus:cli"
	}

	if vars != nil {
		for i := range subtask.Command {
			for o, n := range vars {
				o = "{{" + o + "}}"
				subtask.Command[i] = strings.Replace(subtask.Command[i], o, n, -1)
			}
		}
		for i := range subtask.Bash {
			for o, n := range vars {
				o = "{{" + o + "}}"
				subtask.Bash[i] = strings.Replace(subtask.Bash[i], o, n, -1)
			}
		}
	}

	timeout, _ := strconv.Atoi(subtask.Timeout)
	if timeout == 0 {
		timeout = 15
	}
	if strings.ToLower(subtask.Blocking) == "false" && timeout <= 15 {
		timeout = 300
	}

	containerDef := docker.GetProjectTaskTemplate(envpm.env, envpm.project, envpm.projectPath, vars)
	containerDef.Image = image
	containerDef.Command = []string{"sleep", strconv.Itoa(timeout)}

	id, e := envpm.docker.TaskContainerCreate(containerDef)
	if e != nil {
		ev.Error("Error creating container")
		task.Logs = append(task.Logs, "Error creating container")
		return e
	}

	e = envpm.docker.ContainerStart(id)
	if e != nil {
		ev.Error("Error starting Container")
		task.Logs = append(task.Logs, "Error starting Container")
		return e
	}

	if strings.ToLower(subtask.Blocking) == "false" {
		go func() {
			for _, c := range subtask.Command {
				task.Logs = append(task.Logs, "Exec: "+c)
				res, e := envpm.docker.ExecInContainerById(id, strings.Split(c, " "), false)
				if e != nil {
					ev.Error("Error running Command")
					task.Logs = append(task.Logs, "Error running Command")
				}
				res2 := bytes.Split(res, []byte("\n"))
				envpm.tasklog(task, i, "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -")
				envpm.tasklog(task, i, "non-blocking Task \""+subtask.Name+"\" hash finished")
				for _, line := range res2 {
					envpm.tasklog(task, i, string(line))
				}
				envpm.tasklog(task, i, "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -")
				envpm.tasklog(task, i, "")
				envpm.docker.ExecInContainerById(id, strings.Split("chmod -cR 777 /tplus", " "), false)
			}
		}()
		time.Sleep(100 * time.Millisecond)
	} else {
		for _, c := range subtask.Command {
			task.Logs = append(task.Logs, "Exec: "+c)
			res, e := envpm.docker.ExecInContainerById(id, strings.Split(c, " "), false)
			if e != nil {
				task.Logs = append(task.Logs, "Error running Command")
				return e
			}
			res2 := bytes.Split(res, []byte("\n"))
			for _, line := range res2 {
				envpm.tasklog(task, i, string(line))
			}
			task.Logs = append(task.Logs, "")
			envpm.docker.ExecInContainerById(id, strings.Split("chmod -cR 777 /tplus", " "), false)
		}
	}

	if strings.ToLower(subtask.Blocking) == "false" {
		go func() {
			for _, c := range subtask.Bash {
				task.Logs = append(task.Logs, "Bash: "+c)
				res, e := envpm.docker.ExecInContainerById(id, []string{"bash", "-c", c + " 2>&1"}, false)
				if e != nil {
					task.Logs = append(task.Logs, "Error running Command")
				}
				res2 := bytes.Split(res, []byte("\n"))
				envpm.tasklog(task, i, "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -")
				envpm.tasklog(task, i, "non-blocking Task \""+subtask.Name+"\" hash finished")
				for _, line := range res2 {
					envpm.tasklog(task, i, string(line))
				}
				envpm.tasklog(task, i, "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -")
				envpm.tasklog(task, i, "")
				envpm.docker.ExecInContainerById(id, strings.Split("chmod -cR 777 /tplus", " "), false)
			}
		}()
		time.Sleep(100 * time.Millisecond)
	} else {
		for _, c := range subtask.Bash {
			task.Logs = append(task.Logs, "Bash: "+c)
			res, e := envpm.docker.ExecInContainerById(id, []string{"bash", "-c", c + " 2>&1"}, false)
			if e != nil {
				task.Logs = append(task.Logs, "Error running Command")
				return e
			}
			res2 := bytes.Split(res, []byte("\n"))
			for _, line := range res2 {
				envpm.tasklog(task, i, string(line))
			}
			task.Logs = append(task.Logs, "")
			envpm.docker.ExecInContainerById(id, strings.Split("chmod -cR 777 /tplus", " "), false)
		}
	}

	task.Logs = append(task.Logs, "===================================================================")
	task.Logs = append(task.Logs, "")
	return nil
}

func (envpm *EnvProjectManager) tasklog(task *db.ProjectTask, i int, log string) {
	subtask := task.Tasks[i]

	if subtask.Register != "" {
		if log == "      This transaction was successfully applied" {
			task.Variables[subtask.Register+"."+"success"] = "true"
			envpm.project.Variables[subtask.Register+"."+"success"] = "true"
		}
		regex := map[string]*regexp.Regexp{
			"op_hash":        regexp.MustCompile("^Operation hash is '(.*)'"),
			"included_block": regexp.MustCompile("^Operation found in block: (\\S*) "),
			"op_fee":         regexp.MustCompile("^ {4}Fee to the baker: ꜩ(.*) "),
			"contract":       regexp.MustCompile("^New contract (.*) originated."),
		}
		for key, reg := range regex {
			if matches := reg.FindAllStringSubmatch(log, -1); len(matches) >= 1 {
				if task.Variables == nil {
					task.Variables = map[string]string{}
				}
				if envpm.project.Variables == nil {
					envpm.project.Variables = map[string]string{}
				}
				task.Variables[subtask.Register+"."+key] = matches[0][1]
				envpm.project.Variables[subtask.Register+"."+key] = matches[0][1]
				envpm.save()
			}
		}
	}

	if len(subtask.ParseJson) >= 1 && subtask.Register != "" {
		for _, path := range subtask.ParseJson {
			c := gjson.Get(log, path).String()
			if c != "" {
				task.Variables[subtask.Register+"."+path] = c
				if envpm.project.Variables == nil {
					envpm.project.Variables = map[string]string{}
				}
				envpm.project.Variables[subtask.Register+"."+path] = c
			}
		}
	}

	if subtask.HideLog != "true" {
		task.Logs = append(task.Logs, log)
	}
}

func (envpm *EnvProjectManager) runSubTask(task *db.ProjectTask, i int, vars map[string]string, ev *events.Events) error {

	subtask := task.Tasks[i]

	ev = ev.With("projectSubTaskName", subtask.Name)
	ev.Info("Starting SubTask")

	for a, b := range envpm.project.Variables {
		vars[a] = b
	}

	if subtask.Include != "" {
		ev.Info("including " + subtask.Include)
		t, e := envpm.RunTask(subtask.Include)
		if e != nil {
			task.Logs = append(task.Logs, "ERROR in included TaskCollection: "+subtask.Include)
			task.Logs = append(task.Logs, "")
			return e
		}
		task.Logs = append(task.Logs, "Including TaskCollection "+subtask.Include+"( "+task.Id+" )")
		// wait until finished or timeout
		start := time.Now()
		for {
			if start.Add(600 * time.Second).Before(time.Now()) {
				ev.Warn("Included TaskCollection timed out")
				break
			}
			if t.Finished == true {
				ev.Info("Included TaskCollection Finished")
				break
			}
		}
		for _, l := range t.Logs {
			task.Logs = append(task.Logs, "\t"+l)
		}
		_, vars, _ = envpm.GetVariables()
		return e
	}

	// TYPE IS_SET
	if subtask.IsSet != "" {
		task.Logs = append(task.Logs, "Checking if variable \""+subtask.IsSet+"\" is non-empty")
		val, ok := envpm.project.Variables[subtask.IsSet]
		if ok {
			task.Logs = append(task.Logs, "Set to: "+val)
		} else {
			task.Logs = append(task.Logs, "Empty!")
			return errors.New("IsSet: error")
		}
		task.Logs = append(task.Logs, "===================================================================")
		task.Logs = append(task.Logs, "")
		return nil
	}

	// TYPE IS_NOT_SET
	if subtask.IsNotSet != "" {
		task.Logs = append(task.Logs, "Checking if variable \""+subtask.IsSet+"\" is empty")
		val, ok := envpm.project.Variables[subtask.IsSet]
		if ok {
			task.Logs = append(task.Logs, "Set to: "+val)
			return errors.New("IsNotSet: error")
		} else {
			task.Logs = append(task.Logs, "Empty :)")
		}
		task.Logs = append(task.Logs, "===================================================================")
		task.Logs = append(task.Logs, "")
		return nil
	}

	// TYPE LOG
	if len(subtask.Log) >= 1 {
		if vars != nil {
			for i := range subtask.Log {
				for o, n := range vars {
					o = "{{" + o + "}}"
					subtask.Log[i] = strings.Replace(subtask.Log[i], o, n, -1)
				}
			}
		}
		for _, l := range subtask.Log {
			task.Logs = append(task.Logs, l)
		}
		task.Logs = append(task.Logs, "===================================================================")
		task.Logs = append(task.Logs, "")
		return nil
	}

	if subtask.Name != "" {
		task.Logs = append(task.Logs, "Running: "+subtask.Name)
	}

	// TYPE DebugVars
	if subtask.DebugVars == "true" {
		task.Logs = append(task.Logs, "Variables:")
		for a, b := range task.Variables {
			task.Logs = append(task.Logs, "\t"+a+": "+b)
		}
	}

	// TYPE WAITBLOCKS
	if subtask.WaitBlocks >= 1 {
		time.Sleep(2000 * time.Millisecond)
		task.Logs = append(task.Logs, "Waiting for "+strconv.Itoa(subtask.WaitBlocks)+" new Blocks")

		// get rpc height
		donechan := make(chan error)

		go func(wait int, donechan chan error) {
			start, e := envpm.getBH()
			if e != nil {
				donechan <- e
				return
			}
			for {
				nh, _ := envpm.getBH()
				if nh >= start+uint(wait) {
					donechan <- nil
					return
				}
				time.Sleep(100 * time.Millisecond)
			}
		}(subtask.WaitBlocks, donechan)

		abortBakechan := make(chan struct{}, 10)
		// if sandbox lets bake blocks
		if envpm.env.Tezos.Branch == "sandbox" {
			task.Logs = append(task.Logs, "Sandbox: start baking")
			go func(done chan struct{}) {
			outer:
				for {
					if strings.ToLower(subtask.Verbose) == "true" {
						ev.Info("Bake Block")
						res := envpm.wallet.BakeVerbose(envpm.env)
						task.Logs = append(task.Logs, res...)
					} else {
						envpm.wallet.Bake(envpm.env)
					}

					select {
					case <-time.Tick(3 * time.Second):
						continue
					case <-done:
						break outer
					}
				}
			}(abortBakechan)
		}

		e := <-donechan
		abortBakechan <- struct{}{}
		if e != nil {
			task.Logs = append(task.Logs, "ERROR: cant wait for new Blocks")
			return e
		}

		task.Logs = append(task.Logs, "Done Waiting for Blocks")
		time.Sleep(2000 * time.Millisecond)
		// sleep to let other commands finish
	}

	if len(subtask.Command) >= 1 || len(subtask.Bash) >= 1 {
		return envpm.runContainerTask(task, i, vars,ev)
	}

	task.Logs = append(task.Logs, "===================================================================")
	task.Logs = append(task.Logs, "")
	return nil
}

func (envpm *EnvProjectManager) getBH() (res uint, e error) {

	path := "http://localhost:" + strconv.Itoa(envpm.config.WebPort) + "/endpoints/" + envpm.env.Tezos.Endpoint + "/chains/main/blocks/head/header"

	var netClient = &http.Client{
		Timeout: time.Second * 3,
	}

	response, err := netClient.Get(path)
	if err != nil {
		return res, err
	}
	h := models.Blockheader{}
	byteRes, e := ioutil.ReadAll(response.Body)
	response.Body.Close()
	json.Unmarshal(byteRes, &h)

	res = h.Level

	return res, e
}

func md5sum(filePath string) (string, error) {
	file, err := os.Open(filePath)
	if err != nil {
		return "", err
	}
	defer file.Close()

	hash := md5.New()
	if _, err := io.Copy(hash, file); err != nil {
		return "", err
	}

	result := hex.EncodeToString(hash.Sum(nil))
	return result, nil
}

func (envpm *EnvProjectManager) pullImages(task *db.ProjectTask) error {

	var images []string
	task.CustomDockers = map[string]string{}

	for _, t := range task.Tasks {
		if len(t.Command) != 0 || len(t.Bash) != 0 {
			if t.Image == "" {
				if task.Image == "" {
					if task.TplusInfo.Tasks.DefaultImage != "" {
						images = append(images, task.TplusInfo.Tasks.DefaultImage)
					} else {
						images = append(images, "registry.gitlab.com/tuliptools/tplus:cli")
					}
				} else {
					images = append(images, task.Image)
				}
			} else {
				images = append(images, t.Image)
			}
		}
	}

	if len(images) == 0 {
		return nil
	}

	task.Logs = append(task.Logs, "Preparing Docker Images....")

	images = unique(images)

	wg := sync.WaitGroup{}
	echan := make(chan error)

	for _, i := range images {
		if strings.HasPrefix(i, "library") {
			i = "docker.io/" + i
		}
		if strings.HasPrefix(i, "customDocker:") {
			dockerfile := strings.Replace(i, "customDocker:", "", -1)
			path := envpm.projectPath
			name, _ := md5sum(path + "/" + dockerfile)
			name = strings.ToLower(name)
			if name == "" {
				return errors.New("cant build custom image")
			}
			name = "tplus_task_" + name[:10]
			e := envpm.docker.Build(path, name, dockerfile)
			if e != nil {
				return e
			}
			task.CustomDockers[i] = name
		} else {
			task.Logs = append(task.Logs, "Pulling image "+i)
			if val, ok := envpm.imageCache.Get(i); !ok && (val == nil || val.(time.Time).Before(time.Now().Add(24*time.Hour))) {
				wg.Add(1)
				go func(i string, eg *sync.WaitGroup, e chan error) {
					_, err := envpm.docker.PullImageBlocking(i)
					if err != nil {
						task.Logs = append(task.Logs, "ERROR: cant pull image "+i)
						e <- errors.New("error pulling " + i)
					} else {
						envpm.imageCache.Add(i, time.Now())
						wg.Done()
					}
				}(i, &wg, echan)
			} else {
				task.Logs = append(task.Logs, "Skipping pull for "+i)
			}
		}
	}

	go func(wg *sync.WaitGroup, e chan error) {
		wg.Wait()
		e <- nil
	}(&wg, echan)

	err := <-echan
	if err != nil {
		return err
	} else {
		if len(images) == 1 {
			task.Logs = append(task.Logs, "Image updated")
		}
		if len(images) >= 2 {
			task.Logs = append(task.Logs, "All images updated")
		}
		task.Logs = append(task.Logs, "===================================================================")
		task.Logs = append(task.Logs, "")
	}

	return nil
}

func unique(strSlice []string) []string {
	keys := make(map[string]bool)
	var list []string
	for _, entry := range strSlice {
		if _, value := keys[entry]; !value {
			keys[entry] = true
			list = append(list, entry)
		}
	}
	return list
}

func (envpm *EnvProjectManager) saveTaskBG(task *db.ProjectTask) {
	old := ""
	for {
		b,_ := json.Marshal(task)
		if string(b) != old {
			if task.Finished == false {
				task.LastSave = time.Now()
				task.LastRead = time.Now()
				envpm.db.Save(task)
			} else {
				envpm.db.Save(task)
				break
			}
		}
		time.Sleep(50 * time.Millisecond)
	}

}

func (envpm *EnvProjectManager) GetTask(id string) (*db.ProjectTask, error) {
	return envpm.db.GetProjectTaskById(id)
}

func (envpm *EnvProjectManager) MakeApiRequest(url,body,method string) (string, error) {
	ev := envpm.events.WithMany(map[string]string{
		"url": url,
		"method": method,
	})

	method = strings.ToUpper(method)

	tl := docker.GetDefaultTemplate(envpm.env.Id)
	tl.Image = "docker.io/tuliptools/alpine-curl:latest"
	if _,ok := envpm.imageCache.Get(tl.Image); !ok {
		_,e := envpm.docker.PullImageBlocking(tl.Image)
		if e != nil {
			return "",e
		}
		envpm.imageCache.Add(tl.Image,time.Now())
	}

	b := ""
	if body != "{}" && body != "" && method != "GET" {
		b = "-d '" + body + "'"
	}
	cmd := "curl -v " + b + "-H \"Content-Type: application/json\" -X " + method + " " + url + " 2>&1"
	cmd = strings.Replace(cmd, "-X GET","",1)
	tl.Command = []string{"sleep","5"}
	tl.ContainerName = "tplus_apihelper_" + stringutil.RandomStrings(10,1)[0]

	id, e := envpm.docker.TaskContainerCreate(tl)
	if e != nil {
		ev.Error("Error creating container")
		return "",e
	}

	e = envpm.docker.ContainerStart(id)
	if e != nil {
		ev.Error("Error starting Container")
		return "", e
	}

	res,e := envpm.docker.ExecInContainerById(id,[]string{"sh","-c",cmd},false)

	go envpm.docker.DeleteContainer(id)

	// pretify
	splits := strings.Split(string(res),"> "+ method)
	if len(splits) == 2 {
		return "> "+ method + splits[1],e
	}
	return string(res),e

}

func (envpm *EnvProjectManager) GetTaskLogs() (res []db.ProjectTask, e error) {

	tasks, _ := envpm.db.GetProjectTasks()
	for _, t := range tasks {
		if t.ProjectId == envpm.project.Id {
			res = append(res, t)
		}
	}

	sort.Slice(res, func(i, j int) bool {
		return res[i].Created.After(res[j].Created)
	})

	if len(res) >= 20 {
		res = res[:20]
	}

	return res, e
}

func (envpm *EnvProjectManager) GetTasks() (res []models.Runnable, e error) {
	envpm.fileLock.Lock()
	defer envpm.fileLock.Unlock()
	if val, e := os.Stat(envpm.projectPath + "/tasks"); e != nil {
		if val != nil && val.IsDir() != true {
			return res, errors.New("cannot find any Tasks")
		}
	}
	path := envpm.projectPath + "/tasks"

	var files []string

	filepath.Walk(path, func(path string, info os.FileInfo, err error) error {
		if strings.HasSuffix(path, ".yml") {
			files = append(files, path)
		}
		return nil
	})

	for _, f := range files {
		byteRes, _ := ioutil.ReadFile(f)
		n := models.Runnable{}
		e := yaml.Unmarshal(byteRes, &n)
		n.Filename = strings.Replace(f, envpm.projectPath+"/tasks/", "", 1)
		if e != nil {
			n.Error = "Cannot parse Task definition"
		}
		if n.Name != "" {
			res = append(res, n)
		}

	}

	return res, nil
}

func (envpm *EnvProjectManager) SetVariables(v map[string]string) {
	if envpm.project.Variables == nil {
		envpm.project.Variables = v
		envpm.save()
		return
	}
	for a, b := range v {
		envpm.project.Variables[a] = b
	}
	envpm.save()
}

func (envpm *EnvProjectManager) GetVariables() (res []models.TaskVariable, compiled map[string]string, e error) {
	envpm.fileLock.Lock()
	defer envpm.fileLock.Unlock()
	compiled = map[string]string{}
	res = []models.TaskVariable{}

	if val, e := os.Stat(envpm.projectPath + "/tasks"); e != nil {
		if val != nil && val.IsDir() != true {
			return res, compiled, errors.New("cannot find any Tasks")
		}
	}
	path := envpm.projectPath + "/tasks"

	var files []string

	filepath.Walk(path, func(path string, info os.FileInfo, err error) error {
		if strings.HasSuffix(path, ".yml") {
			files = append(files, path)
		}
		return nil
	})

	for _, f := range files {
		byteRes, _ := ioutil.ReadFile(f)
		n := db.ProjectTask{}
		e := yaml.Unmarshal(byteRes, &n)
		n.Filename = strings.Replace(f, envpm.projectPath+"/tasks/", "", 1)
		if e != nil {
			n.Error = "Cannot parse Task definition"
		}

		for _, v := range n.InitVariables {
			nn := models.TaskVariable{
				Name:        v.Name,
				Description: v.Description,
				Type:        strings.ToLower(v.Type),
				Editable:    true,
			}
			if val, ok := envpm.project.Variables[v.Name]; !ok {
				nn.Value = v.Default
			} else {
				nn.Value = val
			}
			add := true
			for _, a := range res {
				if a.Name == nn.Name {
					add = false
				}
			}
			if add {
				res = append(res, nn)
			}
		}

	}

	for a, b := range envpm.project.Variables {
		compiled[a] = b
	}

	addresses := envpm.wallet.GetAddresses(envpm.env)

	for _, v := range res {
		if v.Type == "string" {
			compiled[v.Name] = v.Value
		}
		if v.Type == "integer" {
			_, e := strconv.Atoi(v.Value)
			if e == nil {
				compiled[v.Name] = v.Value
			} else {
				compiled[v.Name] = " [ Error casting var to int ]"
			}
		}
		if v.Type == "address" {
			success := false
			for _, a := range addresses.Addresses {
				if a.Alias == v.Value {
					success = true
					compiled[v.Name] = a.Alias
					compiled[v.Name+".hash"] = a.Address
					if a.Delegate != "" {
						compiled[v.Name+".delegate"] = a.Delegate
					}
					compiled[v.Name+".balance"] = a.Balance
					details := envpm.wallet.GetAddressDetails(envpm.env, v.Value)
					if details != nil {
						compiled[v.Name+".pubKey"] = details.Pubkey
						compiled[v.Name+".secretKey"] = details.SecretKey
					}
					break
				}
			}

			if success == false {
				compiled[v.Name+".error"] = "Address not found in Wallet"
			}
		}
	}

	return res, compiled, nil
}

func (envpm *EnvProjectManager) GetFile(name string) (string, error) {
	path := envpm.projectPath + "/" + name
	file, err := ioutil.ReadFile(path)
	return string(file), err
}

func (envpm *EnvProjectManager) WriteFile(name,content string) (error) {
	path := envpm.projectPath + "/" + name
	f,e := os.Stat(path)
	if f == nil || e == nil {
		_,_ = os.Create(path)
		f,_ = os.Stat(path)
	}
	err := ioutil.WriteFile(path,[]byte(content),f.Mode())
	return err
}

func (envpm *EnvProjectManager) Mkdir(name string) (error) {
	path := envpm.projectPath + "/" + name
	return os.Mkdir(path,666)
}

func (envpm *EnvProjectManager) RemoveFile(name string) (error) {
	path := envpm.projectPath + "/" + name
	return os.Remove(path)
}

func (envpm *EnvProjectManager) RemoveDir(name string) (error) {
	path := envpm.projectPath + "/" + name
	return os.RemoveAll(path)
}

func (envpm *EnvProjectManager) RenameFile(old,new string) (error) {
	of,_ := os.Stat(old)
	nf,_ := os.Stat(old)
	if of.IsDir() && !nf.IsDir(){
		return errors.New("Cant move directory into file")
	}
	c,e := envpm.GetFile(old)
	if e == nil {
		envpm.RemoveFile(old)
		envpm.WriteFile(new,c)
	}
	return e
}

func (envpm *EnvProjectManager) Down() (*bytes.Buffer, error) {
	var buf bytes.Buffer
	e := compress(envpm.projectPath, &buf)
	time.Sleep(10 * time.Millisecond)
	return &buf, e
}

func (envpm *EnvProjectManager) Up(reader io.Reader) error {
	RemoveContents(envpm.projectPath)
	time.Sleep(100 * time.Millisecond)
	envpm.docker.FixPermissions(envpm.projectPath)
	return Untar(envpm.projectPath, reader)
}

func RemoveContents(dir string) error {
	d, err := os.Open(dir)
	if err != nil {
		return err
	}
	defer d.Close()
	names, err := d.Readdirnames(-1)
	if err != nil {
		return err
	}
	for _, name := range names {
		os.RemoveAll(filepath.Join(dir, name))
	}
	os.RemoveAll(dir)
	return nil
}

func compress(src string, buf io.Writer) error {
	zr := gzip.NewWriter(buf)
	tw := tar.NewWriter(zr)

	filepath.Walk(src, func(file string, fi os.FileInfo, err error) error {
		fileClean := strings.Replace(file, src, "", 1)
		header, err := tar.FileInfoHeader(fi, fileClean)
		if err != nil {
			return err
		}

		header.Name = filepath.ToSlash(fileClean)
		if err := tw.WriteHeader(header); err != nil {
			return err
		}
		if !fi.IsDir() {
			data, err := os.Open(file)
			if err != nil {
				return err
			}
			if _, err := io.Copy(tw, data); err != nil {
				return err
			}
			defer data.Close()
		}
		return nil
	})
	if err := tw.Close(); err != nil {
		return err
	}
	if err := zr.Close(); err != nil {
		return err
	}
	return nil
}

func Untar(dst string, r io.Reader) error {
	gzr, err := gzip.NewReader(r)
	if err != nil {
		return err
	}
	defer gzr.Close()

	tr := tar.NewReader(gzr)

	for {
		header, err := tr.Next()

		switch {
		case err == io.EOF:
			return nil
		case err != nil:
			return err
		case header == nil:
			continue
		}

		target := filepath.Join(dst, header.Name)

		switch header.Typeflag {
		case tar.TypeDir:
			if _, err := os.Stat(target); err != nil {
				if err := os.MkdirAll(target, 0777); err != nil {
					return err
				}
			}
		case tar.TypeReg:
			f, err := os.OpenFile(target, os.O_CREATE|os.O_RDWR, 0777)
			if err != nil {
				return err
			}
			if _, err := io.Copy(f, tr); err != nil {
				return err
			}
			f.Close()
		}
	}
}
