package manager

import (
	"encoding/json"
	"fmt"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/config"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/events"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/models"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/service"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/service/docker"
	"strings"
	"sync"
)

type ProjectServiceManager struct {
	docker *service.DockerService
	project *db.Project
	service *db.Service
	config *config.Config
	projectPath string
	events  *events.Events
	env     *db.Environment
	oldJson []byte
	http *HttpEndpointManager
	cname string
	cid string
	lock *sync.Mutex
	template *docker.Template
	endpoints []models.ProjectServiceEndpoint
}

func NewProjectServiceManager(docker *service.DockerService, events *events.Events,manager *HttpEndpointManager, config *config.Config) *ProjectServiceManager {
	n := ProjectServiceManager{
		docker: docker,
		events: events.Source("ProjectServiceManager"),
		http: manager,
		config: config,
	}
	return &n
}

func (this *ProjectServiceManager) ForEnv(env *db.Environment) *ProjectServiceManager{
	n := ProjectServiceManager{
		docker: this.docker,
		project: nil,
		service: nil,
		env: env,
		config: this.config,
		http: this.http,
		events: this.events.WithMany(map[string]string{
			"env_id":  env.Id,
			"env": env.Name,
		}),
	}
	return &n
}

func (this *ProjectServiceManager) ForProject(p *db.Project) *ProjectServiceManager{
	n := ProjectServiceManager{
		docker: this.docker,
		env: this.env,
		project: p,
		service: nil,
		http: this.http,
		config: this.config,
		events: this.events.WithMany(map[string]string{
			"project_id":  p.Id,
			"project": p.Name,
		}),
	}
	return &n
}

func (this *ProjectServiceManager) ForService(s *db.Service) *ProjectServiceManager{
	fmt.Println(s)
	s.Status = "pending"
	n := ProjectServiceManager{
		docker: this.docker,
		env: this.env,
		project: this.project,
		service: s,
		oldJson: []byte{},
		http: this.http,
		config: this.config,
		endpoints: []models.ProjectServiceEndpoint{},
		events: this.events.WithMany(map[string]string{
			"service":  s.Identifier,
		}),
	}
	n.projectPath = n.config.DataDir + "/envs/" + this.env.Id + "/projects/" + this.project.Id
	n.lock = &sync.Mutex{}
	go n.UpdateService(s)
	return &n
}


// called when service no longer exists
func (this *ProjectServiceManager) Destroy() {


}

func (this *ProjectServiceManager) prepContainer() {
	cname := "tplus_"+ this.service.Identifier + "_" + this.project.Id
	cname = strings.Replace(strings.Replace(cname,"-","_",-1),"project","",-1)
	cname = strings.Replace(cname,"__","_",-1)
	this.cname = cname

	if this.cid == "" {
		this.cid,_ = this.docker.GetContainerId(this.cname)
	}
}


func (this *ProjectServiceManager) GetService() models.ProjectService {
	r := models.ProjectService{
		Name: this.service.Name,
		Identifier: this.service.Identifier,
		Endpoints: this.endpoints,
		Status: this.service.Status,
	}

	return r
}

// called form pm when reading new service
func (this *ProjectServiceManager) UpdateService(s *db.Service) {
	this.lock.Lock()
	defer this.lock.Unlock()
	this.service = s
	if this.service.Status == "" {
		this.service.Status = "running"
	}
	this.prepContainer()
	nJson,_ := json.Marshal(s)
	if string(nJson) != string(this.oldJson) {
		this.oldJson = nJson
	}

	// first run
	if this.service.Status == "pending" {
		id := ""
		var e error
		id,_ = this.docker.GetContainerId(this.cname)
		if id == "" {
			t := this.getTemplate()
			id,e = this.docker.ContainerCreate(t)
		}
		this.cid = id
		if e == nil {
			e = this.docker.ContainerStart(id)
		}
		if e == nil {
			this.service.Status = "running"
		}
		if e != nil {
			this.events.Error(e.Error())
		}
	}



	// last step: endpoints:
	ip,e := this.docker.GetContainerIP(this.cname)
	if e == nil {
		ne := []models.ProjectServiceEndpoint{}
		for _,a := range this.service.Endpoints {
			id1 := a.Name + a.Port
			create := true
			for _,b := range this.endpoints {
				id2 := b.Name + b.Port
				if id1 == id2 {
					ne = append(ne, b)
					create = false
				}
			}
			if create {
				ty := strings.ToLower(a.Type)
				if ty == "web" || ty == "api"{
					id := db.GetRandomId("endpoint")
					url := "http://" + ip + ":" + a.Port
					this.http.RegisterRedirect(id,url)
					ne = append(ne, models.ProjectServiceEndpoint{
						Name: a.Name,
						Port: a.Port,
						Id:  id,
						Type: a.Type,
					})
				}
			}

		}
		this.endpoints = ne
	} else {
		this.events.Error(e.Error())
	}

}

func (this *ProjectServiceManager) getTemplate() docker.Template{
	template := docker.GetDefaultTemplate(this.env.Id)
	template.Image = this.service.Image
	template.ContainerName = this.cname
	template.Mounts = map[string]string{}
	for _,m := range this.service.Mounts {
		template.Mounts[this.projectPath + "/" + m.Source ] = m.Destination
	}


	return template
}