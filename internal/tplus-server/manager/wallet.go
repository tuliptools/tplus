package manager

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/config"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/models"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/service"
	"io/ioutil"
	"net/http"
	"os"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"time"
)

type WalletManager struct {
	db          *db.DB
	docker      *service.DockerService
	pendingLock *sync.Mutex
	pendingOps  map[string][]string
	config      *config.Config
}

func NewWalletManager(db *db.DB, d *service.DockerService, c *config.Config) *WalletManager {
	e := WalletManager{}
	e.db = db
	e.docker = d
	e.pendingLock = &sync.Mutex{}
	e.pendingOps = map[string][]string{}
	e.config = c
	return &e
}

func (wm *WalletManager) addOp(envid, msg string) {
	wm.pendingLock.Lock()
	defer wm.pendingLock.Unlock()
	if _, ok := wm.pendingOps[envid]; !ok {
		wm.pendingOps[envid] = []string{msg}
	} else {
		wm.pendingOps[envid] = append(wm.pendingOps[envid], msg)
	}
}

func (wm *WalletManager) removeOp(envid, msg string) {
	wm.pendingLock.Lock()
	defer wm.pendingLock.Unlock()
	old := wm.pendingOps[envid]
	wm.pendingOps[envid] = []string{}
	for _, a := range old {
		if a != msg {
			wm.pendingOps[envid] = append(wm.pendingOps[envid], msg)
		}
	}
}

func (wm *WalletManager) GetAddresses(env *db.Environment) models.WalletAddressesResponse {
	res := models.WalletAddressesResponse{}
	walletContainer := env.ClientContainer.Id
	info, err := wm.docker.GetContainerInfo(walletContainer)
	if err != nil || info.State.Running == false {
		res.Success = false
		res.Error = "Cant get Addresses, Node running?"
		return res
	}
	res.Success = true

	cmd := strings.Split("tezos-client list known contracts", " ")
	cmdRes, err := wm.docker.ExecInContainerById(walletContainer, cmd, false)

	var addresses []models.WalletAddress
	if err != nil {
		res.Success = false
		res.Error = "Cant get Addresses..."
		return res
	}
	addresses = wm.parseAddresses(cmdRes, addresses, env)

	res.Addresses = addresses
	return res
}

func (wm *WalletManager) GetAddressDetails(env *db.Environment, address string) *models.AddressDetailsResponse {
	if strings.HasPrefix(address, "KT") || strings.HasPrefix(address, "SG") {
		return nil
	}
	res := models.AddressDetailsResponse{}
	walletContainer := env.ClientContainer.Id
	info, err := wm.docker.GetContainerInfo(walletContainer)
	if err != nil || info.State.Running == false {
		return &res
	}

	cmd := strings.Split("tezos-client show address "+address+" -S", " ")
	cmdRes, err := wm.docker.ExecInContainerById(walletContainer, cmd, false)
	res = wm.parseAddress(cmdRes)

	return &res
}

func (wm *WalletManager) NewAddress(env *db.Environment, name string) models.WalletAddressesResponse {
	res := models.WalletAddressesResponse{}
	walletContainer := env.ClientContainer.Id
	info, err := wm.docker.GetContainerInfo(walletContainer)
	if err != nil || info.State.Running == false {
		res.Success = false
		res.Error = "Cant get Addresses, Node running?"
		return res
	}
	res.Success = true

	cmd := strings.Split("tezos-client gen keys "+name, " ")
	_, err = wm.docker.ExecInContainerById(walletContainer, cmd, false)

	return wm.GetAddresses(env)
}

func (wm *WalletManager) NewAlias(env *db.Environment, name string, address string) models.WalletAddressesResponse {
	res := models.WalletAddressesResponse{}
	walletContainer := env.ClientContainer.Id
	info, err := wm.docker.GetContainerInfo(walletContainer)
	if err != nil || info.State.Running == false {
		res.Success = false
		res.Error = "Cant get Addresses, Node running?"
		return res
	}
	res.Success = true

	if address[:2] == "KT" {
		cmd := strings.Split("tezos-client remember contract "+name+" "+address, " ")
		_, err = wm.docker.ExecInContainerById(walletContainer, cmd, false)
	} else {
		cmd := strings.Split("tezos-client add address "+name+" "+address, " ")
		_, err = wm.docker.ExecInContainerById(walletContainer, cmd, false)
	}

	return wm.GetAddresses(env)
}

func (wm *WalletManager) NewFaucet(env *db.Environment, name string, faucet []byte) models.WalletAddressesResponse {
	res := models.WalletAddressesResponse{}
	walletpath := env.Tezos.ClientDataDir + "/faucet_files"
	os.Mkdir(walletpath, 0777)
	walletfile := db.GetRandomString(24) + ".json"
	ioutil.WriteFile(walletpath+"/"+walletfile, faucet, 0777)

	walletContainer := env.ClientContainer.Id
	info, err := wm.docker.GetContainerInfo(walletContainer)
	if err != nil || info.State.Running == false {
		res.Success = false
		res.Error = "Cant get Addresses, Node running?"
		return res
	}
	res.Success = true

	cmd := strings.Split("tezos-client activate account "+name+" with /root/.tezos-client/faucet_files/"+walletfile, " ")
	_, err = wm.docker.ExecInContainerById(walletContainer, cmd, false)

	return wm.GetAddresses(env)
}

func (wm *WalletManager) DeleteKey(env *db.Environment, name string) models.WalletAddressesResponse {
	res := models.WalletAddressesResponse{}
	walletContainer := env.ClientContainer.Id
	info, err := wm.docker.GetContainerInfo(walletContainer)
	if err != nil || info.State.Running == false {
		res.Success = false
		res.Error = "Cant get Addresses, Node running?"
		return res
	}
	res.Success = true

	cmd := strings.Split("tezos-client forget address "+name+" --force", " ")
	_, err = wm.docker.ExecInContainerById(walletContainer, cmd, false)
	cmd = strings.Split("tezos-client forget contract "+name, " ")
	_, err = wm.docker.ExecInContainerById(walletContainer, cmd, false)

	return wm.GetAddresses(env)
}

func (wm *WalletManager) GetPendingOps(env *db.Environment) []string {
	wm.pendingLock.Lock()
	defer wm.pendingLock.Unlock()
	if val, ok := wm.pendingOps[env.Id]; ok {
		return val
	} else {
		return []string{}
	}
}

func (wm *WalletManager) Delegate(env *db.Environment, from string, to string) models.WalletAddressesResponse {
	res := models.WalletAddressesResponse{}
	walletContainer := env.ClientContainer.Id
	info, err := wm.docker.GetContainerInfo(walletContainer)
	if err != nil || info.State.Running == false {
		res.Success = false
		res.Error = "Cant get Addresses, Node running?"
		return res
	}
	res.Success = true

	cmd := strings.Split("tezos-client set delegate for "+from+" to "+to, " ")
	go func() {
		msg := "Setting delegate for " + from + " to " + to
		wm.addOp(env.Id, msg)
		wm.docker.ExecInContainerById(walletContainer, cmd, false)
		wm.removeOp(env.Id, msg)
	}()

	return wm.GetAddresses(env)
}

func (wm *WalletManager) Bake(env *db.Environment) models.WalletAddressesResponse {
	res := models.WalletAddressesResponse{}
	walletContainer := env.ClientContainer.Id
	info, err := wm.docker.GetContainerInfo(walletContainer)
	if err != nil || info.State.Running == false {
		res.Success = false
		res.Error = "Cant get Addresses, Node running?"
		return res
	}
	res.Success = true

	cmd := strings.Split("tezos-client bake for baker", " ")
	go func() {
		wm.docker.ExecInContainerById(walletContainer, cmd, false)
	}()

	return wm.GetAddresses(env)
}

func (wm *WalletManager) BakeVerbose(env *db.Environment) []string {
	walletContainer := env.ClientContainer.Id
	info, err := wm.docker.GetContainerInfo(walletContainer)
	if err != nil || info.State.Running == false {
		return []string{"error whenbaking!"}
	}

	cmd := strings.Split("tezos-client bake for baker", " ")
	res, e := wm.docker.ExecInContainerById(walletContainer, cmd, false)
	if e != nil || info.State.Running == false {
		return []string{"error whenbaking!"}
	}
	res2 := bytes.Split(res, []byte("\n"))

	var returnRes []string
	for _, l := range res2 {
		returnRes = append(returnRes, string(l))
	}
	return returnRes
}

func (wm *WalletManager) Transfer(env *db.Environment, from string, to string, amount string) models.WalletAddressesResponse {
	res := models.WalletAddressesResponse{}
	walletContainer := env.ClientContainer.Id
	info, err := wm.docker.GetContainerInfo(walletContainer)
	if err != nil || info.State.Running == false {
		res.Success = false
		res.Error = "Cant get Addresses, Node running?"
		return res
	}
	res.Success = true

	cmdString := "transfer " + amount + " from " + from + " to " + to
	cmd := strings.Split("tezos-client "+cmdString+" --burn-cap 0.257", " ")
	go func() {
		msg := cmdString
		wm.addOp(env.Id, msg)
		wm.docker.ExecInContainerById(walletContainer, cmd, false)
		wm.removeOp(env.Id, msg)
	}()

	return wm.GetAddresses(env)
}

func (wm *WalletManager) getContractInfo(ep string, address string) models.RPCContract {
	contract := models.RPCContract{}
	url := "http://localhost:" + strconv.Itoa(wm.config.WebPort) + "/endpoints/" + ep + "/chains/main/blocks/head/context/contracts/" + address
	client := http.Client{
		Timeout: 2 * time.Second,
	}
	res, err := client.Get(url)
	if err != nil {
		fmt.Println("Error in WalletManager: ", err)
		return contract
	}
	body, _ := ioutil.ReadAll(res.Body)
	json.Unmarshal(body, &contract)
	res.Body.Close()
	return contract
}

func (wm *WalletManager) parseAddresses(cmdRes []byte, addresses []models.WalletAddress, env *db.Environment) []models.WalletAddress {
	lines := bytes.Split(cmdRes, []byte("\n"))
	for _, l := range lines {
		if bytes.Contains(l, []byte("tz1")) || bytes.Contains(l, []byte("tz2")) || bytes.Contains(l, []byte("tz3")) || bytes.Contains(l, []byte("SG")) || bytes.Contains(l, []byte("KT")) {
			e := bytes.Split(l, []byte(": "))
			addresses = append(addresses, models.WalletAddress{
				Address: string(e[1][:36]),
				Alias:   string(e[0]),
				Balance: "0",
			})
		}
	}

	for i, a := range addresses {
		c := wm.getContractInfo(env.Tezos.Endpoint, a.Address)
		addresses[i].Balance = c.Balance
		addresses[i].Delegate = c.Delegate
		addresses[i].Counter = c.Counter
	}

	return addresses
}

func (wm *WalletManager) parseAddress(cmdRes []byte) models.AddressDetailsResponse {
	lines := bytes.Split(cmdRes, []byte("\n"))
	addr := models.AddressDetailsResponse{}

	regex := map[string]*regexp.Regexp{
		"hash":   regexp.MustCompile("^Hash (.*)"),
		"pub":    regexp.MustCompile("^Public Key: (\\S*)"),
		"secret": regexp.MustCompile("^Secret Key: (.*)"),
	}
	for _, l := range lines {
		for key, reg := range regex {
			if matches := reg.FindAllStringSubmatch(string(l), -1); len(matches) >= 1 {
				switch key {
				case "hash":
					addr.Hash = matches[0][1]
				case "pub":
					addr.Pubkey = matches[0][1]
				case "secret":
					addr.SecretKey = matches[0][1]

				}
			}
		}
	}

	return addr
}
