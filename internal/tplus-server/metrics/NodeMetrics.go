package metrics

import (
	"bufio"
	"encoding/json"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/config"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/service"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/tezcon"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

type NodeMetrics struct {
	env     *db.Environment
	save    func(metric *db.Metric)
	Running bool
	docker  *service.DockerService
	bhready bool
	config  *config.Config
}

func NewNodeMetrics(env *db.Environment, savefnc func(metric *db.Metric), docker *service.DockerService, config2 *config.Config) *NodeMetrics {
	nm := NodeMetrics{}
	nm.env = env
	nm.save = savefnc
	nm.Running = true
	nm.docker = docker
	nm.bhready = false
	nm.config = config2
	go nm.Init()
	return &nm
}

func (nmetrics *NodeMetrics) checkBHReady() {

	// we dont want to bombard the node with http request if it is not
	// ready for them yet, check it syncing started via logs, then send http gets
	logs, err := nmetrics.docker.GetLogs(nmetrics.env.Tezos.Container.Id, false, "100")
	count := 0
	scanner := bufio.NewScanner(logs)
	if err != nil {
		return
	}
	for scanner.Scan() {
		line := scanner.Text()
		if strings.Contains(line, "successfully validated") {
			count++
			if count == 3 {
				nmetrics.bhready = true
				break
			}
		}
	}
}

func (nmetrics *NodeMetrics) IsRunning() bool {
	return nmetrics.Running
}

func (nmetrics *NodeMetrics) saveMetrics() {
	stat := nmetrics.docker.GetContainerStats(nmetrics.env.Tezos.Container.Id)
	mem := stat.GetMemoryUsage()
	cpu := stat.CalculateCPUPercent()
	netIn := stat.GetNetworkIn()
	netOut := stat.GetNetworkOut()
	if mem == 0 || cpu == 0 {
		nmetrics.Running = false
		return
		// metrics will spawn a new NodeMetrics if container state changes
	}

	mmem := db.Metric{Time: time.Now(), EnvironmentId: nmetrics.env.Id, Source: "Tezos", Metric: "mem", Value: mem}
	mcpu := db.Metric{Time: time.Now(), EnvironmentId: nmetrics.env.Id, Source: "Tezos", Metric: "cpu", Value: cpu}
	mnetIn := db.Metric{Time: time.Now(), EnvironmentId: nmetrics.env.Id, Source: "Tezos", Metric: "netIn", Value: netIn}
	mnetOut := db.Metric{Time: time.Now(), EnvironmentId: nmetrics.env.Id, Source: "Tezos", Metric: "netOut", Value: netOut}

	nmetrics.save(&mmem)
	nmetrics.save(&mcpu)
	nmetrics.save(&mnetIn)
	nmetrics.save(&mnetOut)

	storage, err := nmetrics.DirSize(nmetrics.env.Tezos.DataDir)
	if err == nil {
		mdisk := db.Metric{Time: time.Now(), EnvironmentId: nmetrics.env.Id, Source: "Tezos", Metric: "disksize", Value: float64(storage)}
		nmetrics.save(&mdisk)
	}

	if !nmetrics.bhready {
		nmetrics.checkBHReady()
		if !nmetrics.bhready {
			return
		}
	}

	// bh := uint64(0)
	start := time.Now()
	client := http.Client{
		Timeout: 1 * time.Second,
	}
	resp, err := client.Get("http://localhost:" + strconv.Itoa(nmetrics.config.WebPort) + "/endpoints/" + nmetrics.env.Tezos.Endpoint + "/chains/main/blocks/head")
	if err == nil {
		end := time.Now()
		duration := end.Sub(start)
		bytes, _ := ioutil.ReadAll(resp.Body)
		resp.Body.Close()
		block := tezcon.Block{}
		err2 := json.Unmarshal(bytes, &block)
		if err2 == nil {
			mbh := db.Metric{Time: time.Now(), EnvironmentId: nmetrics.env.Id, Source: "Tezos", Metric: "blockheight", Value: float64(block.Header.Level)}
			mlat := db.Metric{Time: time.Now(), EnvironmentId: nmetrics.env.Id, Source: "Tezos", Metric: "latency", Value: float64(duration.Nanoseconds())}
			nmetrics.save(&mbh)
			nmetrics.save(&mlat)
		}
	}
}

func (nmetrics *NodeMetrics) DirSize(path string) (int64, error) {
	var size int64
	err := filepath.Walk(path, func(_ string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if !info.IsDir() {
			size += info.Size()
		}
		return err
	})
	return size, err
}

func (nmetrics *NodeMetrics) Init() {
	t := time.NewTicker(10 * time.Second)
	for {
		<-t.C
		if nmetrics.Running == false {
			break
		}
		go nmetrics.saveMetrics()
	}
}
