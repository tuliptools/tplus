package models

type PluginYMLRoot struct {
	Name        string
	Description string
	Services    map[string]PluginYML
}

type PluginYML struct {
	Image       string               `yaml:"image"`
	Env         []string             `yaml:"env"`
	ServiceName string               `yaml:"serviceName"`
	Description string               `yaml:"description"`
	Endpoints   []PluginYmlEndpoint  `yaml:"endpoints"`
	Entrypoint  []string             `yaml:"entrypoint"`
	Command     []string             `yaml:"command"`
	Volumes     []PluginYmlVolumes   `yaml:"volumes"`
	Templates   []PluginYmlTemplates `yaml:"templates"`
}

type PluginYmlEndpoint struct {
	Port     int    `yaml:"Port"`
	Proto    string `yaml:"Proto"`
	HomePath string `yaml:"HomePath"`
	Name     string `yaml:"Name"`
	Register string `yaml:"Register"`
	Type     string `yaml:"Type"`
}

type PluginYmlVolumes struct {
	Name        string `yaml:"name"`
	Description string `yaml:"description"`
	Mount       string `yaml:"mount"`
}

type PluginYmlTemplates struct {
	Name   string `yaml:"name"`
	Source string `yaml:"source"`
	Dest   string `yaml:"dest"`
}
