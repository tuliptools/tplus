package models

type File struct {
	Path  string
	IsDir bool
	Mode  string
}

type Runnable struct {
	Name        string         `yaml:"Name"`
	Description string         `yaml:"Description"`
	Image       string         `yaml:"Image"`
	Tasks       []RunnableTask `yaml:"Tasks"`
	Filename    string
	Error       string
}

type RunnableTask struct {
	Name       string   `yaml:"Name"`
	Command    []string `yaml:"Command,omitempty"`
	Image      string   `yaml:"Image,omitempty"`
	Timeout    string   `yaml:"Timeout,omitempty"`
	WaitBlocks int      `yaml:"WaitBlocks,omitempty"`
}

type TaskVariable struct {
	Name        string
	Description string
	Type        string
	Value       string
	Editable    bool
}

type ProjectService struct {
	Name string
	Identifier string
	Endpoints []ProjectServiceEndpoint
	Status string
}

type ProjectServiceEndpoint struct {
	Name string
	Type string
	Id string
	Port string
}