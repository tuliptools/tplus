package models

type WalletAddressesResponse struct {
	Error     string
	Success   bool
	Addresses []WalletAddress
}

type WalletAddress struct {
	Address  string
	Alias    string
	Balance  string
	Delegate string
	Counter  string
}

type AddressDetailsResponse struct {
	Hash      string
	Pubkey    string
	SecretKey string
}
