package service

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/api/types/mount"
	network2 "github.com/docker/docker/api/types/network"
	"github.com/docker/docker/api/types/strslice"
	"github.com/docker/docker/client"
	"github.com/docker/go-connections/nat"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/service/docker"
	"io"
	"io/ioutil"
	"net"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"time"
)

type DockerService struct {
	docker *client.Client
	db     *db.DB
}

func NewDockerService(db *db.DB) *DockerService {
	cli, err := client.NewEnvClient()
	if err != nil {
		panic(err)
	}

	ds := DockerService{}
	ds.docker = cli
	ds.db = db

	return &ds
}

func (ds *DockerService) PullImageBlocking(image string) ([]byte, error) {
	dbe, _ := ds.db.GetDockerImage(image)
	ctx := context.Background()
	var res []byte
	reader, err := ds.docker.ImagePull(ctx, image, types.ImagePullOptions{})
	if err != nil {
		return res, err
	}
	dbe.LastPull = time.Now()
	ds.db.Save(dbe)
	return ioutil.ReadAll(reader)
}

func (ds *DockerService) PullImage(image string) chan error {
	res := make(chan error)
	go func() {
		_, e := ds.PullImageBlocking(image)
		res <- e
	}()
	return res
}

func (ds *DockerService) FindContainerWithPrefix(prefix string) ([]string, error) {
	prefix = "/" + prefix
	ctx := context.Background()
	var res []string
	list, err := ds.docker.ContainerList(ctx, types.ContainerListOptions{
		All: true,
	})
	if err != nil {
		return res, err
	}
	l := len(prefix)
	for _, c := range list {
		for _, n := range c.Names {
			if len(n) >= l && n[:l] == prefix {
				res = append(res, c.ID)
			}
		}
	}
	return res, err
}

func (ds *DockerService) FindContainersForEnv(envid string) ([]string, error) {
	ctx := context.Background()
	var res []string
	list, err := ds.docker.ContainerList(ctx, types.ContainerListOptions{
		All: true,
	})
	if err != nil {
		return res, err
	}
	for _, c := range list {
		if val, ok := c.Labels["tplus_env"]; ok && val == envid {
			res = append(res, c.ID)
		}
	}
	return res, err
}

func (ds *DockerService) FindContainersForProject(project string) ([]string, error) {
	ctx := context.Background()
	var res []string
	list, err := ds.docker.ContainerList(ctx, types.ContainerListOptions{
		All: true,
	})
	if err != nil {
		return res, err
	}
	for _, c := range list {
		if val, ok := c.Labels["tplus_project_id"]; ok && val == project {
			res = append(res, c.ID)
		}
	}
	return res, err
}

func (ds *DockerService) Inspect(id string) (types.ContainerJSON, error) {
	ctx := context.Background()
	return ds.docker.ContainerInspect(ctx, id)
}

func (ds *DockerService) GetContainerStats(id string) *docker.TimedContainerStat {
	// need 2 samples to calc cpu usage
	res := docker.TimedContainerStat{}
	ds.getStats(id, &res.First)
	time.Sleep(1 * time.Second)
	ds.getStats(id, &res.Second)
	return &res
}

func (ds *DockerService) getStats(cid string, res interface{}) {
	reader, err := ds.docker.ContainerStats(context.Background(), cid, false)
	if err != nil {
		return
	}
	b, _ := ioutil.ReadAll(reader.Body)
	json.Unmarshal(b, res)
}

func (ds *DockerService) GetLogs(id string, follow bool, tail string) (io.ReadCloser, error) {
	res, e := ds.docker.ContainerLogs(context.Background(), id, types.ContainerLogsOptions{
		Follow:     follow,
		ShowStdout: true,
		ShowStderr: true,
		Tail:       tail,
	})

	return res, e
}

func (ds *DockerService) GetLogsStdin(id string, follow bool) io.ReadCloser {
	res, _ := ds.docker.ContainerLogs(context.Background(), id, types.ContainerLogsOptions{
		Follow:     follow,
		ShowStdout: true,
		ShowStderr: false,
	})

	return res
}

func (ds *DockerService) GetLogsStderr(id string, follow bool) io.ReadCloser {
	res, _ := ds.docker.ContainerLogs(context.Background(), id, types.ContainerLogsOptions{
		Follow:     follow,
		ShowStdout: false,
		ShowStderr: true,
	})

	return res
}

func (ds *DockerService) ExecInContainerById(id string, command []string, tty bool) ([]byte, error) {
	var res []byte
	ctx := context.Background()
	ex, e := ds.docker.ContainerExecCreate(ctx, id, types.ExecConfig{
		Cmd:          command,
		AttachStdin:  true,
		AttachStderr: true,
		AttachStdout: true,
		Tty:          tty,
	})
	hr, e := ds.docker.ContainerExecAttach(ctx, ex.ID, types.ExecStartCheck{
		Tty: true,
	})
	defer hr.Close()
	if tty {
		go func() {
			cmd := ""
			for _, s := range command {
				cmd += s + " "
			}
			io.Copy(hr.Conn, os.Stdin)
		}()
	}
	ds.docker.ContainerExecStart(ctx, ex.ID, types.ExecStartCheck{})
	if e != nil {
		return res, e
	}
	if tty {
		io.Copy(os.Stdout, hr.Reader)
	} else {
		res, _ = ioutil.ReadAll(hr.Reader)
	}
	return res, nil
}

func (ds *DockerService) ExecInContainer(containername string, command []string, tty bool) ([]byte, error) {
	var res []byte
	ctx := context.Background()
	id, _ := ds.GetContainerId(containername)
	ex, e := ds.docker.ContainerExecCreate(ctx, id, types.ExecConfig{
		Cmd:          command,
		AttachStdin:  true,
		AttachStderr: true,
		AttachStdout: true,
		Tty:          tty,
	})
	hr, e := ds.docker.ContainerExecAttach(ctx, ex.ID, types.ExecStartCheck{
		Tty: true,
	})
	if tty {
		go func() {
			cmd := ""
			for _, s := range command {
				cmd += s + " "
			}
			io.Copy(hr.Conn, os.Stdin)
		}()
	}
	ds.docker.ContainerExecStart(ctx, ex.ID, types.ExecStartCheck{})
	if e != nil {
		return res, e
	}
	if tty {
		io.Copy(os.Stdout, hr.Reader)
	} else {
		res, _ = ioutil.ReadAll(hr.Reader)
	}
	return res, nil
}

func (ds *DockerService) ExecRW(containername string, command []string) (io.Reader, io.Writer, error) {
	id, _ := ds.GetContainerId(containername)
	return ds.ExecRWID(id, command)
}

func (ds *DockerService) ExecRWID(ContainerID string, command []string) (io.Reader, net.Conn, error) {
	ctx := context.Background()
	ex, e := ds.docker.ContainerExecCreate(ctx, ContainerID, types.ExecConfig{
		Cmd:          command,
		AttachStdin:  true,
		AttachStderr: true,
		AttachStdout: true,
		Tty:          true,
		Env: []string{
			"COLUMNS=80",
			"LINES=23",
		},
	})
	if e != nil {
		return nil, nil, e
	}
	hr, e := ds.docker.ContainerExecAttach(ctx, ex.ID, types.ExecStartCheck{
		Tty: true,
	})
	if e != nil {
		return nil, nil, e
	}
	go ds.docker.ContainerExecStart(ctx, ex.ID, types.ExecStartCheck{})
	return hr.Reader, hr.Conn, nil
}

func (ds *DockerService) GetContainerId(name string) (string, error) {
	ctx := context.Background()
	resp, err := ds.docker.ContainerList(ctx, types.ContainerListOptions{
		All: true,
	})
	if err != nil {
		return "", err
	}
	for _, c := range resp {
		for _, n := range c.Names {
			if n == "/"+name {
				return c.ID, nil
			}
		}
	}
	return "", errors.New("No such Container: " + name)
}

func (ds *DockerService) CheckNetworkExists(name string) (bool, error) {
	res := false
	nets, e := ds.docker.NetworkList(context.Background(), types.NetworkListOptions{})
	for _, n := range nets {
		if strings.ToLower(n.Name) == strings.ToLower(name) || strings.ToLower(n.ID) == strings.ToLower(name) {
			res = true
			break
		}
	}
	return res, e
}

func (ds *DockerService) CreateNetwork(name string) (string, error) {
	ctx := context.Background()
	resp, err := ds.docker.NetworkCreate(ctx, name, types.NetworkCreate{
		CheckDuplicate: true,
		Labels: map[string]string{
			"created_by": "tplus",
		},
	})
	ctx.Done()
	return resp.ID, err
}

func (ds *DockerService) RemoveNetwork(id string) error {
	ctx := context.Background()
	err := ds.docker.NetworkRemove(ctx, id)
	ctx.Done()
	return err
}

func (ds *DockerService) RunTask(name, image, network string, entry, cmd strslice.StrSlice, mounts []mount.Mount) ([]byte, error) {
	ctx := context.Background()
	cconfig := container.Config{
		Hostname:     "tplusTask",
		AttachStdin:  false,
		AttachStdout: false,
		AttachStderr: false,
		Cmd:          cmd,
		Image:        image,
		Entrypoint:   entry,
		Labels:       map[string]string{"created_by": "Tplus", "tmp": "true"},
	}
	chostc := container.HostConfig{
		DNS:    []string{"1.1.1.1", "8.8.8.8", "8.8.4.4"},
		Mounts: mounts,
	}
	cnetc := network2.NetworkingConfig{
		EndpointsConfig: map[string]*network2.EndpointSettings{
			"default": {
				NetworkID: network,
			},
		},
	}
	resp, err := ds.docker.ContainerCreate(ctx, &cconfig, &chostc, &cnetc, name)
	ds.ContainerStart(resp.ID)
	if err != nil {
		return []byte{}, err
	}
	ctx.Done()
	ctx = context.Background()
	time.Sleep(200 * time.Millisecond)
	logs, err := ds.docker.ContainerLogs(ctx, resp.ID, types.ContainerLogsOptions{
		ShowStdout: true,
		ShowStderr: true,
		Follow:     true,
	})
	if err != nil {
		fmt.Print("ERROR: ", err)
		return []byte{}, nil
	}
	res, _ := ioutil.ReadAll(logs)
	_ = ds.docker.ContainerRemove(context.Background(), resp.ID, types.ContainerRemoveOptions{})
	return res, nil
}

func (ds *DockerService) Build(path, name, file string) error {
	dockerBinary, _ := exec.LookPath("docker")
	command := strings.Split("build -t "+name+" -f "+path+"/"+file+" "+path, " ")
	cmd := exec.Command(dockerBinary, command...)
	return cmd.Run()
}

func (ds *DockerService) FixPermissions(path string) error {
	dockerBinary, _ := exec.LookPath("docker")
	command := strings.Split("docker run --rm -v "+path+":/m alpine:latest chmod -cR 777 /m/*", " ")
	cmd := exec.Command(dockerBinary, command...)
	return cmd.Run()
}

func (ds *DockerService) RunTezosTask(name, image, network string, entry, cmd strslice.StrSlice, mounts []mount.Mount) ([]byte, error) {
	ctx := context.Background()
	cconfig := container.Config{
		Hostname:     "tplusContainer",
		AttachStdin:  false,
		AttachStdout: false,
		AttachStderr: false,
		Cmd:          cmd,
		Image:        image,
		Entrypoint:   entry,
		Labels:       map[string]string{"created_by": "Tplus", "tmp": "true"},
	}
	chostc := container.HostConfig{
		DNS:    []string{"1.1.1.1", "8.8.8.8", "8.8.4.4"},
		Mounts: mounts,
	}
	cnetc := network2.NetworkingConfig{
		EndpointsConfig: map[string]*network2.EndpointSettings{
			"default": {
				NetworkID: network,
			},
		},
	}
	resp, err := ds.docker.ContainerCreate(ctx, &cconfig, &chostc, &cnetc, name)
	ds.ContainerStart(resp.ID)
	if err != nil {
		return []byte{}, err
	}
	ctx.Done()
	ctx = context.Background()
	time.Sleep(800 * time.Millisecond)
	logs, err := ds.docker.ContainerLogs(ctx, resp.ID, types.ContainerLogsOptions{
		ShowStdout: true,
		ShowStderr: true,
		Follow:     true,
	})
	if err != nil {
		fmt.Print("ERROR: ", err)
		return []byte{}, nil
	}
	res, _ := ioutil.ReadAll(logs)
	_ = ds.docker.ContainerRemove(context.Background(), resp.ID, types.ContainerRemoveOptions{})
	return res, nil
}

func (ds *DockerService) StopContainer(id string) {
	d := 3 * time.Second
	ds.docker.ContainerStop(context.Background(), id, &d)
}

func (ds *DockerService) DeleteContainer(id string) {
	ds.docker.ContainerRemove(context.Background(), id, types.ContainerRemoveOptions{Force: true})
}

func (ds *DockerService) DeleteLike(str string) {
	list, _ := ds.docker.ContainerList(context.Background(), types.ContainerListOptions{All: true})
	for _, c := range list {
		for _, n := range c.Names {
			if strings.Contains(n, str) || n == str {
				n = n[1:]
				id, err := ds.GetContainerId(n)
				if err == nil {
					ds.DeleteContainer(id)
				} else {
					fmt.Println(err)
				}
			}
		}
	}
}

func (ds *DockerService) DeleteNetwork(str string) {
	nets, _ := ds.docker.NetworkList(context.Background(), types.NetworkListOptions{})
	for _, n := range nets {
		if n.Name == str || n.ID == str {
			ds.docker.NetworkRemove(context.Background(), n.ID)
		}
	}
}

func (ds *DockerService) GetContainerIP(name string) (string, error) {
	id, err := ds.GetContainerId(name)
	if err != nil {
		return "", err
	}
	info, err := ds.Inspect(id)
	for _, e := range info.NetworkSettings.Networks {
		return e.IPAddress, nil
	}
	return "", errors.New("no networks")
}

func (ds *DockerService) ContainerStart(id string) error {
	return ds.docker.ContainerStart(context.Background(), id, types.ContainerStartOptions{})
}

func (ds *DockerService) ContainerStop(id string) error {
	duration := 5 * time.Second
	return ds.docker.ContainerStop(context.Background(), id, &duration)
}

func (ds *DockerService) ContainerRM(id string) error {
	return ds.docker.ContainerRemove(context.Background(), id, types.ContainerRemoveOptions{
		RemoveLinks:   false,
		RemoveVolumes: false,
		Force:         true,
	})
}

func (ds *DockerService) NetworkRM(id string) error {
	return ds.docker.NetworkRemove(context.Background(), id)
}

func (ds *DockerService) EnsureNetworkExists(name string) (string, error) {
	ctx := context.Background()
	resp, err := ds.docker.NetworkCreate(ctx, "tplus_"+name, types.NetworkCreate{
		CheckDuplicate: true,
		Labels: map[string]string{
			"created_by": "tplus",
		},
	})
	if err != nil {
		if strings.Contains(err.Error(), "already exists") {
			nets, _ := ds.docker.NetworkList(context.Background(), types.NetworkListOptions{})
			for _, n := range nets {
				if n.Name == strings.ToLower(name) {
					return n.ID, nil
				}
			}
		}
		return "", err
	}
	ctx.Done()
	return resp.ID, nil
}

func (ds *DockerService) ContainerCreate(template docker.Template) (string, error) {
	ctx := context.Background()
	var envs []string
	for k, v := range template.EnvVariables {
		envs = append(envs, k+"="+v)
	}
	var mounts []mount.Mount
	for k, v := range template.Mounts {
		mounts = append(mounts,
			mount.Mount{
				Type:   mount.TypeBind,
				Source: k,
				Target: v,
			})
	}
	// (ctx context.Context, config *container.Config, hostConfig *container.HostConfig, networkingConfig *network.NetworkingConfig, containerName string)
	portset := nat.PortSet{}
	for _, c := range template.TcpPorts {
		portset[nat.Port(strconv.Itoa(c)+"/tcp")] = struct{}{}
	}

	portBindings := nat.PortMap{}
	for h, c := range template.TcpPorts {
		port := nat.Port(strconv.Itoa(c) + "/tcp")
		portBindings[port] = []nat.PortBinding{
			{
				HostIP:   "127.0.0.1", // todo possibly change for Tplus web
				HostPort: strconv.Itoa(h),
			},
		}
	}

	cconfig := container.Config{
		Hostname:     "container",
		AttachStdin:  false,
		AttachStdout: false,
		AttachStderr: false,
		Cmd:          template.Command,
		Image:        template.Image,
		Entrypoint:   template.Entrypoint,
		Labels:       template.Labels,
		Env:          envs,
		ExposedPorts: portset,
	}
	chostc := container.HostConfig{
		DNS:          []string{"8.8.8.8", "8.8.4.4"},
		Mounts:       mounts,
		PortBindings: portBindings,
	}
	chostc.Resources.NanoCPUs = int64(template.Resources.CPUCount)
	chostc.Resources.Memory = int64(template.Resources.MemoryMax)
	resp, err := ds.docker.ContainerCreate(ctx, &cconfig, &chostc, nil, template.ContainerName)
	if err != nil {
		return "", err

	}
	if template.EnvironmentId != "" {
		env, err := ds.db.GetEnvironmentById(template.EnvironmentId)
		if err != nil {
			return "", err
		}
		ds.docker.NetworkConnect(context.Background(), env.Tezos.Container.NetworkID, resp.ID, &network2.EndpointSettings{
			Aliases: template.NetworkAliases,
		})
	}
	return resp.ID, nil
}

func (ds *DockerService) TaskContainerCreate(template docker.Template) (string, error) {
	ctx := context.Background()
	var envs []string
	for k, v := range template.EnvVariables {
		envs = append(envs, k+"="+v)
	}
	var mounts []mount.Mount
	for k, v := range template.Mounts {
		mounts = append(mounts,
			mount.Mount{
				Type:   mount.TypeBind,
				Source: k,
				Target: v,
			})
	}
	// (ctx context.Context, config *container.Config, hostConfig *container.HostConfig, networkingConfig *network.NetworkingConfig, containerName string)
	cconfig := container.Config{
		Hostname:     "container",
		AttachStdin:  false,
		AttachStdout: false,
		AttachStderr: false,
		Cmd:          template.Command,
		Image:        template.Image,
		Entrypoint:   template.Entrypoint,
		Labels:       template.Labels,
		Env:          envs,
		WorkingDir:   "/tplus",
	}
	chostc := container.HostConfig{
		DNS:    []string{"8.8.8.8", "8.8.4.4"},
		Mounts: mounts,
	}
	chostc.Resources.NanoCPUs = int64(template.Resources.CPUCount)
	chostc.Resources.Memory = int64(template.Resources.MemoryMax)
	resp, err := ds.docker.ContainerCreate(ctx, &cconfig, &chostc, nil, template.ContainerName)
	if err != nil {
		return "", err

	}
	// disconnect from all nets
	info, _ := ds.GetContainerInfo(resp.ID)
	list, _ := ds.docker.NetworkList(context.Background(), types.NetworkListOptions{})
	for n := range info.NetworkSettings.Networks {
		for _, net := range list {
			if net.Name == n {
				ds.docker.NetworkDisconnect(context.Background(), net.ID, resp.ID, true)
			}
		}

	}
	if template.EnvironmentId != "" {
		env, err := ds.db.GetEnvironmentById(template.EnvironmentId)
		if err != nil {
			return "", err
		}
		ds.docker.NetworkConnect(context.Background(), env.Tezos.Container.NetworkID, resp.ID, &network2.EndpointSettings{
			Aliases: template.NetworkAliases,
		})
	}
	return resp.ID, nil
}

func (ds *DockerService) ContainerCreateDEPRECETAED(name, image, network string, entry, cmd strslice.StrSlice, mounts []mount.Mount, resources db.Resources, env []string) (string, error) {
	ctx := context.Background()
	// (ctx context.Context, config *container.Config, hostConfig *container.HostConfig, networkingConfig *network.NetworkingConfig, containerName string)
	cconfig := container.Config{
		Hostname:     "container",
		AttachStdin:  false,
		AttachStdout: false,
		AttachStderr: false,
		Cmd:          cmd,
		Image:        image,
		Entrypoint:   entry,
		Labels:       map[string]string{"created_by": "tplus"},
		Env:          env,
	}
	chostc := container.HostConfig{
		DNS:    []string{"8.8.8.8", "8.8.4.4"},
		Mounts: mounts,
	}
	chostc.Resources.NanoCPUs = int64(resources.CPUCount * 1000000)
	chostc.Resources.Memory = int64(resources.MemoryMax * 1000000)
	resp, err := ds.docker.ContainerCreate(ctx, &cconfig, &chostc, nil, name)
	if err != nil {
		return "", err
	}
	// disconnect from all nets
	info, _ := ds.GetContainerInfo(resp.ID)
	list, _ := ds.docker.NetworkList(context.Background(), types.NetworkListOptions{})
	for n := range info.NetworkSettings.Networks {
		for _, net := range list {
			if net.Name == n {
				ds.docker.NetworkDisconnect(context.Background(), net.ID, resp.ID, true)
			}
		}

	}
	ds.docker.NetworkConnect(context.Background(), network, resp.ID, nil)
	return resp.ID, nil
}

func (ds *DockerService) DEFAULTCONTAINERDEPRECATED(name, image, network string, mounts []mount.Mount, resources db.Resources, env []string) (string, error) {
	ctx := context.Background()
	// (ctx context.Context, config *container.Config, hostConfig *container.HostConfig, networkingConfig *network.NetworkingConfig, containerName string)
	cconfig := container.Config{
		Hostname:     "node",
		AttachStdin:  false,
		AttachStdout: false,
		AttachStderr: false,
		Image:        image,
		Labels:       map[string]string{"created_by": "tplus"},
		Env:          env,
	}
	chostc := container.HostConfig{
		DNS:    []string{"8.8.8.8", "8.8.4.4"},
		Mounts: mounts,
	}
	chostc.Resources.NanoCPUs = int64(resources.CPUCount * 1000000)
	chostc.Resources.Memory = int64(resources.MemoryMax * 1000000)
	resp, err := ds.docker.ContainerCreate(ctx, &cconfig, &chostc, nil, name)
	if err != nil {
		return "", err
	}
	// disconnect from all nets
	info, _ := ds.GetContainerInfo(resp.ID)
	list, _ := ds.docker.NetworkList(context.Background(), types.NetworkListOptions{})
	for n := range info.NetworkSettings.Networks {
		for _, net := range list {
			if net.Name == n {
				ds.docker.NetworkDisconnect(context.Background(), net.ID, resp.ID, true)
			}
		}

	}
	ds.docker.NetworkConnect(context.Background(), network, resp.ID, nil)
	return resp.ID, nil
}

func (ds *DockerService) SimpleCreate(name, image string) (string, error) {
	ctx := context.Background()
	// (ctx context.Context, config *container.Config, hostConfig *container.HostConfig, networkingConfig *network.NetworkingConfig, containerName string)
	cconfig := container.Config{
		Hostname:     "node",
		AttachStdin:  false,
		AttachStdout: false,
		AttachStderr: false,
		Image:        image,
		Labels:       map[string]string{"created_by": "tplus"},
	}
	chostc := container.HostConfig{
		DNS: []string{"8.8.8.8", "8.8.4.4"},
	}
	resp, err := ds.docker.ContainerCreate(ctx, &cconfig, &chostc, nil, name)
	if err != nil {
		return "", err
	}
	return resp.ID, nil
}

func (ds *DockerService) GetContainerInfo(id string) (types.ContainerJSON, error) {
	return ds.docker.ContainerInspect(context.Background(), id)
}
