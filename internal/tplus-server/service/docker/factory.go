package docker

import (
	"github.com/docker/docker/api/types/strslice"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/version"
	"strings"
	"time"
)

//mounts []mount.Mount, resources db.Resources, env []string

type Template struct {
	Image          string
	ContainerName  string
	Hostname       string
	EnvironmentId  string
	NetworkAliases []string
	Entrypoint     []string
	Command        []string
	Mounts         map[string]string
	Resources      db.Resources
	EnvVariables   map[string]string
	Labels         map[string]string
	DNS            []string
	TcpPorts       map[int]int //[Host]=Container
}

func GetDefaultTemplate(envid string) Template {
	new := Template{}
	new.Resources = db.Resources{
		CPUCount:  1000,
		MemoryMax: 2000000000,
	}
	new.EnvironmentId = envid
	new.Hostname = "container"
	new.Labels = map[string]string{
		"created_by":    "tplus",
		"creation_date": time.Now().String(),
		"tplus_env":     envid,
	}
	new.DNS = []string{"8.8.8.8", "8.8.4.4"}

	return new
}

func GetTezosTemplate(env *db.Environment) Template {
	image := version.GetVersionInfo().TezosImage
	t := GetDefaultTemplate(env.Id)
	t.NetworkAliases = []string{"tezos", "node", "tezosnode"}
	t.Image = image
	if env.Tezos.Container.Resources.MemoryMax >= 10 && env.Tezos.Container.Resources.CPUCount >= 10 {
		t.Resources = env.Tezos.Container.Resources
	}

	t.EnvVariables = map[string]string{
		"DATA_DIR":                               "/tezosdata",
		"TEZOS_CLIENT_UNSAFE_DISABLE_DISCLAIMER": "Y",
	}

	t.Mounts = map[string]string{
		env.Tezos.DataDir:   "/tezosdata/",
		env.Tezos.ConfigDir: "/tezosconf/",
	}

	if env.Tezos.Branch != "sandbox" {
		t.Command = strslice.StrSlice{"/usr/local/bin/tezos-node", "run", "--data-dir", "/tezosdata", "--rpc-addr", ":8732", "--connections", "25"}
	} else {
		t.Command = strslice.StrSlice{"/usr/local/bin/tezos-node", "run", "--data-dir", "/tezosdata",
			"--rpc-addr", ":8732", "--connections", "25",
			"--sandbox", "/tezosconf/sandbox.json",
			"--private-mode", "--bootstrap-threshold", "0",
		}
	}
	t.Entrypoint = strslice.StrSlice{""}

	t.ContainerName = env.Tezos.Container.Name
	return t
}

func GetProxyTemplate(env *db.Environment) Template {
	image := version.GetVersionInfo().NodeProxyImage
	t := GetDefaultTemplate(env.Id)
	t.NetworkAliases = []string{"tezos", "node", "tezosnode"}
	t.Image = image
	if env.Tezos.Container.Resources.MemoryMax >= 10 && env.Tezos.Container.Resources.CPUCount >= 10 {
		t.Resources = env.Tezos.Container.Resources
	}

	t.Mounts = map[string]string{
		env.Tezos.DataDir: "/etc/tezproxy/",
	}

	t.Command = strslice.StrSlice{"./main"}
	t.Entrypoint = strslice.StrSlice{""}

	t.ContainerName = env.Tezos.Container.Name
	return t
}

func GetPluginService(env *db.Environment, p *db.PluginService) Template {
	image := ""
	if strings.Contains(p.Image, "registry.gitlab.com") || strings.Contains(p.Image, "docker.io") {
		image = p.Image
	} else {
		image = "docker.io/" + p.Image
	}
	t := GetDefaultTemplate(env.Id)
	t.NetworkAliases = []string{p.Name}
	t.EnvVariables = map[string]string{}
	t.Labels["plugin_service"] = p.Name
	t.Labels["plugin_serviceName"] = p.ServiceName
	t.Labels["plugin_desc"] = p.Description

	t.Mounts = map[string]string{}

	if len(p.Command) >= 1 {
		t.Command = p.Command
	}

	if len(p.Entrypoint) >= 1 {
		t.Entrypoint = p.Entrypoint
	}

	for _, v := range p.Volumes {
		t.Mounts[v.HostDir] = v.Mount
	}

	for _, template := range p.Templates {
		t.Mounts[template.HostFile] = template.Dest
	}

	for _, e := range p.Env {
		ee := strings.Split(e, "=")
		for i := range ee {
			if i >= 2 {
				ee[1] = ee[1] + "=" + ee[i]
			}
		}
		t.EnvVariables[ee[0]] = ee[1]
	}
	t.Image = image
	return t
}

func GetClientTemplate(env *db.Environment) Template {
	base := GetTezosTemplate(env)
	base.ContainerName = env.Tezos.Container.Name + "_client"
	base.NetworkAliases = []string{"client", "tezosclient", "clientcontainer"}
	base.Command = strslice.StrSlice{"sleep", "infinity"}
	base.Mounts = map[string]string{
		env.Tezos.ClientDataDir: "/root/.tezos-client",
		env.Tezos.ConfigDir:     "/tezosconf",
	}
	base.EnvVariables = map[string]string{
		"TEZOS_CLIENT_UNSAFE_DISABLE_DISCLAIMER": "Y",
	}
	return base
}

func GetProjectTaskTemplate(env *db.Environment, project *db.Project, projectdir string, vars map[string]string) Template {
	base := GetTezosTemplate(env)
	base.ContainerName = strings.Replace(db.GetRandomId("projectask"), "-", "_", -1)
	base.NetworkAliases = []string{}
	base.Command = strslice.StrSlice{"cd", "/app"}
	base.Labels["tplus_project_id"] = project.Id
	base.Labels["tplus_project_name"] = project.Name
	base.Labels["tplus_project_alias"] = project.Alias
	base.Mounts = map[string]string{
		env.Tezos.ClientDataDir: "/tezos-client",
		env.Tezos.ClientDataDir: "/root/.tezos-client",
		env.Tezos.ConfigDir:     "/tezosconf",
		projectdir:              "/tplus",
	}
	base.EnvVariables = map[string]string{
		"TEZOS_CLIENT_UNSAFE_DISABLE_DISCLAIMER": "Y",
	}
	for k, v := range vars {
		k = "TPLUS_" + strings.Replace(strings.ToUpper(k), ".", "_", -1)
		k = strings.Replace(k, "-", "_", -1)
		base.EnvVariables[k] = v
	}
	return base
}

func GetSnapshotImportTemplate(env *db.Environment, importFile string) Template {
	image := version.GetVersionInfo().TezosImage
	t := GetDefaultTemplate(env.Id)
	t.NetworkAliases = []string{"snapshotimporter"}
	t.Image = image
	if env.Tezos.Container.Resources.MemoryMax >= 10 && env.Tezos.Container.Resources.CPUCount >= 10 {
		t.Resources = env.Tezos.Container.Resources
	}

	t.EnvVariables = map[string]string{
		"TEZOS_CLIENT_UNSAFE_DISABLE_DISCLAIMER": "Y",
	}

	t.Mounts = map[string]string{
		env.Tezos.DataDir:     "/root/.tezos-node/",
		env.Tezos.SnapshotDir: "/snapshots/",
	}

	t.Command = strslice.StrSlice{}
	if env.Tezos.Branch != "mainnet" {
		t.Command = append(t.Command, "/usr/local/bin/tezos-node", "snapshot", "import", "/snapshots/"+importFile)
		t.Command = append(t.Command, "--network", env.Tezos.Branch)
	} else {
		t.Command = append(t.Command, "/usr/local/bin/tezos-node", "snapshot", "import", "/snapshots/"+importFile)
	}
	t.Entrypoint = strslice.StrSlice{""}

	t.ContainerName = db.GetRandomId("importer")
	return t
}

func GetCLITemplate(env *db.Environment, projectdir string, name string) Template {
	t := GetDefaultTemplate(env.Id)
	t.NetworkAliases = []string{}
	t.Image = "docker.io/tuliptools/tplus-code:latest"
	if env.Tezos.Container.Resources.MemoryMax >= 10 && env.Tezos.Container.Resources.CPUCount >= 10 {
		t.Resources = db.Resources{
			CPUCount:  2000,
			MemoryMax: 4000000000,
		}
	}

	t.EnvVariables = map[string]string{
		"DATA_DIR":                               "/tezosdata",
		"TEZOS_CLIENT_UNSAFE_DISABLE_DISCLAIMER": "Y",
	}

	t.Mounts = map[string]string{
		env.Tezos.DataDir: "/tezosdata/",
		projectdir:        "/home/coder/project",
	}

	t.Command = strslice.StrSlice{"haproxy", "-f", "/etc/haproxy/haproxy.cfg"}

	t.ContainerName = name

	return t
}
