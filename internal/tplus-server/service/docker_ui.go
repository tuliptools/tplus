package service

import (
	"fmt"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/service/docker"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/version"
	"time"
)

/* returns UI IP */
func (ds *DockerService) RunUI() (string, error) {
	cname := "tplus_ui"
	cimage := version.GetVersionInfo().WebImage
	humanName := "TplusWeb"
	digest := version.GetVersionInfo().WebImageDigest
	return ds.startSimple(cname, cimage, humanName, digest)
}

func (ds *DockerService) startSimple(cname string, cimage string, humanName string, digest string) (string, error) {
	id, err := ds.GetContainerId(cname)
	if err != nil {
		ds.PullImageBlocking(cimage)
		id, err = ds.SimpleCreate(cname, cimage)
		if err != nil {
			return "", err
		}
	}
	if err == nil && id != "" {
		info, err := ds.GetContainerInfo(id)
		if err == nil && info.Image != digest {
			fmt.Println("Updating " + humanName + " Image to latest version ")
			ds.StopContainer(id)
			ds.DeleteContainer(id)
			time.Sleep(500 * time.Millisecond)
			ds.PullImageBlocking(cimage)
			id, err = ds.SimpleCreate(cname, cimage)
			if err != nil {
				return "", err
			}
		}
	}
	ds.ContainerStart(id)
	time.Sleep(100 * time.Millisecond)
	return ds.GetContainerIP(cname)
}

/*
 On mac docker runs inside a vm, so we need a proxy server
 to access Containers via their internal IP
*/
func (ds *DockerService) RunMacOsXProxy() error {
	cname := "tplus_mac_proxy"
	cimage := version.GetVersionInfo().ProxyImage
	id, err := ds.GetContainerId(cname)
	if err != nil {
		ds.PullImageBlocking(cimage)
		tmplate := docker.GetDefaultTemplate("global")
		tmplate.ContainerName = cname
		tmplate.Image = cimage
		tmplate.TcpPorts = map[int]int{
			60501: 60501,
		}
		id, err = ds.ContainerCreate(tmplate)
		if err != nil {
			return err
		}
	}
	return ds.ContainerStart(id)
}
