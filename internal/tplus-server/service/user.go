package service

import (
	"errors"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/config"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"time"
)

type UserService struct {
	db     *db.DB
	config *config.Config
}

func NewUserService(db *db.DB, conf *config.Config) *UserService {
	u := UserService{}
	u.db = db
	u.config = conf
	return &u
}

func (users *UserService) GetDefaultUser() *db.User {
	if v, e := users.db.GetUserById("default"); v != nil && e == nil {
		return v
	}
	defU := db.User{}
	defU.Id = "default"
	defU.SetPassword("")
	defU.Admin = true
	defU.Created = time.Now()
	users.db.Save(&defU)
	return &defU
}

func (users *UserService) Register(username, password string) error {
	u := db.User{}
	u.Username = username
	u.SetPassword(password)
	u.Id = db.GetRandomId("user")
	return users.db.Save(&u)
}

func (users *UserService) GetToken(username, password string) (*db.Token, error) {
	var res *db.Token
	u, e := users.db.GetUserByUsername(username)
	if e != nil {
		return res, e
	}
	if !u.PasswordIsCorrect(password) {
		return res, errors.New("password incorrect")
	}
	token := db.Token{}
	token.UserId = u.Id
	token.Id = db.GetTokenString()
	e = users.db.Create(&token)
	return &token, e
}

func (users *UserService) GetTokenForUser(u *db.User) (*db.Token, error) {
	token := db.Token{}
	token.UserId = u.Id
	token.Id = db.GetTokenString()
	e := users.db.Create(&token)
	return &token, e
}

func (users *UserService) TokenToUser(token string) (*db.User, error) {
	tok, e := users.db.GetTokenById(token)
	if e != nil {
		return nil, e
	}
	if tok.Id == "" || tok.UserId == "" {
		return nil, errors.New("user not found")
	}
	return users.db.GetUserById(tok.UserId)
}

func (users *UserService) IsAuthed() {

}
