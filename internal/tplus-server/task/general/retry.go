package general

import (
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"sync"
	"time"
)

type WaitTask struct {
	task.Base
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.Result
	Status      string
	Id          string

	duration    time.Duration
	Name        string
	Description string
}

func (task *WaitTask) GetLogVars() map[string]string{
	m := map[string]string{}
	return m
}

func (task *WaitTask) GetResult() *task.Result {
	task.wg.Wait()
	return &task.result
}

func (task *WaitTask) GetProgress() string {
	return task.Status
}

func (task *WaitTask) GetEventPrefix() string {
	return "wait"
}

func (task *WaitTask) GetName() string {
	return task.Name
}

func (task *WaitTask) GetEnvironment() *db.Environment {
	return task.Environment
}

func (task *WaitTask) GetDescription() string {
	return task.Description
}

func (task *WaitTask) GetId() string {
	return task.Id
}

func (task *WaitTask) Run() {
	task.result.Success = true
	task.result.Message = "waited"
	task.wg.Done()
}
