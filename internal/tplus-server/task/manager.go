package task

import (
	"github.com/gobuffalo/packr"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/config"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/events"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/service"
	"sync"
	"time"
)

type Manager struct {
	conf               *config.Config
	db                 *db.DB
	docker             *service.DockerService
	registeredTasks    map[string]Task
	events            *events.Events
	box                *packr.Box
	registeredType     map[string]string
	registeredTypeLock *sync.Mutex
}

func (tm *Manager) setRegisteredType(k,v string) {
	tm.registeredTypeLock.Lock()
	defer tm.registeredTypeLock.Unlock()
	tm.registeredType[k] = v
}

func (tm *Manager) getRegisteredType(k string) (string,bool) {
	tm.registeredTypeLock.Lock()
	defer tm.registeredTypeLock.Unlock()
	val,ok :=  tm.registeredType[k]
	return val,ok
}

func (tm *Manager) getRegisteredTask(k string) (Task, bool) {
	tm.registeredTypeLock.Lock()
	defer tm.registeredTypeLock.Unlock()
	val,ok := tm.registeredTasks[k]
	return val,ok
}


func (tm *Manager) setRegisteredTasks(k string, task Task) {
	tm.registeredTypeLock.Lock()
	defer tm.registeredTypeLock.Unlock()
	tm.registeredTasks[k] = task
}

func NewTaskManager(conf *config.Config, db *db.DB, docker *service.DockerService, events *events.Events, box *packr.Box) *Manager {
	t := Manager{}
	t.conf = conf
	t.db = db
	t.docker = docker
	t.events = events.Source("TaskManager")
	t.registeredTasks = map[string]Task{}
	t.registeredType = map[string]string{}
	t.registeredTypeLock = &sync.Mutex{}
	t.box = box
	return &t
}

func (tm *Manager) RegisterInternal(t Task) Task {
	t.SetDocker(tm.docker)
	t.SetDocker(tm.docker)
	t.SetConfig(tm.conf)
	t.SetBox(tm.box)
	tm.setRegisteredType(t.GetId(),"internal")
	tm.setRegisteredTasks(t.GetId(),t)
	return t
}

func (tm *Manager) RegisterUserTask(t Task) Task {
	t.SetDocker(tm.docker)
	t.SetConfig(tm.conf)
	t.SetBox(tm.box)
	tm.setRegisteredType(t.GetId(),"user")
	tm.setRegisteredTasks(t.GetId(),t)
	return t
}

func (tm *Manager) getResult(t Task) (*Result, db.Task) {
	dbE := db.Task{}
	dbE.Id = t.GetId()
	dbE.Name = t.GetName()
	dbE.Description = t.GetDescription()
	dbE.State = "pending"
	dbE.Done = false
	dbE.OnSuccess = t.GetOnSuccess()
	dbE.OnFailure = t.GetOnFail()
	dbE.Type,_ = tm.getRegisteredType(t.GetId())
	dbE.ObjectsUpdated = [][]string{}
	if t.GetEnvironment() != nil {
		dbE.EnvironmentId = t.GetEnvironment().Id
	}
	_ = tm.db.Create(&dbE)

	go t.Run()

	dbE.State = "running"
	_ = tm.db.Save(&dbE)

	ticker := time.NewTicker(1500 * time.Second)

	donechan := make(chan *Result)

	var OldProgress string

	go func() {
		donechan <- t.GetResult()

	}()

	var result *Result
outer:
	for {
		select {
		case model := <-t.GetSaveChan():
			_ = tm.db.Save(model)
			dbE.ObjectsUpdated = append(dbE.ObjectsUpdated, []string{model.GetId(), string(model.Marshal())})

		case <-ticker.C:
			if t.GetProgress() != OldProgress {
				dbE.State = t.GetProgress()
				_ = tm.db.Save(&dbE)
			}
		case res := <-donechan:
			ticker.Stop()
			dbE.Done = true
			dbE.State = t.GetProgress()
			dbE.Logs = res.Logs
			_ = tm.db.Save(&dbE)
			result = res
			break outer
		}
	}

	if result.Success {
		dbE.Progress = "done"
		dbE.State = "successfull"
	} else {
		dbE.Progress = "done"
		dbE.State = "failure"
	}
	tm.db.Save(&dbE)

	return result, dbE

}

func (tm *Manager) RunAndBlock(task Task) *Result {
	return tm.RunAndBlockWithEvents(task, tm.events)
}


func (tm *Manager) RunAndBlockWithEvents(task Task, ev *events.Events) *Result {
	log := ev.
		With("taskId", task.GetId()).
		With("taskName", task.GetName()).
		With("taskDescription", task.GetDescription()).
		With("taskEventPrefix", task.GetEventPrefix()).
		WithMany(task.GetLogVars())
	if task.GetEnvironment() != nil {
		log = log.WithEnv(task.GetEnvironment())
	}
	log.Debug("Start Task: " + task.GetName())
	result, entity := tm.getResult(task)
	if result.Success == true {
		log.Debug(task.GetName() + " successful.")
		if task.GetOnSuccess() != "" {
			if next, ok := tm.getRegisteredTask(task.GetOnSuccess()); ok {
				log.Debug(task.GetName() + " --on success--> " + next.GetName())
				result := tm.RunAndBlockWithEvents(next,ev)
				if result.Success {
					entity.State = "successfull"
					_ = tm.db.Save(&entity)
					return result
				} else {
					entity.State = "failed"
					_ = tm.db.Save(&entity)
					log.Error("Task could not recover")
				}
			}
		}
		return result
	}
	if result.Success == false {
		if task.GetResult().Message != "" {
			log.Debug("Task failed: " + task.GetResult().Message)
		} else {
			log.Debug(task.GetName() + " failed")
		}
		if task.GetOnFail() != "" {
			if next, ok := tm.getRegisteredTask(task.GetOnFail()); ok {
				log.Debug(task.GetName() + " --recover with--> " + next.GetName())
				result := tm.RunAndBlockWithEvents(next,ev)
				if result.Success {
					entity.State = "recovered"
					_ = tm.db.Save(&entity)
					log.Debug(task.GetName() + " recovered")

					if next, ok := tm.getRegisteredTask(task.GetOnSuccess()); ok {
						log.Debug(task.GetName() + " --on success--> " + next.GetName())
						result := tm.RunAndBlockWithEvents(next,ev)
						if result.Success {
							entity.State = "successfull"
							_ = tm.db.Save(&entity)
							return result
						} else {
							entity.State = "failed"
							_ = tm.db.Save(&entity)
							log.Error("Task could not recover")
						}
					}
					return result
				} else {
					entity.State = "failed"
					_ = tm.db.Save(&entity)
					log.Error("Task could not recover")
					return result
				}
			}
		} else {
			entity.State = "failed"
			_ = tm.db.Save(&entity)
			log.Error("Task has no OnFail, failed.")
			return result
		}
	}

	time.Sleep(10 * time.Second)
	return result
}

func (tm *Manager) RunAndIgnore(t Task) {
	go tm.RunAndBlock(t)
}
