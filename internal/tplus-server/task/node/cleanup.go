package node

import (
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"os"
	"sync"
)

/*
 * Removes all chain data from the data dir and restart
 */
type RemoveEnvDataTask struct {
	task.Base
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.Result
	Status      string
	Id          string
}


func NewRemoveEnvDataTask(e *db.Environment) *RemoveEnvDataTask {
	t := RemoveEnvDataTask{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	return &t
}

func (task *RemoveEnvDataTask) GetResult() *task.Result {
	task.wg.Wait()
	return &task.result
}

func (task *RemoveEnvDataTask) GetProgress() string {
	return task.Status
}

func (task *RemoveEnvDataTask) GetEventPrefix() string {
	return "remove_data_env"
}

func (task *RemoveEnvDataTask) GetName() string {
	return "Clean Environment"
}

func (task *RemoveEnvDataTask) GetEnvironment() *db.Environment {
	return task.Environment
}

func (task *RemoveEnvDataTask) GetDescription() string {
	return "Remove all chain-Data from Environment"
}

func (task *RemoveEnvDataTask) GetId() string {
	return task.Id
}

func (task *RemoveEnvDataTask) Run() {

	if len(task.Environment.Id) >= 5 {
		os.RemoveAll(task.Config.DataDir + "/envs/" + task.Environment.Id)
		os.Remove(task.Config.DataDir + "/envs/" + task.Environment.Id)
	}

	task.result.Success = true
	task.result.Message = "Cleared all Chain Data and Backups"
	task.wg.Done()
}
