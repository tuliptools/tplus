package node

import (
	"encoding/json"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/service/docker"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"sync"
	"time"
)

type ClientContainerCheckTask struct {
	task.Base
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.Result
	Status      string
	Id          string
}

func NewClientContainerCheckTask(e *db.Environment) *ClientContainerCheckTask {
	t := ClientContainerCheckTask{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	return &t
}

func (task *ClientContainerCheckTask) GetResult() *task.Result {
	task.wg.Wait()
	return &task.result
}

func (task *ClientContainerCheckTask) GetProgress() string {
	return task.Status
}

func (task *ClientContainerCheckTask) GetEventPrefix() string {
	return "client_container_check"
}

func (task *ClientContainerCheckTask) GetName() string {
	return "Client Container Check"
}

func (task *ClientContainerCheckTask) GetEnvironment() *db.Environment {
	return task.Environment
}

func (task *ClientContainerCheckTask) GetDescription() string {
	return "Check if container exists"
}

func (task *ClientContainerCheckTask) GetId() string {
	return task.Id
}

func (task *ClientContainerCheckTask) Run() {
	if task.Environment.ClientContainer.Id == "" {
		task.result.Success = false
		task.result.Message = "Container does not exists"
		task.wg.Done()
		return
	} else {
		id := task.Environment.ClientContainer.Id
		if len(id) <= 4 {
			task.result.Success = false
			task.result.Message = "Container does not exists"
			task.wg.Done()
			return
		}
	}
	task.result.Success = true
	task.result.Message = "Container exists"
	task.wg.Done()
	return
}

//--------------------------------------------------------------------------------------------------------------------//

type ClientContainerCreateTask struct {
	task.Base
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.Result
	Status      string
	Id          string
}

func NewClientContainerCreateTask(e *db.Environment) *ClientContainerCreateTask {
	t := ClientContainerCreateTask{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	return &t
}

func (task *ClientContainerCreateTask) GetResult() *task.Result {
	task.wg.Wait()
	return &task.result
}

func (task *ClientContainerCreateTask) GetProgress() string {
	return task.Status
}

func (task *ClientContainerCreateTask) GetEventPrefix() string {
	return "client_container_create"
}

func (task *ClientContainerCreateTask) GetName() string {
	return "Client Container create"
}

func (task *ClientContainerCreateTask) GetEnvironment() *db.Environment {
	return task.Environment
}

func (task *ClientContainerCreateTask) GetDescription() string {
	return "Creating docker container"
}

func (task *ClientContainerCreateTask) GetId() string {
	return task.Id
}

func (task *ClientContainerCreateTask) Run() {

	template := docker.GetClientTemplate(task.Environment)
	id, err := task.Docker.ContainerCreate(template)
	if err != nil {
		task.result.Success = false
		task.result.Message = err.Error()
		task.wg.Done()
		return
	}

	task.Environment.ClientContainer.Id = id
	task.SaveChan <- task.Environment

	time.Sleep(2 * time.Second)

	infoLogs, _ := task.Docker.GetContainerInfo(id)

	b, _ := json.MarshalIndent(infoLogs, "", "    ")

	task.result.Logs = map[string]string{
		"Docker Inspect": string(b),
	}

	task.result.Success = true
	task.result.Message = "Container created"
	task.wg.Done()
	return
}

//--------------------------------------------------------------------------------------------------------------------//

type ClientContainerStartTask struct {
	task.Base
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.Result
	Status      string
	Id          string
}

func NewClientContainerStartTask(e *db.Environment) *ClientContainerStartTask {
	t := ClientContainerStartTask{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	return &t
}

func (task *ClientContainerStartTask) GetLogVars() map[string]string{
	m := map[string]string{}
	return m
}


func (task *ClientContainerStartTask) GetResult() *task.Result {
	task.wg.Wait()
	return &task.result
}

func (task *ClientContainerStartTask) GetProgress() string {
	return task.Status
}

func (task *ClientContainerStartTask) GetEventPrefix() string {
	return "client_container_start"
}

func (task *ClientContainerStartTask) GetName() string {
	return "Container Start"
}

func (task *ClientContainerStartTask) GetEnvironment() *db.Environment {
	return task.Environment
}

func (task *ClientContainerStartTask) GetDescription() string {
	return "Start docker container"
}

func (task *ClientContainerStartTask) GetId() string {
	return task.Id
}

func (task *ClientContainerStartTask) Run() {
	time.Sleep(1 * time.Second)
	if task.Environment.Consoles == nil {
		task.Environment.Consoles = map[string]db.Container{}
	}

	id := task.Environment.ClientContainer.Id

	info, err := task.Docker.GetContainerInfo(id)
	if err != nil {
		task.result.Success = false
		task.result.Message = err.Error()
		task.wg.Done()
		return
	}

	if info.State.Status == "running" {
		infoLogs, _ := task.Docker.GetContainerInfo(id)
		task.Environment.ClientContainer.Ips = []string{}
		for _, n := range infoLogs.NetworkSettings.Networks {
			task.Environment.ClientContainer.Ips = append(task.Environment.ClientContainer.Ips, n.IPAddress)
		}
		task.Environment.Consoles["client"] = task.Environment.ClientContainer
		task.SaveChan <- task.Environment

		task.result.Success = true
		task.result.Message = "Container already running"
		task.wg.Done()
		return
	}
	err = task.Docker.ContainerStart(id)

	if err != nil {
		task.result.Success = false
		task.result.Message = err.Error()
		task.wg.Done()
		return
	}

	task.Environment.ClientContainer.Status = "running"
	task.SaveChan <- task.Environment

	time.Sleep(200 * time.Millisecond)

	infoLogs, _ := task.Docker.GetContainerInfo(id)

	b, _ := json.MarshalIndent(infoLogs, "", "    ")

	task.result.Logs = map[string]string{
		"Docker Inspect": string(b),
	}

	task.Environment.ClientContainer.Ips = []string{}
	for _, n := range infoLogs.NetworkSettings.Networks {
		task.Environment.ClientContainer.Ips = append(task.Environment.ClientContainer.Ips, n.IPAddress)
	}

	task.Environment.Consoles["client"] = task.Environment.ClientContainer
	task.SaveChan <- task.Environment
	task.result.Success = true
	task.result.Message = "Container started"
	task.wg.Done()
	return
}
