package node

// dir := task.Config.DataDir + "/envs/" + task.Environment.Id

import (
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"io/ioutil"
	"os"
	"sync"
)

type ClientConfigCheck struct {
	task.Base
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.Result
	Status      string
	Id          string
}

func NewClientConfigCheck(e *db.Environment) *ClientConfigCheck {
	t := ClientConfigCheck{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	return &t
}


func (task *ClientConfigCheck) GetResult() *task.Result {
	task.wg.Wait()
	return &task.result
}

func (task *ClientConfigCheck) GetProgress() string {
	return task.Status
}

func (task *ClientConfigCheck) GetEventPrefix() string {
	return "client_config_check"
}

func (task *ClientConfigCheck) GetName() string {
	return "Config Check"
}

func (task *ClientConfigCheck) GetEnvironment() *db.Environment {
	return task.Environment
}

func (task *ClientConfigCheck) GetDescription() string {
	return "Make sure config file exists"
}

func (task *ClientConfigCheck) GetId() string {
	return task.Id
}

func (task *ClientConfigCheck) Run() {
	if _, err := os.Stat(task.Environment.Tezos.ClientDataDir + "/config.json"); os.IsNotExist(err) {
		task.result.Success = false
		task.result.Message = "Config does not exists"
	} else {
		task.result.Success = true
		task.result.Message = "config exists"
		bytes, _ := ioutil.ReadFile(task.Environment.Tezos.ClientDataDir + "/config.json")
		task.result.Logs = map[string]string{
			"Config file": string(bytes),
		}
	}
	task.wg.Done()
}

/*
 ***********************************************************************************
 */

type ClientConfigCreate struct {
	task.Base
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.Result
	Status      string
	Id          string
}

func NewClientConfigCreate(e *db.Environment) *ClientConfigCreate {
	t := ClientConfigCreate{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	return &t
}

func (task *ClientConfigCreate) GetLogVars() map[string]string{
	m := map[string]string{}
	return m
}

func (task *ClientConfigCreate) GetResult() *task.Result {
	task.wg.Wait()
	return &task.result
}

func (task *ClientConfigCreate) GetProgress() string {
	return task.Status
}

func (task *ClientConfigCreate) GetEventPrefix() string {
	return "client_config_create"
}

func (task *ClientConfigCreate) GetName() string {
	return "Config create"
}

func (task *ClientConfigCreate) GetEnvironment() *db.Environment {
	return task.Environment
}

func (task *ClientConfigCreate) GetDescription() string {
	return "Write Client config File"
}

func (task *ClientConfigCreate) GetId() string {
	return task.Id
}

func (task *ClientConfigCreate) Run() {

	filename := "./tezos/client_config.json"

	file, err := task.Box.Find(filename)
	if err != nil {
		task.result.Success = false
		task.wg.Done()
		return
	}

	err = ioutil.WriteFile(task.Environment.Tezos.ClientDataDir+"/config", file, 0660)
	if err != nil {
		task.result.Success = false
		task.result.Message = "Error writing File: " + err.Error()
		task.wg.Done()
		return
	}

	task.result.Success = true
	task.result.Message = "Config file written."
	bytes, _ := ioutil.ReadFile(task.Environment.Tezos.ClientDataDir + "/config")
	task.result.Logs = map[string]string{
		"Config file": string(bytes),
	}
	task.wg.Done()
}
