package node

// dir := task.Config.DataDir + "/envs/" + task.Environment.Id

import (
	"bytes"
	"github.com/tyler-sommer/stick"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"io/ioutil"
	"os"
	"sync"
)

type ConfigCheck struct {
	task.Base
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.Result
	Status      string
	Id          string
}

func NewNodeConfigCheck(e *db.Environment) *ConfigCheck {
	t := ConfigCheck{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	return &t
}

func (task *ConfigCheck) GetResult() *task.Result {
	task.wg.Wait()
	return &task.result
}

func (task *ConfigCheck) GetProgress() string {
	return task.Status
}

func (task *ConfigCheck) GetEventPrefix() string {
	return "node_config_check"
}

func (task *ConfigCheck) GetName() string {
	return "Config Check"
}

func (task *ConfigCheck) GetEnvironment() *db.Environment {
	return task.Environment
}

func (task *ConfigCheck) GetDescription() string {
	return "Make sure config file exists"
}

func (task *ConfigCheck) GetId() string {
	return task.Id
}

func (task *ConfigCheck) Run() {
	if _, err := os.Stat(task.Environment.Tezos.DataDir + "/config.json"); os.IsNotExist(err) {
		task.result.Success = false
		task.result.Message = "Config does not exists"
	} else {
		task.result.Success = true
		task.result.Message = "config exists"
		bytes, _ := ioutil.ReadFile(task.Environment.Tezos.DataDir + "/config.json")
		task.result.Logs = map[string]string{
			"Config file": string(bytes),
		}
	}
	task.wg.Done()
}

/*
 ***********************************************************************************
 */

type ConfigCreate struct {
	task.Base
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.Result
	Status      string
	Id          string
}

func NewNodeConfigCreate(e *db.Environment) *ConfigCreate {
	t := ConfigCreate{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	return &t
}

func (task *ConfigCreate) GetResult() *task.Result {
	task.wg.Wait()
	return &task.result
}

func (task *ConfigCreate) GetProgress() string {
	return task.Status
}

func (task *ConfigCreate) GetEventPrefix() string {
	return "node_config_create"
}

func (task *ConfigCreate) GetName() string {
	return "Config create"
}

func (task *ConfigCreate) GetEnvironment() *db.Environment {
	return task.Environment
}

func (task *ConfigCreate) GetDescription() string {
	return "Write Tezos config File"
}

func (task *ConfigCreate) GetId() string {
	return task.Id
}

func (task *ConfigCreate) Run() {

	filename := "./tezos/config_" + task.Environment.Tezos.Branch + ".json.twig"

	file, err := task.Box.FindString(filename)
	if err != nil {
		task.result.Success = false
		task.wg.Done()
		return
	}

	vars := map[string]stick.Value{
		"privatemode": "false",
		"historymode": task.Environment.Tezos.HistoryMode,
		"loglevel":    "info",
		"network":     task.Environment.Tezos.Branch,
	}
	env := stick.New(nil)
	filebuffer := bytes.Buffer{}
	if err := env.Execute(file, &filebuffer, vars); err != nil {
		task.result.Success = false
		task.result.Message = "Error from templating: " + err.Error()
		task.wg.Done()
		return
	}

	err = ioutil.WriteFile(task.Environment.Tezos.DataDir+"/config.json", filebuffer.Bytes(), 0660)
	if err != nil {
		task.result.Success = false
		task.result.Message = "Error writing File: " + err.Error()
		task.wg.Done()
		return
	}

	if task.Environment.Tezos.Branch == "sandbox" {
		file := task.Environment.SandboxConfig
		ioutil.WriteFile(task.Environment.Tezos.ConfigDir+"/sandbox.json", []byte(file), 0777)

		file = task.Environment.SandboxParams
		ioutil.WriteFile(task.Environment.Tezos.ConfigDir+"/protocol_parameters.json", []byte(file), 0777)
	}

	task.result.Success = true
	task.result.Message = "Config file written."
	bytes, _ := ioutil.ReadFile(task.Environment.Tezos.DataDir + "/config.json")
	task.result.Logs = map[string]string{
		"Config file": string(bytes),
	}
	task.wg.Done()
}
