package node

import (
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"sync"
)

/*
 * Removes all chain data from the data dir and restart
 */
type RemoveEnvContainerTask struct {
	task.Base
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.Result
	Status      string
	Id          string
}

func NewRemoveEnvContainerTask(e *db.Environment) *RemoveEnvContainerTask {
	t := RemoveEnvContainerTask{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	return &t
}

func (task *RemoveEnvContainerTask) GetResult() *task.Result {
	task.wg.Wait()
	return &task.result
}

func (task *RemoveEnvContainerTask) GetProgress() string {
	return task.Status
}

func (task *RemoveEnvContainerTask) GetEventPrefix() string {
	return "remove_containers"
}

func (task *RemoveEnvContainerTask) GetName() string {
	return "Destroy Environment Container"
}

func (task *RemoveEnvContainerTask) GetEnvironment() *db.Environment {
	return task.Environment
}

func (task *RemoveEnvContainerTask) GetDescription() string {
	return "Docker Cleanup ( Container + Networks )"
}

func (task *RemoveEnvContainerTask) GetId() string {
	return task.Id
}

func (task *RemoveEnvContainerTask) Run() {

	task.Docker.ContainerRM(task.Environment.Tezos.Container.Id)
	containers, _ := task.Docker.FindContainersForEnv(task.Environment.Id)

	for _, c := range containers {
		task.Docker.StopContainer(c)
		task.Docker.DeleteContainer(c)
	}
	task.Docker.RemoveNetwork(task.Environment.Tezos.Container.Network)

	task.result.Success = true
	task.result.Message = "Cleared all Networks and Containers"
	task.wg.Done()
}



/*
 * Removes only Node Container
 */
type RemoveNodeContainerTask struct {
	task.Base
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.Result
	Status      string
	Id          string
}

func NewRemoveNodeContainerTask(e *db.Environment) *RemoveNodeContainerTask {
	t := RemoveNodeContainerTask{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	return &t
}

func (task *RemoveNodeContainerTask) GetResult() *task.Result {
	task.wg.Wait()
	return &task.result
}

func (task *RemoveNodeContainerTask) GetProgress() string {
	return task.Status
}

func (task *RemoveNodeContainerTask) GetEventPrefix() string {
	return "remove_node"
}

func (task *RemoveNodeContainerTask) GetName() string {
	return "Destroy Node Container"
}

func (task *RemoveNodeContainerTask) GetEnvironment() *db.Environment {
	return task.Environment
}

func (task *RemoveNodeContainerTask) GetDescription() string {
	return "Remove only node container"
}

func (task *RemoveNodeContainerTask) GetId() string {
	return task.Id
}

func (task *RemoveNodeContainerTask) Run() {


	id,_ := task.Docker.GetContainerId(task.Environment.Tezos.Container.Name)
	e1 := task.Docker.ContainerRM(id)
	if e1 != nil {
		task.result.Success = false
		task.result.Message = e1.Error()
		task.wg.Done()
		return
	}
	task.Environment.Tezos.Container.Id = ""

	id,_ = task.Docker.GetContainerId(task.Environment.Tezos.Container.Name + "_client")
	task.Docker.ContainerRM(id)

	task.Environment.ClientContainer.Id = ""

	task.SaveChan <- task.Environment
	task.result.Success = true
	task.result.Message = "Removed Node Container"
	task.wg.Done()
}
