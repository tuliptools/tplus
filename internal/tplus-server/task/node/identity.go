package node

import (
	"fmt"
	"github.com/cstockton/go-conv"
	"github.com/docker/docker/api/types/mount"
	"github.com/docker/docker/api/types/strslice"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/version"
	"io/ioutil"
	"os"
	"strconv"
	"sync"
	"time"
)

type IdentityCheckTask struct {
	task.Base
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.Result
	Status      string
	Id          string
}

func NewNodeIdentityCheckTask(e *db.Environment) *IdentityCheckTask {
	t := IdentityCheckTask{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	return &t
}

func (task *IdentityCheckTask) GetResult() *task.Result {
	task.wg.Wait()
	return &task.result
}

func (task *IdentityCheckTask) GetProgress() string {
	return task.Status
}

func (task *IdentityCheckTask) GetEventPrefix() string {
	return "node_identity_check"
}

func (task *IdentityCheckTask) GetName() string {
	return "Identity file check"
}

func (task *IdentityCheckTask) GetEnvironment() *db.Environment {
	return task.Environment
}

func (task *IdentityCheckTask) GetDescription() string {
	return "Check if identity file exists"
}

func (task *IdentityCheckTask) GetId() string {
	return task.Id
}

func (task *IdentityCheckTask) Run() {
	if _, err := os.Stat(task.Environment.Tezos.DataDir + "/identity.json"); os.IsNotExist(err) {
		task.result.Success = false
		task.result.Message = "Identity file does not exists"
	} else {
		task.result.Success = true
		task.result.Message = "Identity file exists"
	}
	task.wg.Done()
}

//--------------------------------------------------------------------------------------------------------------------//

type IdentityCreateTask struct {
	task.Base
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.Result
	Status      string
	Id          string
}

func NewNodeIdentityCreateTask(e *db.Environment) *IdentityCreateTask {
	t := IdentityCreateTask{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	return &t
}

func (task *IdentityCreateTask) GetResult() *task.Result {
	task.wg.Wait()
	return &task.result
}

func (task *IdentityCreateTask) GetProgress() string {
	return task.Status
}

func (task *IdentityCreateTask) GetEventPrefix() string {
	return "node_identity_generate"
}

func (task *IdentityCreateTask) GetName() string {
	return "Identity create"
}

func (task *IdentityCreateTask) GetEnvironment() *db.Environment {
	return task.Environment
}

func (task *IdentityCreateTask) GetDescription() string {
	return "Creating new Identity File"
}

func (task *IdentityCreateTask) GetId() string {
	return task.Id
}

func (task *IdentityCreateTask) Run() {

	if task.Environment.Tezos.Branch == "sandbox" {
		file := task.Environment.Tezos.DataDir + "/identity.json"
		content, _ := task.Box.Find("tezos/default_identity.json")
		ioutil.WriteFile(file, content, 0777)

		task.result.Success = true
		task.result.Message = "Identity generated"
		task.wg.Done()
		return
	}

	u, _ := conv.String(time.Now().UnixNano())
	donechan := make(chan struct{})
	randid := db.GetRandomString(4)
	max := 3
	for i := 1; i <= max; i++ {
		go func(i int, e *db.Environment, done chan struct{}) {
			_, err := task.Docker.RunTezosTask(
				"tplus_"+randid+"_identity_gen_task"+u+"_"+strconv.Itoa(i),
				version.GetVersionInfo().TezosImage,
				e.Tezos.Container.Network,
				strslice.StrSlice{""},
				strslice.StrSlice{"/usr/local/bin/tezos-node", "identity", "generate", "--data-dir", "/tezosdata"},
				[]mount.Mount{
					{
						Type:   mount.TypeBind,
						Source: e.Tezos.DataDir,
						Target: "/tezosdata",
					},
				},
			)
			if err != nil {
				fmt.Println(err.Error())
				task.result.Message = "Error creating identity: " + err.Error()
				task.result.Success = false
				return
			}
			done <- struct{}{}
		}(i, task.Environment, donechan)
	}
	<-donechan
	task.Docker.DeleteLike("tplus_" + randid + "_identity_gen_task" + u)
	time.Sleep(500 * time.Millisecond)
	task.result.Success = true
	task.result.Message = "Identity generated"
	task.wg.Done()
}
