package node

import (
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"strings"
	"sync"
)

type NetworkCheckTask struct {
	task.Base
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.Result
	Status      string
	Id          string
}

func NewNetworkCheckTask(e *db.Environment) *NetworkCheckTask {
	t := NetworkCheckTask{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	return &t
}

func (task *NetworkCheckTask) GetResult() *task.Result {
	task.wg.Wait()
	return &task.result
}

func (task *NetworkCheckTask) GetProgress() string {
	return task.Status
}

func (task *NetworkCheckTask) GetEventPrefix() string {
	return "docker_network_check"
}

func (task *NetworkCheckTask) GetName() string {
	return "Check Network"
}

func (task *NetworkCheckTask) GetEnvironment() *db.Environment {
	return task.Environment
}

func (task *NetworkCheckTask) GetDescription() string {
	return "Check if Docker Network exists"
}

func (task *NetworkCheckTask) GetId() string {
	return task.Id
}

func (task *NetworkCheckTask) Run() {

	if task.Environment.Tezos.Container.Network == "" {
		task.Environment.Tezos.Container.Network = "tplus_net_" + strings.Replace(strings.ToLower(task.Environment.Name), " ", "", -1) + "_" + db.GetRandomString(4)
		task.SaveChan <- task.Environment
	}

	res, e := task.Docker.CheckNetworkExists(task.Environment.Tezos.Container.Network)

	if e != nil {
		task.result.Success = false
		task.result.Message = e.Error()
		task.wg.Done()
		return
	}

	if res == false {
		task.result.Success = false
		task.result.Message = "Network does not exist"
		task.wg.Done()
		return
	}

	task.result.Success = true
	task.result.Message = "Network exists"
	task.wg.Done()
}

//--------------------------------------------------------------------------------------------------------------------//

type NetworkCreateTask struct {
	task.Base
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.Result
	Status      string
	Id          string
}

func NewNetworkCreateTask(e *db.Environment) *NetworkCreateTask {
	t := NetworkCreateTask{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	return &t
}

func (task *NetworkCreateTask) GetResult() *task.Result {
	task.wg.Wait()
	return &task.result
}

func (task *NetworkCreateTask) GetProgress() string {
	return task.Status
}

func (task *NetworkCreateTask) GetEventPrefix() string {
	return "docker_network_create"
}

func (task *NetworkCreateTask) GetName() string {
	return "Create Network"
}

func (task *NetworkCreateTask) GetEnvironment() *db.Environment {
	return task.Environment
}

func (task *NetworkCreateTask) GetDescription() string {
	return "Create Docker Network"
}

func (task *NetworkCreateTask) GetId() string {
	return task.Id
}

func (task *NetworkCreateTask) Run() {

	id, e := task.Docker.CreateNetwork(task.Environment.Tezos.Container.Network)

	if e != nil {
		task.result.Success = false
		task.result.Message = e.Error()
		task.wg.Done()
		return
	}

	task.Environment.Tezos.Container.NetworkID = id
	task.SaveChan <- task.Environment

	task.result.Success = true
	task.result.Message = "Network created"
	task.wg.Done()
}
