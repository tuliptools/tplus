package node

import (
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"strings"
	"sync"
	"time"
)

type SandboxSetupTask struct {
	task.Base
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.Result
	Status      string
	Id          string
}

func NewSandboxSetupTask(e *db.Environment) *SandboxSetupTask {
	t := SandboxSetupTask{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	return &t
}

func (task *SandboxSetupTask) GetResult() *task.Result {
	task.wg.Wait()
	return &task.result
}

func (task *SandboxSetupTask) GetProgress() string {
	return task.Status
}

func (task *SandboxSetupTask) GetEventPrefix() string {
	return "client_sandbox_setup"
}

func (task *SandboxSetupTask) GetName() string {
	return "Sandbox Setup"
}

func (task *SandboxSetupTask) GetEnvironment() *db.Environment {
	return task.Environment
}

func (task *SandboxSetupTask) GetDescription() string {
	return "Setup Client and Protocol"
}

func (task *SandboxSetupTask) GetId() string {
	return task.Id
}

func (task *SandboxSetupTask) Run() {

	time.Sleep(1 * time.Second)
	if task.Environment.SandboxSetupDone {
		task.result.Success = true
		task.result.Message = "Skipped, not first setup"
		task.wg.Done()
		return
	}

	task.Environment.SandboxSetupDone = true
	task.SaveChan <- task.Environment

	client := task.Environment.ClientContainer.Id

	cmds := []string{
		"tezos-client import secret key activator unencrypted:edsk31vznjHSSpGExDMHYASz45VZqXN4DPxvsa4hAyY8dHM28cZzp6",
		"tezos-client import secret key bootstrap1 unencrypted:edsk3gUfUPyBSfrS9CCgmCiQsTCHGkviBDusMxDJstFtojtc1zcpsh",
		"tezos-client import secret key bootstrap2 unencrypted:edsk39qAm1fiMjgmPkw1EgQYkMzkJezLNewd7PLNHTkr6w9XA2zdfo",
		"tezos-client import secret key bootstrap3 unencrypted:edsk4ArLQgBTLWG5FJmnGnT689VKoqhXwmDPBuGx3z4cvwU9MmrPZZ",
		"tezos-client import secret key baker unencrypted:edsk4QLrcijEffxV31gGdN2HU7UpyJjA8drFoNcmnB28n89YjPNRFm",
	}

	for _, c := range cmds {
		task.Docker.ExecInContainerById(client, strings.Split(c, " "), false)
	}

	if task.Environment.Protocol != "ProtoGenesis" {
		timestamp := time.Now().Add(-24 * time.Hour).Format("2006-01-02T15:04:05Z")
		cmd := []string{"tezos-client", "-block", "genesis", "activate", "protocol", task.Environment.Protocol, "with", "fitness", "1", "and", "key", "activator", "and", "parameters", "/tezosconf/protocol_parameters.json", "-timestamp", timestamp}
		task.Docker.ExecInContainerById(client, cmd, false)

		time.Sleep(2 * time.Second)
		cmd = []string{"tezos-client", "bake", "for", "baker"}
		task.Docker.ExecInContainerById(client, cmd, false)

		time.Sleep(2 * time.Second)
		cmd = []string{"tezos-client", "bake", "for", "baker"}
		task.Docker.ExecInContainerById(client, cmd, false)

		time.Sleep(2 * time.Second)
		cmd = []string{"tezos-client", "bake", "for", "baker"}
		task.Docker.ExecInContainerById(client, cmd, false)

		time.Sleep(2 * time.Second)
		cmd = []string{"tezos-client", "bake", "for", "baker"}
		task.Docker.ExecInContainerById(client, cmd, false)

	}

	task.result.Success = true
	task.result.Message = "Setup done"
	task.wg.Done()
	return
}
