package node

// dir := task.Config.DataDir + "/envs/" + task.Environment.Id

import (
	"encoding/json"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/service/docker"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"io"
	"net/http"
	"os"
	"sync"
	"time"
)

type SnapshotCheck struct {
	task.Base
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.Result
	Status      string
	Id          string
}

func NewSnapshotCheckTask(e *db.Environment) *SnapshotCheck {
	t := SnapshotCheck{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	return &t
}

func (task *SnapshotCheck) GetResult() *task.Result {
	task.wg.Wait()
	return &task.result
}

func (task *SnapshotCheck) GetProgress() string {
	return task.Status
}

func (task *SnapshotCheck) GetEventPrefix() string {
	return "node_snapshot_check"
}

func (task *SnapshotCheck) GetName() string {
	return "Snapshot Check"
}

func (task *SnapshotCheck) GetEnvironment() *db.Environment {
	return task.Environment
}

func (task *SnapshotCheck) GetDescription() string {
	return "Make sure snapshot downloaded"
}

func (task *SnapshotCheck) GetId() string {
	return task.Id
}

func (task *SnapshotCheck) Run() {
	// skip if not first start, or no src specified
	if task.Environment.SnapshotSource == "" {
		task.result.Success = true
		task.result.Message = "Skip snapshot import: not source specified"
		task.wg.Done()
		return
	}

	if task.Environment.Tezos.HeadLevel >= 1 {
		task.result.Success = true
		task.result.Message = "Skip snapshot import: not first start"
		task.wg.Done()
		return
	}

	if _, err := os.Stat(task.Environment.Tezos.SnapshotDir + "start.snap"); os.IsNotExist(err) {
		task.result.Success = false
		task.result.Message = "Snapshot does not exists"
	} else {
		task.result.Success = true
		task.result.Message = "snapshot exists"
	}
	task.wg.Done()
}

/*
 ***********************************************************************************
 */

type SnapshotDownload struct {
	task.Base
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.Result
	Status      string
	Id          string
}

func NewSnapshotDownloadTask(e *db.Environment) *SnapshotDownload {
	t := SnapshotDownload{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	return &t
}

func (task *SnapshotDownload) GetResult() *task.Result {
	task.wg.Wait()
	return &task.result
}

func (task *SnapshotDownload) GetProgress() string {
	return task.Status
}

func (task *SnapshotDownload) GetEventPrefix() string {
	return "node_snapshot_download"
}

func (task *SnapshotDownload) GetName() string {
	return "Download Snapshot"
}

func (task *SnapshotDownload) GetEnvironment() *db.Environment {
	return task.Environment
}

func (task *SnapshotDownload) GetDescription() string {
	return "Downloading Snapshot file and prepare for import"
}

func (task *SnapshotDownload) GetId() string {
	return task.Id
}

func (task *SnapshotDownload) Run() {
	err := DownloadFile(task.Environment.Tezos.SnapshotDir+"/start.snap", task.Environment.SnapshotSource)
	if err == nil {
		task.result.Success = true
		task.result.Message = "Snapshot file written."
		task.wg.Done()
	} else {
		task.result.Success = false
		task.result.Message = err.Error()
		task.wg.Done()
	}
}

func DownloadFile(filepath string, url string) error {

	// Get the data
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// CreatePublicNetworkNode the file
	out, err := os.Create(filepath)
	if err != nil {
		return err
	}
	defer out.Close()

	// Write the body to file
	_, err = io.Copy(out, resp.Body)
	return err
}

/*
 ***********************************************************************************
 */

type SnapshotImport struct {
	task.Base
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.Result
	Status      string
	Id          string
}

func NewSnapshotImportTask(e *db.Environment) *SnapshotImport {
	t := SnapshotImport{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	return &t
}

func (task *SnapshotImport) GetResult() *task.Result {
	task.wg.Wait()
	return &task.result
}

func (task *SnapshotImport) GetProgress() string {
	return task.Status
}

func (task *SnapshotImport) GetEventPrefix() string {
	return "node_snapshot_import"
}

func (task *SnapshotImport) GetName() string {
	return "Import Snapshot"
}

func (task *SnapshotImport) GetEnvironment() *db.Environment {
	return task.Environment
}

func (task *SnapshotImport) GetDescription() string {
	return "Import Snapshot"
}

func (task *SnapshotImport) GetId() string {
	return task.Id
}

func (task *SnapshotImport) Run() {
	// skip if not first start, or no src specified
	if task.Environment.SnapshotSource == "" {
		task.result.Success = true
		task.result.Message = "Skip snapshot import: not source specified"
		task.wg.Done()
		return
	}

	if task.Environment.Tezos.HeadLevel >= 1 {
		task.result.Success = true
		task.result.Message = "Skip snapshot import: not first start"
		task.wg.Done()
		return
	}

	// delete in dir
	template := docker.GetSnapshotImportTemplate(task.Environment, "start.snap")

	id, err := task.Docker.ContainerCreate(template)

	if err != nil {
		task.result.Success = false
		task.result.Message = err.Error()
		task.wg.Done()
		return
	}

	time.Sleep(2 * time.Second)

	infoLogs, _ := task.Docker.GetContainerInfo(id)

	b, _ := json.MarshalIndent(infoLogs, "", "    ")

	task.result.Logs = map[string]string{
		"Import Container": string(b),
	}

	err = task.Docker.ContainerStart(id)

	if err != nil {
		task.result.Success = false
		task.result.Message = "Error starting import container"
		task.wg.Done()
		return
	}

	// wait for container exit
	for {
		time.Sleep(3)
		infoLogs, _ := task.Docker.GetContainerInfo(id)
		if infoLogs.State.Running != true {
			break
		}
	}

	task.result.Success = true
	task.result.Message = "Snapshot imported"
	task.wg.Done()
}
