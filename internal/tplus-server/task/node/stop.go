package node

import (
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"sync"
	"time"
)

type StopEnvTask struct {
	task.Base
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.Result
	Status      string
	Id          string
}

func NewStopEnvTask(e *db.Environment) *StopEnvTask {
	t := StopEnvTask{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	return &t
}

func (task *StopEnvTask) GetResult() *task.Result {
	task.wg.Wait()
	return &task.result
}

func (task *StopEnvTask) GetProgress() string {
	return task.Status
}

func (task *StopEnvTask) GetEventPrefix() string {
	return "stop_env"
}

func (task *StopEnvTask) GetName() string {
	return "Stop Environment"
}

func (task *StopEnvTask) GetEnvironment() *db.Environment {
	return task.Environment
}

func (task *StopEnvTask) GetDescription() string {
	return "Environment Stop"
}

func (task *StopEnvTask) GetId() string {
	return task.Id
}

func (task *StopEnvTask) Run() {

	task.Environment.Tezos.Status = "stopping..."
	task.Environment.Tezos.Container.Status = "stopping..."
	delete(task.Environment.Consoles, "node")
	task.SaveChan <- task.Environment

	task.Docker.StopContainer(task.Environment.Tezos.Container.Id)
	task.Docker.StopContainer(task.Environment.ClientContainer.Id)

	i := 0

	for {
		i++
		time.Sleep(1 * time.Second)
		if i >= 5 {
			task.result.Success = true
			task.result.Message = "Stopped container"
			task.wg.Done()
			break
		}

		info, err := task.Docker.GetContainerInfo(task.Environment.Tezos.Container.Id)
		if err == nil && info.State.Running == false {
			task.Environment.Tezos.Status = "stopped"
			task.Environment.Tezos.Container.Status = "stopped"
			task.SaveChan <- task.Environment
			task.result.Success = true
			task.result.Message = "Stopped container: " + task.Environment.Tezos.Container.Id
			task.wg.Done()
			break
		}
	}

	containers, _ := task.Docker.FindContainersForEnv(task.Environment.Id)

	for _, c := range containers {
		task.Docker.StopContainer(c)
	}

}
