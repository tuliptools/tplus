package node

import (
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"os"
	"sync"
)

type StoragePrepareTask struct {
	task.Base
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.Result
	Status      string
	Id          string
}

func NewNodeStoragePrepareTask(e *db.Environment) *StoragePrepareTask {
	t := StoragePrepareTask{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	return &t
}

func (task *StoragePrepareTask) GetResult() *task.Result {
	task.wg.Wait()
	return &task.result
}

func (task *StoragePrepareTask) GetProgress() string {
	return task.Status
}

func (task *StoragePrepareTask) GetEventPrefix() string {
	return "node_storage"
}

func (task *StoragePrepareTask) GetName() string {
	return "Storage Prepare"
}

func (task *StoragePrepareTask) GetEnvironment() *db.Environment {
	return task.Environment
}

func (task *StoragePrepareTask) GetDescription() string {
	return "Preparing filesystem for Tezos"
}

func (task *StoragePrepareTask) GetId() string {
	return task.Id
}

func (task *StoragePrepareTask) Run() {
	basedir := task.Config.DataDir + "/envs/" + task.Environment.Id
	nodedata := basedir + "/node"
	clientdata := basedir + "/client"
	sharedDir := basedir + "/shared"
	snapshot := basedir + "/snapshots"
	backups := basedir + "/backups"
	configs := basedir + "/configs"
	projects := basedir + "/projects"
	dirs := []string{basedir, nodedata, sharedDir, clientdata, backups, snapshot, configs, projects}

	for _, dir := range dirs {
		e := os.MkdirAll(dir, 0777)
		if e == nil {
			task.result.Success = true
			task.result.Message = "Filesystem prepared"
		} else {
			task.result.Success = false
			task.result.Message = "Failed with: " + e.Error()
			task.wg.Done()
			return
		}
	}

	task.Environment.Tezos.DataDir = nodedata
	task.Environment.Tezos.SharedDir = sharedDir
	task.Environment.Tezos.ClientDataDir = clientdata
	task.Environment.Tezos.SnapshotDir = snapshot
	task.Environment.Tezos.ConfigDir = configs
	task.Environment.Tezos.BackupDir = backups
	task.SaveChan <- task.Environment
	task.wg.Done()
}
