package node

import (
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"sync"
	"time"
)

type SuccessTask struct {
	task.Base
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.Result
	Status      string
	Id          string
	Name        string
}

func (task *SuccessTask) GetResult() *task.Result {
	task.wg.Wait()
	return &task.result
}

func (task *SuccessTask) GetProgress() string {
	return task.Status
}

func (task *SuccessTask) GetEventPrefix() string {
	return "test"
}

func (task *SuccessTask) GetName() string {
	return task.Name
}

func (task *SuccessTask) GetEnvironment() *db.Environment {
	return task.Environment
}

func (task *SuccessTask) GetDescription() string {
	return "Testing Name"
}

func (task *SuccessTask) GetId() string {
	return task.Id
}

func (task *SuccessTask) Run() {
	time.Sleep(500 * time.Millisecond)
	task.result.Success = true
	task.wg.Done()
}

type FailureTask struct {
	task.Base
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.Result
	Status      string
	Id          string
	Name        string
}

func (task *FailureTask) GetResult() *task.Result {
	task.wg.Wait()
	return &task.result
}

func (task *FailureTask) GetProgress() string {
	return task.Status
}

func (task *FailureTask) GetEventPrefix() string {
	return "test"
}

func (task *FailureTask) GetName() string {
	return task.Name
}

func (task *FailureTask) GetEnvironment() *db.Environment {
	return task.Environment
}

func (task *FailureTask) GetDescription() string {
	return "Testing Name"
}

func (task *FailureTask) GetId() string {
	return task.Id
}

func (task *FailureTask) Run() {
	task.wg.Add(1)
	time.Sleep(500 * time.Millisecond)
	task.result.Success = false
	task.wg.Done()
}
