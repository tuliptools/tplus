package node

// dir := task.Config.DataDir + "/envs/" + task.Environment.Id

import (
	"github.com/cstockton/go-conv"
	"github.com/docker/docker/api/types/mount"
	"github.com/docker/docker/api/types/strslice"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/version"
	"io/ioutil"
	"os"
	"sync"
	"time"
)

type VersionFileCheck struct {
	task.Base
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.Result
	Status      string
	Id          string
}

func NewVersionFileCheck(e *db.Environment) *VersionFileCheck {
	t := VersionFileCheck{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	return &t
}

func (task *VersionFileCheck) GetResult() *task.Result {
	task.wg.Wait()
	return &task.result
}

func (task *VersionFileCheck) GetProgress() string {
	return task.Status
}

func (task *VersionFileCheck) GetEventPrefix() string {
	return "node_version_check"
}

func (task *VersionFileCheck) GetName() string {
	return "version.json Check"
}

func (task *VersionFileCheck) GetEnvironment() *db.Environment {
	return task.Environment
}

func (task *VersionFileCheck) GetDescription() string {
	return "Make sure version.json file exists"
}

func (task *VersionFileCheck) GetId() string {
	return task.Id
}

func (task *VersionFileCheck) Run() {
	if _, err := os.Stat(task.Environment.Tezos.DataDir + "/version.json"); os.IsNotExist(err) {
		task.result.Success = false
		task.result.Message = "file does not exists"
	} else {
		task.result.Success = true
		task.result.Message = "file exists"
	}
	task.wg.Done()
}

/*
 ***********************************************************************************
 */

type VersionFileCreate struct {
	task.Base
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.Result
	Status      string
	Id          string
}

func NewNodeVersionFileCreate(e *db.Environment) *VersionFileCreate {
	t := VersionFileCreate{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	return &t
}

func (task *VersionFileCreate) GetResult() *task.Result {
	task.wg.Wait()
	return &task.result
}

func (task *VersionFileCreate) GetProgress() string {
	return task.Status
}

func (task *VersionFileCreate) GetEventPrefix() string {
	return "node_version_create"
}

func (task *VersionFileCreate) GetName() string {
	return "version.json create"
}

func (task *VersionFileCreate) GetEnvironment() *db.Environment {
	return task.Environment
}

func (task *VersionFileCreate) GetDescription() string {
	return "Write Tezos version file"
}

func (task *VersionFileCreate) GetId() string {
	return task.Id
}

func (task *VersionFileCreate) Run() {

	if task.Environment.Tezos.Sandboxed {
		task.result.Success = true
		task.result.Message = "Skipped in Sandbox Mode"
		task.wg.Done()
		return
	}

	randid := db.GetRandomString(4)
	u, _ := conv.String(time.Now().UnixNano())

	res, err := task.Docker.RunTezosTask(
		"tplus_"+randid+"_version_"+u,
		version.GetVersionInfo().TezosImage,
		task.Environment.Tezos.Container.Network,
		strslice.StrSlice{""},
		strslice.StrSlice{"/bin/sh", "-c", "/usr/local/bin/tezos-node config init &&  /bin/cat ~/.tezos-node/version.json && echo \"\""},
		[]mount.Mount{},
	)
	if len(res) >= 8 {
		res = res[8:]
	}

	task.Docker.DeleteLike("tplus_" + randid + "_version_")

	if err != nil {
		task.result.Success = false
		task.result.Message = err.Error()
		task.wg.Done()
		return
	}

	err = ioutil.WriteFile(task.Environment.Tezos.DataDir+"/version.json", res, 0660)
	if err != nil {
		task.result.Success = false
		task.result.Message = "Error writing File: " + err.Error()
		task.wg.Done()
		return
	}

	task.result.Success = true
	task.result.Message = "Version file written"
	task.result.Logs = map[string]string{
		"Version File Content": string(res),
	}
	task.wg.Done()
	return

}
