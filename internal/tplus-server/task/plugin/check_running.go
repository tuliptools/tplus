package plugin

import (
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"strings"
	"sync"
)

type ContainerCheckTask struct {
	task.Base
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.Result
	Status      string
	Id          string
	plugin      *db.PluginService
}

func NewContainerCheckTask(e *db.Environment, p *db.PluginService) *ContainerCheckTask {
	t := ContainerCheckTask{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	t.plugin = p
	return &t
}

func (task *ContainerCheckTask) GetLogVars() map[string]string{
	m := map[string]string{
		"pluginService" : task.plugin.Name,
		"pluginServiceDescription" : task.plugin.Description,
	}
	return m
}


func (task *ContainerCheckTask) GetResult() *task.Result {
	task.wg.Wait()
	return &task.result
}

func (task *ContainerCheckTask) GetProgress() string {
	return task.Status
}

func (task *ContainerCheckTask) GetEventPrefix() string {
	return "plugin_check"
}

func (task *ContainerCheckTask) GetName() string {
	return "Check container for plugin"
}

func (task *ContainerCheckTask) GetEnvironment() *db.Environment {
	return task.Environment
}

func (task *ContainerCheckTask) GetDescription() string {
	return "making sure container is running"
}

func (task *ContainerCheckTask) GetId() string {
	return task.Id
}

func (task *ContainerCheckTask) Run() {
	task.result.NoLog = true
	envID := strings.Replace(task.Environment.Id, "env-", "", -1)
	envID = strings.ToLower(envID)
	sid := strings.ToLower(task.plugin.Name)
	sid = strings.Replace(sid, " ", "", -1)
	containerName := "tplus_plugin_" + envID + "_" + sid


	id, e := task.Docker.GetContainerId(containerName)
	if e == nil {
		info,e2 := task.Docker.GetContainerInfo(id)
		if e2 == nil && !info.State.Running {
			task.Docker.ContainerStart(id)
			task.result.Message = "Restarting Container"
			task.result.NoLog = false
		}
	}

	task.result.Success = true
	task.wg.Done()
}
