package plugin

import (
	"bytes"
	"fmt"
	"github.com/tyler-sommer/stick"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"io/ioutil"
	"net/http"
	"strings"
	"sync"
)

type ConfigPrepareTask struct {
	task.Base
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.Result
	Status      string
	Id          string
	service     *db.PluginService
	vars        map[string]stick.Value
}

func NewPluginConfigPrepareTask(e *db.Environment, p *db.PluginService, vars map[string]stick.Value) *ConfigPrepareTask {
	t := ConfigPrepareTask{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	t.service = p
	t.vars = vars
	return &t
}

func (task *ConfigPrepareTask) GetLogVars() map[string]string{
	m := map[string]string{
		"pluginService" : task.service.Name,
		"pluginServiceDescription" : task.service.Description,
	}
	return m
}


func (task *ConfigPrepareTask) GetResult() *task.Result {
	task.wg.Wait()
	return &task.result
}

func (task *ConfigPrepareTask) GetProgress() string {
	return task.Status
}

func (task *ConfigPrepareTask) GetEventPrefix() string {
	return "plugin_configs"
}

func (task *ConfigPrepareTask) GetName() string {
	return "Write Config Files"
}

func (task *ConfigPrepareTask) GetEnvironment() *db.Environment {
	return task.Environment
}

func (task *ConfigPrepareTask) GetDescription() string {
	return "Write Templates/Config Files for Service " + task.service.Name
}

func (task *ConfigPrepareTask) GetId() string {
	return task.Id
}

func (task *ConfigPrepareTask) Run() {

	for key, val := range task.vars {
		task.vars[strings.ToUpper(key)] = val
	}

	for _, t := range task.service.Templates {
		url := "https://gitlab.com/tuliptools/TplusPlugins/-/raw/master" + t.Source
		res, err := http.Get(url)
		if err != nil {
			fmt.Println(err.Error())
		}
		b, _ := ioutil.ReadAll(res.Body)
		res.Body.Close()
		stickEnv := stick.New(nil)
		filebuffer := bytes.Buffer{}
		if err := stickEnv.Execute(string(b), &filebuffer, task.vars); err == nil {
			ioutil.WriteFile(t.HostFile, filebuffer.Bytes(), 0777)
		} else {
			fmt.Println(err.Error())
		}
	}

	task.result.Success = true
	task.result.Message = "Containers prepared"
	task.wg.Done()
}
