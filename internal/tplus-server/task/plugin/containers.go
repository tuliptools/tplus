package plugin

import (
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/service/docker"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"strings"
	"sync"
)

type ContainerPrepareTask struct {
	task.Base
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.Result
	Status      string
	Id          string
	plugin      *db.PluginService
}

func NewPluginContainerPrepareTask(e *db.Environment, p *db.PluginService) *ContainerPrepareTask {
	t := ContainerPrepareTask{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	t.plugin = p
	return &t
}

func (task *ContainerPrepareTask) GetLogVars() map[string]string{
	m := map[string]string{
		"pluginService" : task.plugin.Name,
		"pluginServiceDescription" : task.plugin.Description,
	}
	return m
}


func (task *ContainerPrepareTask) GetResult() *task.Result {
	task.wg.Wait()
	return &task.result
}

func (task *ContainerPrepareTask) GetProgress() string {
	return task.Status
}

func (task *ContainerPrepareTask) GetEventPrefix() string {
	return "plugin_containers"
}

func (task *ContainerPrepareTask) GetName() string {
	return "Create containers for Plugin"
}

func (task *ContainerPrepareTask) GetEnvironment() *db.Environment {
	return task.Environment
}

func (task *ContainerPrepareTask) GetDescription() string {
	return "Preparing container for Plugin"
}

func (task *ContainerPrepareTask) GetId() string {
	return task.Id
}

func (task *ContainerPrepareTask) Run() {
	envID := strings.Replace(task.Environment.Id, "env-", "", -1)
	envID = strings.ToLower(envID)
	sid := strings.ToLower(task.plugin.Name)
	sid = strings.Replace(sid, " ", "", -1)
	containerName := "tplus_plugin_" + envID + "_" + sid

	id, _ := task.Docker.GetContainerId(containerName)
	info, err := task.Docker.GetContainerInfo(id)
	if err != nil || info.ID == "" {
		template := docker.GetPluginService(task.Environment, task.plugin)
		template.ContainerName = containerName
		task.Docker.ContainerCreate(template)
	}

	task.plugin.ContainerId = id
	task.result.Success = true
	task.result.Message = "Containers prepared"
	task.wg.Done()
}
