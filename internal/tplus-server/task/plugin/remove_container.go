package plugin

import (
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"strings"
	"sync"
)

type ContainerRemoveTask struct {
	task.Base
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.Result
	Status      string
	Id          string
	plugin      *db.PluginService
}

func NewPluginContainerRemoveTask(e *db.Environment, p *db.PluginService) *ContainerRemoveTask {
	t := ContainerRemoveTask{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	t.plugin = p
	return &t
}

func (task *ContainerRemoveTask) GetLogVars() map[string]string{
	m := map[string]string{
		"pluginService" : task.plugin.Name,
		"pluginServiceDescription" : task.plugin.Description,
		"containerId" : task.plugin.ContainerId,
	}
	return m
}


func (task *ContainerRemoveTask) GetResult() *task.Result {
	task.wg.Wait()
	return &task.result
}

func (task *ContainerRemoveTask) GetProgress() string {
	return task.Status
}

func (task *ContainerRemoveTask) GetEventPrefix() string {
	return "plugin_remove"
}

func (task *ContainerRemoveTask) GetName() string {
	return "Remove Container"
}

func (task *ContainerRemoveTask) GetEnvironment() *db.Environment {
	return task.Environment
}

func (task *ContainerRemoveTask) GetDescription() string {
	return "remove docker container"
}

func (task *ContainerRemoveTask) GetId() string {
	return task.Id
}

func (task *ContainerRemoveTask) Run() {

	envID := strings.Replace(task.Environment.Id, "env-", "", -1)
	envID = strings.ToLower(envID)
	sid := strings.ToLower(task.plugin.Name)
	sid = strings.Replace(sid, " ", "", -1)
	containerName := "tplus_plugin_" + envID + "_" + sid

	id, _ := task.Docker.GetContainerId(containerName)
	task.Docker.ContainerStop(id)
	task.Docker.ContainerRM(id)

	task.result.Success = true
	task.result.Message = "Containers stoped"
	task.wg.Done()
}
