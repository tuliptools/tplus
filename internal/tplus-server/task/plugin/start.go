package plugin

import (
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"strings"
	"sync"
)

type ContainerStartTask struct {
	task.Base
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.Result
	Status      string
	Id          string
	plugin      *db.PluginService
}

func NewPluginContainerStartTask(e *db.Environment, p *db.PluginService) *ContainerStartTask {
	t := ContainerStartTask{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	t.plugin = p
	return &t
}

func (task *ContainerStartTask) GetLogVars() map[string]string{
	m := map[string]string{
		"pluginService" : task.plugin.Name,
		"pluginServiceDescription" : task.plugin.Description,
		"containerId" : task.plugin.ContainerId,
	}
	return m
}


func (task *ContainerStartTask) GetResult() *task.Result {
	task.wg.Wait()
	return &task.result
}

func (task *ContainerStartTask) GetProgress() string {
	return task.Status
}

func (task *ContainerStartTask) GetEventPrefix() string {
	return "plugin_start"
}

func (task *ContainerStartTask) GetName() string {
	return "Start Container"
}

func (task *ContainerStartTask) GetEnvironment() *db.Environment {
	return task.Environment
}

func (task *ContainerStartTask) GetDescription() string {
	return "Starting docker container"
}

func (task *ContainerStartTask) GetId() string {
	return task.Id
}

func (task *ContainerStartTask) Run() {

	envID := strings.Replace(task.Environment.Id, "env-", "", -1)
	envID = strings.ToLower(envID)
	sid := strings.ToLower(task.plugin.Name)
	sid = strings.Replace(sid, " ", "", -1)
	containerName := "tplus_plugin_" + envID + "_" + sid

	id, _ := task.Docker.GetContainerId(containerName)
	task.plugin.ContainerId = id
	info, err := task.Docker.GetContainerInfo(id)

	if err == nil && info.State.Running == false {
		task.Docker.ContainerStart(info.ID)
	}

	task.plugin.Status = "running"
	task.result.Success = true
	task.result.Message = "Containers started"
	task.wg.Done()
}
