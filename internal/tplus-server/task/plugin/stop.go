package plugin

import (
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"strings"
	"sync"
)

type ContainerStopTask struct {
	task.Base
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.Result
	Status      string
	Id          string
	plugin      *db.PluginService
}

func NewPluginContainerStopTask(e *db.Environment, p *db.PluginService) *ContainerStopTask {
	t := ContainerStopTask{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	t.plugin = p
	return &t
}

func (task *ContainerStopTask) GetLogVars() map[string]string{
	m := map[string]string{
		"pluginService" : task.plugin.Name,
		"pluginServiceDescription" : task.plugin.Description,
		"containerId" : task.plugin.ContainerId,
	}
	return m
}


func (task *ContainerStopTask) GetResult() *task.Result {
	task.wg.Wait()
	return &task.result
}

func (task *ContainerStopTask) GetProgress() string {
	return task.Status
}

func (task *ContainerStopTask) GetEventPrefix() string {
	return "plugin_stop"
}

func (task *ContainerStopTask) GetName() string {
	return "Stop Container"
}

func (task *ContainerStopTask) GetEnvironment() *db.Environment {
	return task.Environment
}

func (task *ContainerStopTask) GetDescription() string {
	return "Stopping docker container"
}

func (task *ContainerStopTask) GetId() string {
	return task.Id
}

func (task *ContainerStopTask) Run() {

	envID := strings.Replace(task.Environment.Id, "env-", "", -1)
	envID = strings.ToLower(envID)
	sid := strings.ToLower(task.plugin.Name)
	sid = strings.Replace(sid, " ", "", -1)
	containerName := "tplus_plugin_" + envID + "_" + sid

	id, _ := task.Docker.GetContainerId(containerName)
	task.Docker.ContainerStop(id)

	task.result.Success = true
	task.result.Message = "Containers stopped"
	task.wg.Done()
}
