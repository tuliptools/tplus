package plugin

import (
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"os"
	"sync"
)

type StoragePrepareTask struct {
	task.Base
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.Result
	Status      string
	Id          string
	service     *db.PluginService
}

func NewPluginStoragePrepareTask(e *db.Environment, p *db.PluginService) *StoragePrepareTask {
	t := StoragePrepareTask{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	t.service = p
	return &t
}

func (task *StoragePrepareTask) GetLogVars() map[string]string{
	m := map[string]string{
		"pluginService" : task.service.Name,
		"pluginServiceDescription" : task.service.Description,
	}
	return m
}


func (task *StoragePrepareTask) GetResult() *task.Result {
	task.wg.Wait()
	return &task.result
}

func (task *StoragePrepareTask) GetProgress() string {
	return task.Status
}

func (task *StoragePrepareTask) GetEventPrefix() string {
	return "plugin_storage"
}

func (task *StoragePrepareTask) GetName() string {
	return "Storage Prepare"
}

func (task *StoragePrepareTask) GetEnvironment() *db.Environment {
	return task.Environment
}

func (task *StoragePrepareTask) GetDescription() string {
	return "Preparing filesystem for Plugin"
}

func (task *StoragePrepareTask) GetId() string {
	return task.Id
}

func (task *StoragePrepareTask) Run() {
	basedir := task.Config.DataDir + "/envs/" + task.Environment.Id
	basepPluginDir := basedir + "/plugins"
	basepPluginDir2 := basedir + "/plugins/" + task.service.Name
	configs := basedir + "/plugins/" + task.service.Name + "/configs"

	dirs := []string{basedir, basepPluginDir, basepPluginDir2, configs}

	for _, v := range task.service.Volumes {
		dirs = append(dirs, basepPluginDir2+"/"+v.Name)
	}

	for _, dir := range dirs {
		e := os.MkdirAll(dir, 0777)
		if e == nil {
			task.result.Success = true
			task.result.Message = "Filesystem prepared"
		} else {
			task.result.Success = false
			task.result.Message = "Failed with: " + e.Error()
			task.wg.Done()
			return
		}
	}

	task.wg.Done()
}
