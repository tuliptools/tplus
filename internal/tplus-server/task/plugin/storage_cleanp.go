package plugin

import (
	"github.com/docker/docker/api/types/mount"
	"github.com/docker/docker/api/types/strslice"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"os"
	"path/filepath"
	"sync"
)

type StorageDeleteTask struct {
	task.Base
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.Result
	Status      string
	Id          string
	service     *db.PluginService
}

func NewPluginStorageDeleteTask(e *db.Environment, p *db.PluginService) *StorageDeleteTask {
	t := StorageDeleteTask{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	t.service = p
	return &t
}

func (task *StorageDeleteTask) GetLogVars() map[string]string{
	m := map[string]string{
		"pluginService" : task.service.Name,
		"pluginServiceDescription" : task.service.Description,
	}
	return m
}


func (task *StorageDeleteTask) GetResult() *task.Result {
	task.wg.Wait()
	return &task.result
}

func (task *StorageDeleteTask) GetProgress() string {
	return task.Status
}

func (task *StorageDeleteTask) GetEventPrefix() string {
	return "plugin_storage_remove"
}

func (task *StorageDeleteTask) GetName() string {
	return "Storage Delete"
}

func (task *StorageDeleteTask) GetEnvironment() *db.Environment {
	return task.Environment
}

func (task *StorageDeleteTask) GetDescription() string {
	return "Removing all files Plugin"
}

func (task *StorageDeleteTask) GetId() string {
	return task.Id
}

func (task *StorageDeleteTask) Run() {
	basedir := task.Config.DataDir + "/envs/" + task.Environment.Id
	basepPluginDir2 := basedir + "/plugins/" + task.service.Name

	dirs := []string{basepPluginDir2}

	task.Docker.PullImageBlocking("docker.io/library/alpine:latest")
	for _, dir := range dirs {
		_, _ = task.Docker.RunTask(
			"env_cleanup_"+task.Environment.Id,
			"docker.io/library/alpine:latest",
			task.Environment.Tezos.Container.NetworkID,
			strslice.StrSlice{"/bin/rm"},
			strslice.StrSlice{"-r", "/rmdata"},
			[]mount.Mount{
				{
					Type:   mount.TypeBind,
					Source: dir,
					Target: "/rmdata",
				},
			},
		)
		RemoveDir(dir)
	}

	task.result.Success = true
	task.result.Message = "Cleared all Data"
	task.wg.Done()
}

func RemoveDir(dir string) error {
	d, err := os.Open(dir)
	if err != nil {
		return err
	}
	defer d.Close()
	names, err := d.Readdirnames(-1)
	if err != nil {
		return err
	}
	for _, name := range names {
		err = os.RemoveAll(filepath.Join(dir, name))
		if err != nil {
			return err
		}
	}
	err = os.RemoveAll(dir)
	return err
}
