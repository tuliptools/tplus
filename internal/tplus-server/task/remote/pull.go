package remote

import (
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/version"
	"sync"
	"time"
)

type ImagePullTask struct {
	task.Base
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.Result
	Status      string
	Id          string
}

func NewRemoteImagePullTask(e *db.Environment) *ImagePullTask {
	t := ImagePullTask{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	return &t
}

func (task *ImagePullTask) GetResult() *task.Result {
	task.wg.Wait()
	return &task.result
}

func (task *ImagePullTask) GetProgress() string {
	return task.Status
}

func (task *ImagePullTask) GetEventPrefix() string {
	return "node_pull_image"
}

func (task *ImagePullTask) GetName() string {
	return "Pull docker Image"
}

func (task *ImagePullTask) GetEnvironment() *db.Environment {
	return task.Environment
}

func (task *ImagePullTask) GetDescription() string {
	return "Pulling latest Image for tezos " + task.Environment.Tezos.Branch
}

func (task *ImagePullTask) GetId() string {
	return task.Id
}

func (task *ImagePullTask) Run() {
	task.Status = "in Progress"
	start := time.Now()
	var err error
	images := []string{version.GetVersionInfo().NodeProxyImage,version.GetVersionInfo().TezosImage}
	tries := 0
	var logs []byte
	for _, image := range images {
		for {
			logs, err = task.Docker.PullImageBlocking(image)
			if err == nil {
				break
			}
			tries++
			if tries >= 5 {
				break
			}
			time.Sleep(5 * time.Second)
		}
	}
	done := time.Now()

	task.result.Logs = map[string]string{
		"Pull Log": string(logs),
	}

	if err == nil {
		task.result.Success = true
		task.result.Message = "Image Pulled within " + done.Sub(start).String()
	} else {
		task.result.Success = false
		task.result.Message = "Error: " + err.Error()
	}
	task.wg.Done()
}
