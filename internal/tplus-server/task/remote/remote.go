package remote

// dir := task.Config.DataDir + "/envs/" + task.Environment.Id

import (
	"encoding/json"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/service/docker"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"sync"
	"time"
)

type ContainerCheckTask struct {
	task.Base
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.Result
	Status      string
	Id          string
}

func NewContainerCheckTask(e *db.Environment) *ContainerCheckTask {
	t := ContainerCheckTask{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	return &t
}

func (task *ContainerCheckTask) GetResult() *task.Result {
	task.wg.Wait()
	return &task.result
}

func (task *ContainerCheckTask) GetProgress() string {
	return task.Status
}

func (task *ContainerCheckTask) GetEventPrefix() string {
	return "remote_container_check"
}

func (task *ContainerCheckTask) GetName() string {
	return "Container Check"
}

func (task *ContainerCheckTask) GetEnvironment() *db.Environment {
	return task.Environment
}

func (task *ContainerCheckTask) GetDescription() string {
	return "Check if container file exists"
}

func (task *ContainerCheckTask) GetId() string {
	return task.Id
}

func (task *ContainerCheckTask) Run() {
	if task.Environment.Tezos.Container.Status == "" || task.Environment.Tezos.Container.Id == "" || task.Environment.Tezos.Container.Name == "" {
		task.result.Success = false
		task.result.Message = "Container does not exists"
		task.wg.Done()
		return
	} else {
		id, e := task.Docker.GetContainerId(task.Environment.Tezos.Container.Name)
		if e != nil || len(id) <= 4 {
			task.result.Success = false
			task.result.Message = "Container does not exists"
			task.wg.Done()
			return
		}
	}
	task.result.Success = true
	task.result.Message = "Container exists"
	task.wg.Done()
	return
}

//--------------------------------------------------------------------------------------------------------------------//

type ContainerCreateTask struct {
	task.Base
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.Result
	Status      string
	Id          string
}

func NewContainerCreateTask(e *db.Environment) *ContainerCreateTask {
	t := ContainerCreateTask{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	return &t
}

func (task *ContainerCreateTask) GetResult() *task.Result {
	task.wg.Wait()
	return &task.result
}

func (task *ContainerCreateTask) GetProgress() string {
	return task.Status
}

func (task *ContainerCreateTask) GetEventPrefix() string {
	return "remote_container_create"
}

func (task *ContainerCreateTask) GetName() string {
	return "Container create"
}

func (task *ContainerCreateTask) GetEnvironment() *db.Environment {
	return task.Environment
}

func (task *ContainerCreateTask) GetDescription() string {
	return "Creating docker container"
}

func (task *ContainerCreateTask) GetId() string {
	return task.Id
}

func (task *ContainerCreateTask) Run() {

	if task.Environment.Tezos.Container.Name == "" {
		task.Environment.Tezos.Container.Name = "tplus_proxy_" + task.Environment.Tezos.Branch + "_" + db.GetRandomString(6)
		task.SaveChan <- task.Environment
	}

	template := docker.GetProxyTemplate(task.Environment)

	id, err := task.Docker.ContainerCreate(template)

	if err != nil {
		task.result.Success = false
		task.result.Message = err.Error()
		task.wg.Done()
		return
	}

	task.Environment.Tezos.Container.Id = id
	task.SaveChan <- task.Environment

	time.Sleep(2 * time.Second)

	infoLogs, _ := task.Docker.GetContainerInfo(id)

	b, _ := json.MarshalIndent(infoLogs, "", "    ")

	task.result.Logs = map[string]string{
		"Docker Inspect": string(b),
	}

	task.result.Success = true
	task.result.Message = "Container created"
	task.wg.Done()
	return
}

//--------------------------------------------------------------------------------------------------------------------//

type ContainerStartTask struct {
	task.Base
	Environment *db.Environment
	wg          *sync.WaitGroup
	result      task.Result
	Status      string
	Id          string
}

func NewContainerStartTask(e *db.Environment) *ContainerStartTask {
	t := ContainerStartTask{}
	wait := sync.WaitGroup{}
	t.wg = &wait
	t.wg.Add(1)
	t.Status = "pending"
	t.Id = db.GetRandomId("task")
	t.SaveChan = make(chan db.Model)
	t.Environment = e
	return &t
}

func (task *ContainerStartTask) GetResult() *task.Result {
	task.wg.Wait()
	return &task.result
}

func (task *ContainerStartTask) GetProgress() string {
	return task.Status
}

func (task *ContainerStartTask) GetEventPrefix() string {
	return "node_container_start"
}

func (task *ContainerStartTask) GetName() string {
	return "Container Start"
}

func (task *ContainerStartTask) GetEnvironment() *db.Environment {
	return task.Environment
}

func (task *ContainerStartTask) GetDescription() string {
	return "Start docker container"
}

func (task *ContainerStartTask) GetId() string {
	return task.Id
}

func (task *ContainerStartTask) Run() {
	if task.Environment.Consoles == nil {
		task.Environment.Consoles = map[string]db.Container{}
	}

	id := task.Environment.Tezos.Container.Id

	info, err := task.Docker.GetContainerInfo(id)
	if err != nil {
		task.result.Success = false
		task.result.Message = err.Error()
		task.wg.Done()
		return
	}

	if info.State.Status == "running" {
		infoLogs, _ := task.Docker.GetContainerInfo(id)
		task.Environment.Tezos.Container.Ips = []string{}
		for _, n := range infoLogs.NetworkSettings.Networks {
			task.Environment.Tezos.Container.Ips = append(task.Environment.Tezos.Container.Ips, n.IPAddress)
		}
		task.SaveChan <- task.Environment

		task.result.Success = true
		task.result.Message = "Container already running"
		task.wg.Done()
		return
	}
	err = task.Docker.ContainerStart(id)

	if err != nil {
		task.result.Success = false
		task.result.Message = err.Error()
		task.wg.Done()
		return
	}

	task.Environment.Tezos.Status = "running"
	task.Environment.Tezos.Container.Status = "running"
	task.SaveChan <- task.Environment

	time.Sleep(200 * time.Millisecond)

	infoLogs, _ := task.Docker.GetContainerInfo(id)

	b, _ := json.MarshalIndent(infoLogs, "", "    ")

	task.result.Logs = map[string]string{
		"Docker Inspect": string(b),
	}

	task.Environment.Tezos.Container.Ips = []string{}
	for _, n := range infoLogs.NetworkSettings.Networks {
		task.Environment.Tezos.Container.Ips = append(task.Environment.Tezos.Container.Ips, n.IPAddress)
	}

	task.SaveChan <- task.Environment
	task.result.Success = true
	task.result.Message = "Container started"
	task.wg.Done()
	return
}
