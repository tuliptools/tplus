package task

import (
	"github.com/gobuffalo/packr"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/config"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/service"
	"time"
)

type Base struct {
	Next      string
	OnFail    string
	OnSuccess string
	SaveChan  chan db.Model
	Docker    *service.DockerService
	Config    *config.Config
	Box       *packr.Box
}

func (task *Base) SetDocker(d *service.DockerService) {
	task.Docker = d
}

func (task *Base) SetConfig(d *config.Config) {
	task.Config = d
}

func (task *Base) GetNext() string {
	return task.Next
}

func (task *Base) SetNext(t Task) {
	task.Next = t.GetId()
}

func (task *Base) SetBox(p *packr.Box) {
	task.Box = p
}

func (task *Base) SetOnFailure(t Task) {
	task.OnFail = t.GetId()
}

func (task *Base) SetOnSuccess(t Task) {
	task.OnSuccess = t.GetId()
}

func (task *Base) GetOnFail() string {
	return task.OnFail
}

func (task *Base) GetOnSuccess() string {
	return task.OnSuccess
}

func (task *Base) GetSaveChan() chan db.Model {
	return task.SaveChan
}

func (task *Base) GetLogVars() map[string]string{
	m := map[string]string{}
	return m
}


type Task interface {
	GetResult() *Result // blocks until task is done
	GetProgress() string
	GetName() string
	GetEnvironment() *db.Environment
	GetDescription() string
	Run()
	GetId() string
	GetEventPrefix() string
	GetNext() string
	GetOnFail() string
	GetOnSuccess() string
	GetSaveChan() chan db.Model
	GetLogVars() map[string]string

	SetDocker(d *service.DockerService)
	SetOnSuccess(t Task)
	SetOnFailure(t Task)
	SetBox(p *packr.Box)
	SetConfig(d *config.Config)
}

type Result struct {
	Success  bool
	NoLog    bool
	Message  string
	duration time.Duration
	Finished time.Time
	Logs     map[string]string
}

type Group struct {
	Tasks []Task
}
