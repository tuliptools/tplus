package tezcon

import (
	"encoding/json"
	gotezos "github.com/DefinitelyNotAGoat/go-tezos"
	"github.com/pkg/errors"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"net/http"
	"reflect"
	"sort"
)

type TezosClient struct {
	*http.Client
	BaseURL string
}

type TezosConnector struct {
	db      *db.DB
	clients map[string]*gotezos.GoTezos
}

func NewTezosConnector(dbw *db.DB) *TezosConnector {
	t := TezosConnector{}
	t.db = dbw
	t.clients = map[string]*gotezos.GoTezos{}
	return &t
}

func (tc *TezosConnector) GetClient(envid string) (*gotezos.GoTezos, error) {
	if val, ok := tc.clients[envid]; ok {
		return val, nil
	}
	e, err := tc.db.GetEnvironmentById(envid)
	if err != nil {
		return nil, err
	}

	if len(e.Tezos.Container.Ips) == 0 {
		return nil, errors.New("NO IP for container")
	}

	ngt, err := gotezos.NewGoTezos("http://"+e.Tezos.Container.Ips[0]+":8732", "nowthisworks")
	if err != nil {
		return nil, err
	}
	tc.clients[envid] = ngt

	return ngt, nil

}

func (tc *TezosConnector) GetPeers(envid string) ([]Peer, error) {
	var res []Peer
	gt, e := tc.GetClient(envid)
	if e != nil {
		return res, e
	}
	b, err := gt.Get("/network/peers", map[string]string{})
	if err != nil {
		return res, nil
	}
	var jsonres [][]interface{}
	json.Unmarshal(b, &jsonres)

	id := ""
	for _, a := range jsonres {
		for _, b := range a {
			if reflect.TypeOf(b).String() == "string" {
				id = b.(string)
			} else {
				raw, _ := json.Marshal(b)
				p := Peer{}
				json.Unmarshal(raw, &p)
				p.Id = id
				res = append(res, p)
			}
		}
	}

	sort.Slice(res, func(i, j int) bool {
		return res[i].Stat.TotalRecv >= res[j].Stat.TotalRecv
	})

	return res, nil
}
