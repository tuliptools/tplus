package version

type VersionInfo struct {
	Version              string
	WebImage             string
	WebImageDigest       string
	TezosImage           string
	TezosImageDigest     string
	NodeProxyImage       string
	NodeProxyImageDigest string
	ProxyImage           string
	ProxyImageDigest     string
}

func GetVersionInfo() VersionInfo {
	// digest = config digest in gitlab
	return VersionInfo{
		Version:              "next",
		WebImage:             "registry.gitlab.com/tuliptools/tplusgui:next",
		WebImageDigest:       "sha256:459f9623905de586aa04559a8dbbe2de18d42aed70c1fca952308ea2b30a423f",
		TezosImage:           "registry.gitlab.com/tuliptools/tplus:tezos-v7.3",
		TezosImageDigest:     "sha256:3fb0b099a68a1f5546286cd6194177c5629d2d26ce70933f60f9c2ecf5db6df6",
		NodeProxyImage:       "registry.gitlab.com/tuliptools/tezproxy:latest",
		NodeProxyImageDigest: "sha256:8fd7e760afc3d7c15b30f38945d10ca9fa85247a72b64acbab78ca8f7fb9cbe2",
		ProxyImage:           "registry.gitlab.com/tuliptools/tplusvmproxy:latest",
		ProxyImageDigest:     "sha256:bd2d13b788f4ff2fef552bb77ed51e9c28889c5c656e9c7d360cdb3593e6fbeb",
	}
}
