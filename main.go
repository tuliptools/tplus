package main

import (
	"fmt"
	"github.com/gobuffalo/packr"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/tuliptools/tplus/cmd"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/api"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/config"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/db"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/events"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/http"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/manager"
	manager2 "gitlab.com/tuliptools/tplus/internal/tplus-server/metrics"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/service"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/task"
	"gitlab.com/tuliptools/tplus/internal/tplus-server/tezcon"
	"go.uber.org/dig"
	"os"
	"syscall"
)

func main() {
	syscall.Umask(0)

	c := dig.New()

	var rootCmd = &cobra.Command{
		Use:   "tplus-server",
		Short: "Tplus Server",
	}

	var run = &cobra.Command{
		Use:   "run",
		Short: "run Tplus Server",
		Run: func(cmd *cobra.Command, args []string) {
			e := c.Invoke(func(api *api.Api) {
				api.Run()
			})
			fmt.Println(e)
		},
	}

	mustnil := func(err error) {
		if err != nil {
			fmt.Println(err)
		}
	}

	mustnil(c.Provide(func() *packr.Box {
		box := packr.NewBox("./data")
		return &box
	}))

	mustnil(c.Provide(func() *logrus.Logger {
		l := logrus.New()
		l.SetFormatter(&logrus.TextFormatter{
			FullTimestamp: true,
		})
		l.Level = logrus.InfoLevel
		return l
	}))
	mustnil(c.Provide(events.NewEvents))
	mustnil(c.Provide(events.NewTezosWatcherFactory))
	mustnil(c.Provide(events.NewLogListenerFactory))
	mustnil(c.Provide(http.GetClient))
	mustnil(c.Provide(config.ReadConfig))
	mustnil(c.Provide(db.NewDB))
	mustnil(c.Provide(db.NewBoltDb))
	mustnil(c.Provide(api.NewApi))
	mustnil(c.Provide(service.NewUserService))
	mustnil(c.Provide(manager.NewProjectServiceManager))
	mustnil(c.Provide(service.NewDockerService))
	mustnil(c.Provide(task.NewTaskManager))
	mustnil(c.Provide(manager.NewEnvManager))
	mustnil(c.Provide(manager2.NewMetrics))
	mustnil(c.Provide(tezcon.NewTezosConnector))
	mustnil(c.Provide(manager.NewProjectManager))
	mustnil(c.Provide(manager.NewEndpointManager))
	mustnil(c.Provide(manager.NewBashManager))
	mustnil(c.Provide(manager.NewWalletManager))
	mustnil(c.Provide(manager.NewPluginsManager))

	cmd.UserCommands(c, rootCmd)
	cmd.ConfigCommands(c, rootCmd)
	cmd.EnvCommands(c, rootCmd)
	cmd.VersionCommands(c,rootCmd)
	cmd.UpdateCommands(c,rootCmd)
	rootCmd.AddCommand(run)

	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

}