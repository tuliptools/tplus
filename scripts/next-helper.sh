#!/bin/bash

FILE=~/.tplus/server.yml


update_binaries() {
  if [[ "$OSTYPE" == "linux-gnu"* ]]; then
          update_binaries_linux
  elif [[ "$OSTYPE" == "darwin"* ]]; then
          update_binaries_mac
  elif [[ "$OSTYPE" == "cygwin" ]]; then
          echo "OS not supported with this script, check docs"
  elif [[ "$OSTYPE" == "msys" ]]; then
          echo "OS not supported with this script, check docs"
  elif [[ "$OSTYPE" == "win32" ]]; then
          echo "OS not supported with this script, check docs"
  elif [[ "$OSTYPE" == "freebsd"* ]]; then
          echo "OS not supported with this script, check docs"
  else
          echo "OS not supported with this script, check docs"
  fi
}


update_binaries_linux(){
   ## download binaries
  echo "Downloading Tplus binaries....."
  sudo wget https://files.tplus.dev/tplus-server-next -O /usr/local/bin/tplus-server
  sudo wget https://files.tplus.dev/tplus-next -O /usr/local/bin/tplus
  sudo sudo chmod +x /usr/local/bin/tplus
  sudo sudo chmod +x /usr/local/bin/tplus-server
}

update_binaries_mac(){
   ## download binaries
  echo "Downloading Tplus binaries....."
  sudo wget https://files.tplus.dev/mac/tplus-server-next -O /usr/local/bin/tplus-server
  sudo wget https://files.tplus.dev/mac/tplus-next -O /usr/local/bin/tplus
  sudo sudo chmod +x /usr/local/bin/tplus
  sudo sudo chmod +x /usr/local/bin/tplus-server
}

first_install(){
  tplus-server config init --port 8558 --userauth=no
  echo "Starting tplus-server to set up configuration, please dont close this window"
  docker pull registry.gitlab.com/tuliptools/tplusvmproxy:latest
  tplus-server run &
  sleep 15
  echo "Configuring tplus cli tool"
  tplus server add --apiUrl http://localhost:8558 --name localhost
  tplus server use localhost
  tplus server ls
  killall tplus-server
}

install(){
 if [ -x "$(command -v docker)" ]; then
    update_binaries

    if [ -f "$FILE" ]; then
        echo "Found a tplus config file, skipping init"
    else
        echo "Creating initial configs for tplus...."
        first_install

    fi
  else
      echo "Please make sure you have docker installed"
      echo "See: https://docs.docker.com/get-docker/ "
  fi
}

clean_install() {
  remove
  install
}

clean_docker(){
  docker rm -f $(docker ps --filter label=created_by=tplus -a -q)
}

remove(){
  echo "Stopping tplus-server..."
  killall tplus 2>/dev/null
  killall tplus-server 2>/dev/null
  echo "Remove binaries..."
  sudo rm -r ~/.tplus 2>/dev/null
  sudo rm /usr/local/bin/tplus 2>/dev/null
  sudo rm /usr/local/bin/tplus-server 2>/dev/null
  echo "Removing docker containers, if any...."
  clean_docker 2>/dev/null
  echo "Done."
}

precheck(){
  if ! command -v wget &> /dev/null
  then
      echo "make sure wget is installed"
      if [[ "$OSTYPE" == "darwin"* ]]; then
              echo "with: brew install wget - for example"
      else
              echo "with: apt-get install wget - for example"
      fi
      exit
  fi
}

if [ $# -eq 0 ]
  then
    echo "No arguments supplied, run $0 <cmd>"
    echo "Available Commands:"
    echo " install - Installs Tplus locally"
    echo " remove  - Removes al traces of Tplus"
    echo " clean_docker - Removes Tplus docker containers"
    echo " clean_install - Reinstall Tplus, remove existing resources"
    echo " update_binaries - Only update tplus and tplus-server binaries"
fi


precheck

$1
