﻿

$ErrorActionPreference = 'SilentlyContinue'

function Check-ProgramInstalled($Name)
{
    $Apps = @()

    Get-ChildItem -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall" | %{
        $Properties = $_ | Get-ItemProperty
        if($Properties)
        {
            $item = New-Object psobject
            $item | Add-Member -MemberType NoteProperty -Name "Name" -Value ($_ | Get-ItemProperty).DisplayName
            $item | Add-Member -MemberType NoteProperty -Name "Version" -Value ($_ | Get-ItemProperty).DisplayVersion
            $item | Add-Member -MemberType NoteProperty -Name "Publisher" -Value ($_ | Get-ItemProperty).Publisher
            $item | Add-Member -MemberType NoteProperty -Name "UninstallString" -Value ($_ | Get-ItemProperty).UninstallString
            if($item.Name)
            {
                $Apps += $item
            }
        }
    }

    # For windows 32 bit compatible
    Get-ChildItem -Path "HKLM:\SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\Uninstall" | %{
        $Properties = $_ | Get-ItemProperty
        if($Properties)
        {
            $item = New-Object psobject
            $item | Add-Member -MemberType NoteProperty -Name "Name" -Value ($_ | Get-ItemProperty).DisplayName
            $item | Add-Member -MemberType NoteProperty -Name "Version" -Value ($_ | Get-ItemProperty).DisplayVersion
            $item | Add-Member -MemberType NoteProperty -Name "Publisher" -Value ($_ | Get-ItemProperty).Publisher
            $item | Add-Member -MemberType NoteProperty -Name "UninstallString" -Value ($_ | Get-ItemProperty).UninstallString
            if($item.Name)
            {
                $Apps += $item
            }
        }
    }

    if($Apps | ?{$_.Name -match $Name})
    {
        return $true
    }
    return $false
}

function Download-File($URL, $Destination, $FileName)
{
    if(!(Test-Path "$Destination\$FileName"))
    {
        try
        {
            Write-Host -ForegroundColor Yellow "Downloading $FileName"
            Start-BitsTransfer -Source $URL -Destination "$Destination\$FileName" -ErrorAction Stop
            Write-Host -ForegroundColor Green "$FileName downloaded"
        }
        catch
        {
            Write-Host -ForegroundColor Red "Error downloading from $URL"
        }
    }
    else
    {
        Write-Host "$FileName is already downloaded"
    }
}

function Install-VirtualBox($FilePath)
{
    Write-Host -ForegroundColor Cyan "Installing VirtualBox..."
    try
    {
        Start-Process -FilePath $FilePath -Wait -ArgumentList "--silent" -ErrorAction Stop
        Write-Host -ForegroundColor Green "Installed successfully!"
    }
    catch
    {
        Write-Host -ForegroundColor Red "Error installing VirtualBox"
    }
}

function Install-Vagrant($FilePath)
{
    Write-Host -ForegroundColor Cyan "Installing Vagrant..."
    try
    {
        Start-Process -FilePath "msiexec" -Wait -ArgumentList "/i $FilePath /qn /norestart" -ErrorAction Stop
        Write-Host -ForegroundColor Green "Installed successfully!"
        Write-Host -BackgroundColor Yellow "A restart is required to finish installation"
    }
    catch
    {
        Write-Host -ForegroundColor Red "Error installing Vagrant"
    }
    Write-Host ""
}

function Check-AdminRights()
{
    if (!(([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)))
    {
        Write-Host -ForegroundColor Yellow "Please run the script as administrator"
        Read-Host "Press any key to continue..."
        break
    }
}

# VARIABLES
$BinaryFileURL = "https://files.tplus.dev/tplus.exe"
$VirtualBoxURL = "https://download.virtualbox.org/virtualbox/6.0.22/VirtualBox-6.0.22-137980-Win.exe"
$VagrantURL = "https://releases.hashicorp.com/vagrant/2.2.9/vagrant_2.2.9_x86_64.msi"
$VagrantFileName = "vagrant_2.2.9_x86_64.msi"
$VirtualBoxFileName = "VirtualBox-6.0.22-137980-Win.exe"
$DownloadsFolder = "C:\Installs"

Check-AdminRights

New-Item -ItemType Directory -Path "C:\Installs"


if(!(Check-ProgramInstalled -Name "VirtualBox"))
{
    Download-File -URL $VirtualBoxURL -Destination $DownloadsFolder -FileName $VirtualBoxFileName
    Install-VirtualBox -FilePath "$DownloadsFolder\$VirtualBoxFileName"
}

if(!(Check-ProgramInstalled -Name "Vagrant"))
{
    Download-File -URL $VagrantURL -Destination $DownloadsFolder -FileName  $VagrantFileName
    Install-Vagrant -FilePath "$DownloadsFolder\$VagrantFileName"
}


Download-File -URL $BinaryFileURL -Destination "C:\Windows\System32" -FileName "tplus.exe"

Read-Host "Press any key to continue..."
