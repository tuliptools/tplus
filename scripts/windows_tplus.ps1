
$ErrorActionPreference = 'SilentlyContinue'

function Download-File($URL, $Destination, $FileName)
{
    if(!(Test-Path "$Destination\$FileName"))
    {
        try
        {
            Write-Host -ForegroundColor Yellow "Downloading $FileName"
            Start-BitsTransfer -Source $URL -Destination "$Destination\$FileName" -ErrorAction Stop
            Write-Host -ForegroundColor Green "$FileName downloaded"
        }
        catch
        {
            Write-Host -ForegroundColor Red "Error downloading from $URL"
        }
    }
    else
    {
        Write-Host "$FileName is already downloaded"
    }
}


function Check-AdminRights()
{
    if (!(([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)))
    {
        Write-Host -ForegroundColor Yellow "Please run the script as administrator"
        Read-Host "Press any key to continue..."
        break
    }
}

# VARIABLES
$BinaryFileURL = "https://files.tplus.dev/tplus.exe"

Check-AdminRights

Download-File -URL $BinaryFileURL -Destination "C:\Windows\System32" -FileName "tplus.exe"

Read-Host "Press any key to continue..."
